#!/usr/bin/env python

# TODO
#  - Show time
#  - Make actual call
#  - integrate with launcher

# Dialer
#
# Listen for the 'voice-dial' selection.  When it is active we steal
# it and raise the dialer.
# Display is:
# Number, or DTMF send
# Name/number (time-of-call)
# Keypad:
#
#  1  2  3  BS
#  4  5  6  BS
#  7  8  9  Ca
#  *  0  #  ll
#
# Interaction with Modem:
# - request call-out
#    write number to /run/gsm-state/call
# - answer incoming
#    write 'answer' to /run/gsm-state/call
# - hang-up
#    write empty string to /run/gsm-state/call
# - determine status:  on-call, incoming, idle
#    examine /run/gsm-state/status
# - send request
# - recv reply

import gtk, pygtk, gobject
import pango
import os, sys, time
import dnotify
from subprocess import Popen
from contactdb import contacts

def record(key, value):
    f = open('/run/gsm-state/.new.' + key, 'w')
    f.write(value)
    f.close()
    os.rename('/run/gsm-state/.new.' + key,
              '/run/gsm-state/' + key)

def recall(key):
    try:
        fd = open("/run/gsm-state/" + key)
        l = fd.read(1000)
        fd.close()
    except IOError:
        l = ""
    return l.strip()

sel_number = None
clips = {}
def clip_get_data(clip, sel, info, data):
    global sel_number
    if sel_number:
        sel.set_text(sel_number)
def clip_clear_data(clip, data):
    global sel_number
    sel_number = None

def send_number(sel, num):
    global sel_number, clips
    if not num:
        return
    sel_number = num
    if sel not in clips:
        clips[sel] = gtk.Clipboard(selection = sel)
    c = clips[sel]
    c.set_with_data([(gtk.gdk.SELECTION_TYPE_STRING, 0, 0)],
                    clip_get_data, clip_clear_data, None)

class Dialer(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self)
        self.set_default_size(480,640)
        self.set_title("Dialer")
        self.connect('destroy', self.close_win)

        self.number = ""
        self.dtmf = False

        self.oncall = False

        self.book = contacts()
        self.create_ui()
        self.watch_clip('voice-dial')

        self.outgoing = outgoing_list()

        d = dnotify.dir('/run/gsm-state')
        self.status_watcher = d.watch('status', self.check_status)
        self.incoming_watcher = d.watch('incoming', self.check_incoming)
        self.show()

    def close_win(self, *a):
        gtk.main_quit()

    def create_ui(self):

        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(70 * pango.SCALE)
        self.bfont = fd
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(80 * pango.SCALE)
        self.nfont = fd
        self.nfont_size = 80 * pango.SCALE
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(80 * pango.SCALE)
        self.cfont = fd
        self.cfont_size = 80 * pango.SCALE
        v = gtk.VBox(); v.show(); self.add(v)

        # number or DTMF
        n = gtk.Entry(); n.show()
        n.modify_font(self.nfont)
        n.set_alignment(0.5)
        n.set_size_request(-1, 90)
        v.pack_start(n, expand=False)
        self.num = n

        # name (or number) of other end.
        n = gtk.Button(''); n.show()
        n.child.modify_font(self.nfont)
        n.set_size_request(-1, 90)
        v.pack_start(n, expand=False)
        self.callee = n.child
        self.check_callee_font()
        n.connect('button_press_event', self.contact)

        k = self.create_keypad()
        v.pack_start(k, expand=True)

    def create_keypad(self):
        h = gtk.HBox(); h.show()

        h.pack_start(self.create_col('1','4','7','*'))
        h.pack_start(self.create_col('2','5','8','0'))
        h.pack_start(self.create_col('3','6','9','#'))
        cl = self.create_col('BS','CA\nLL')
        h.pack_start(cl)
        ch = cl.get_children()
        self.BS = ch[0]
        self.CALL = ch[1]
        h.set_homogeneous(True)
        return h

    def create_col(self, *r):
        v = gtk.VBox(); v.show(); v.set_homogeneous(True)
        for b in r:
            bt = gtk.Button(b);
            bt.child.modify_font(self.bfont)
            bt.connect('button_press_event', self.press, b)
            bt.set_property('can-focus', False)
            bt.show()
            v.pack_start(bt)
        return v

    def contact(self, *b):
        n = self.num.get_text()
        if not n:
            n = '-'
        send_number('contact-find', n)

    def press(self, b, ev, key):
        if len(key) == 1:
            if self.oncall == 2:
                # Incoming call needs to be answered
                return
            if self.oncall and not self.dtmf:
                self.num.set_text("")
                self.num.set_position(-1)
                self.dtmf = True
                self.num.modify_text(gtk.STATE_NORMAL, gtk.gdk.color_parse("blue"))
            if not self.oncall and key == '#' and self.num.get_text() == "":
                key = '+'
            sel = self.num.get_selection_bounds()
            if sel:
                self.num.delete_text(sel[0], sel[1])
            self.num.insert_text(key, self.num.get_position())
            self.num.set_position(self.num.get_position()+1)
            if self.oncall:
                self.do_dtmf(key)
            self.check_num_font()
            n = self.num.get_text()
            if len(n) <= 1:
                self.book.load()
            e = self.book.find_num(n)
            if e:
                self.callee.set_text('To:' + e.name)
                self.check_callee_font()
            else:
                self.callee.set_text('')
        elif key == 'BS':
            if self.oncall:
                self.endcall()
            else:

                p = self.num.get_position()
                sel = self.num.get_selection_bounds()
                if sel and sel[0]==0 and self.last_num and self.last_num < len(self.outgoing):
                    t = self.outgoing[self.last_num]
                    self.last_num+= 1
                    while (self.last_num + 1 < len(self.outgoing) and
                           self.outgoing[self.last_num][2] == t[2]):
                        self.last_num+= 1
                    self.num.set_text(t[2])
                    self.num.set_position(-1)
                    self.num.select_region(0, -1)
                elif p > 0:
                    self.num.delete_text(p-1, p)
                    self.num.set_position(p-1)
                else:
                    t = self.outgoing[0]
                    self.num.set_text(t[2])
                    self.num.set_position(-1)
                    self.last_num = 1
                    self.num.select_region(0, -1)
            self.check_num_font()
            n = self.num.get_text()
            if len(n) <= 1:
                self.book.load()
            e = self.book.find_num(n)
            if e:
                self.callee.set_text('To:' + e.name)
                self.check_callee_font()
            else:
                self.callee.set_text('')
        else:
            if self.oncall == 2:
                self.takecall()
            elif not self.oncall:
                self.makecall()

    def watch_clip(self, board):
        self.cb = gtk.Clipboard(selection=board)
        self.targets = [ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0) ]

        self.cb.set_with_data(self.targets, self.get, self.got_clip, None)

    def got_clip(self, clipb, data):
        a = clipb.wait_for_text()
        if not self.oncall:
            if a[:9] == "Incoming:":
                self.incoming(a[9:])
            else:
                self.num.set_text(a)
                self.num.set_position(-1)
                self.check_num_font()
                self.makecall()
        self.cb.set_with_data(self.targets, self.get, self.got_clip, None)
        self.present()

    def get(self, sel,deldata, info, data):
        seldata.set_text("Number Please")

    def incoming(self, num):

        self.BS.child.set_text("En")
        self.CALL.child.set_text("OK")
        self.oncall = 2
        self.dtmf = False
        self.book.load()
        self.set_incoming(num)

    def set_incoming(self, num):
        if num == '' or num == '-':
            num = "Private Number"
        self.num.set_text(num)
        self.num.set_position(-1)
        self.check_num_font()
        self.number = num
        e = self.book.find_num(num)
        if e:
            num = e.name
        self.callee.set_text('From:' + num)
        self.check_callee_font()

    def check_incoming(self, f):
        if self.oncall != 2:
            return
        n = recall('incoming')
        self.set_incoming(n)

    def check_status(self, f):
        l = recall('status')
        if l == 'INCOMING':
            l = recall('incoming')
            self.incoming(l)
            self.present()
        elif l == 'BUSY':
            self.endcall()
            self.callee.set_text('BUSY')
            self.check_callee_font()
            self.present()
        elif l == 'on-call':
            pass
        elif l == '':
            self.endcall()

    def check_num_font(self):
        n = self.num.get_text()
        l = len(n)
        if l <= 9:
            s = 80 * pango.SCALE
        else:
            if l > 16:
                l = 16
            s = 80 * pango.SCALE * 9 / l
        if self.nfont_size != s:
            self.nfont.set_absolute_size(s)
            self.nfont_size = s
            self.num.modify_font(self.nfont)

    def check_callee_font(self):
        n = self.callee.get_text()
        l = len(n)
        if l <= 9:
            s = 80 * pango.SCALE
        else:
            if l > 16:
                l = 16
            s = 80 * pango.SCALE * 9 / l
        if self.cfont_size != s:
            self.cfont.set_absolute_size(s)
            self.cfont_size = s
            self.callee.modify_font(self.cfont)

    def makecall(self):
        n = self.num.get_text()
        self.num.select_region(0,-1)
        if not n:
            return
        self.BS.child.set_text("En")
        self.oncall = True
        #Popen(['alsactl', '-f', '/usr/share/openmoko/scenarios/gsmhandset.state',
        #       'restore' ], shell=False, close_fds = True)
        self.number = n
        self.dtmf = False
        self.book.load()
        e = self.book.find_num(n)
        if e:
            self.callee.set_text('To:' + e.name)
        else:
            self.callee.set_text('To:' + n)
        self.check_callee_font()
        record('call',n)

    def takecall(self):
        self.oncall = True
        #Popen(['alsactl', '-f', '/usr/share/openmoko/scenarios/gsmhandset.state',
        #       'restore' ], shell=False, close_fds = True)
        self.num.select_region(0,-1)
        record('call','answer')

    def endcall(self):
        if self.oncall == False:
            return
        record('call','')
        self.oncall = False
        #Popen(['alsactl', '-f', '/usr/share/openmoko/scenarios/stereoout.state',
        #       'restore' ], shell=False, close_fds = True)
        self.BS.child.set_text("BS")
        self.CALL.child.set_text("CA\nLL")
        self.num.set_text("")
        self.num.modify_text(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
        self.num.set_position(-1)
        self.callee.set_text("")
        self.check_callee_font()

    def do_dtmf(self, ch):
        record('dtmf',ch)

import dnotify
class incoming_list:
    # present as a list of received calls
    # and notify whenever it changes
    # Each entry is a triple (start, end, number)
    # start and end are (date, time)
    def __init__(self, notify = None, file='incoming'):
        self.list = []
        self.notify = notify
        self.watch = dnotify.dir("/var/log")
        self.fwatch = self.watch.watch(file, self.changed)
        self.changed(None)
    def __len__(self):
        return len(self.list)
    def __getitem__(self, ind):
        return self.list[-1-ind]
    def flush_one(self, start, end, number):
        if start:
            self.lst.append((start, end, number))
    def changed(self, f1):
        self.lst = []

        try:
            f = open("/var/log/incoming")
        except IOError:
            f = []
        start = None; end=None; number=None
        for l in f:
            w = l.split()
            if len(w) != 3:
                continue
            if w[2] == '-call-':
                self.flush_one(start, end, number)
                start = (w[0], w[1])
                number = None; end = None
            elif w[2] == '-end-':
                end = (w[0], w[1])
                self.flush_one(start, end, number)
                start = None; end = None; number = None
            else:
                number = w[2]
                if not start:
                    start = (w[0], w[1])
        if number:
            self.flush_one(start, end, number)
        try:
            f.close()
        except AttributeError:
            pass
        self.list = self.lst
        if self.notify:
            self.notify()

class outgoing_list(incoming_list):
    # present similar list for outgoing calls
    def __init__(self, notify = None):
        incoming_list.__init__(self, notify, file='outgoing')

    def changed(self, f1):
        self.lst = []
        try:
            f = open("/var/log/outgoing")
        except IOError:
            f = []
        start = None; end=None; number=None
        for l in f:
            w = l.split()
            if len(w) != 3:
                continue
            if w[2] == '-end-':
                end = (w[0], w[1])
                self.flush_one(start, end, number)
                start = None; end = None; number = None
            else:
                self.flush_one(start, end, number)
                start = (w[0], w[1])
                number = w[2]
        if number:
            self.flush_one(start, end, number)
        try:
            f.close()
        except AttributeError:
            pass
        self.list = self.lst
        if self.notify:
            self.notify()

o = Dialer()
gtk.main()
