#!/usr/bin/env python

import suspend
import time
import gobject
from subprocess import Popen, PIPE

sus_time = 0
sus_chg = 0
sus_msg = ''

chg='/sys/class/power_supply/bq27000-battery/charge_now'
def file_text(name):
    try:
        f = open(name)
    except:
        return ""
    t = f.read()
    f.close()
    return t.strip()

def file_num(name):
    try:
        i = int(file_text(name))
    except:
        i = 0
    return i

def do_suspend():
    global sus_time, sus_chg, sus_msg
    sus_time = time.time()
    sus_chg = file_num(chg)
    if not sus_msg:
        sus_msg = time.strftime('%Y-%m-%d %H:%M:%S suspend ') + ("%d"%sus_chg) + '\n'
    return True

delta_time  = 0
delta_chg = 0
res_chg = 0
def do_resume():
    global delta_time, delta_chg, res_chg, sus_msg

    delta = "%d " % (delta_chg * 3600 / delta_time)
    l = ''
    if False:
        p = Popen('/root/suspend-state', shell=False, close_fds = True,
                  stdout = PIPE)
        try:
            l = p.stdout.read(4096);
        except:
            l = ''
            pass
        if not l:
            try:
                l = p.stdout.read(4096);
            except:
                l = ''
                pass
        p.wait()
    flight = ""
    try:
        fd = open("/var/lib/misc/flightmode/active")
        fl = fd.read(1)
        fd.close()
        if len(fl) > 0:
            flight = " [flight]"
    except:
        pass
    try:
        f = open('/var/log/suspend-power.log', 'a')
        if sus_msg:
            f.write(sus_msg)
            sus_msg = ''
        f.write(time.strftime('%Y-%m-%d %H:%M:%S resume  ') + ("%d "%res_chg) + delta + ("(%ds)"%delta_time) + l.strip() + flight + '\n')
        f.close()
    except:
        pass

    try:
	f = open("/sys/kernel/debug/pm_debug/core_pwrdm/suspend","w")
	f.write("0\n")
	f.close()
    except:
	pass

def sched_resume():
    global sus_time, sus_chg
    global delta_time, delta_chg, res_chg

    dt = time.time() - sus_time
    if dt <= 5:
        return
    delta_time = dt
    res_chg = file_num(chg)
    delta_chg = sus_chg - res_chg
    gobject.idle_add(do_resume)

c = gobject.main_context_default()
handle = suspend.monitor(do_suspend, sched_resume)
while True:
    c.iteration()
