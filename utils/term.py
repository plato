#!/usr/bin/env python

# Terminal ap for touch screen
# we create a keyboard window and a blank space,
# then run 'xterm' and make it appear in the blank space.

from tapboard import TapBoard
from subprocess import Popen, PIPE
from fingerscroll2 import FingerScroll
import gtk, pango
import vte
import time

class Terminal(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self)
        self.connect("destroy", self.close_app)
        self.set_title("Terminal")

        v = gtk.VBox()
        v.show()
        self.add(v)

        t = TapBoard()
        t.show()
        v.pack_end(t, expand=False)
        
        term = vte.Terminal()
        term.show()
        v.pack_start(term, expand=True)
        term.fork_command()
        term.set_scroll_on_keystroke(True)
        term.connect("child-exited", self.close_app)

        h = term.get_char_height()
        print h
        f = FingerScroll(term, 15)

        fd = term.get_font()
        fd.set_absolute_size(14 * pango.SCALE)
        term.set_font(fd)

        t.connect('key', lambda t,str: term.feed_child(str))

    def close_app(self, *x):
        gtk.main_quit()


if __name__ == "__main__":
    t = Terminal()
    t.set_default_size(480, 640)
    t.show()
    gtk.main()
