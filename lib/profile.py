#
# Load a list of profiles.  Later ones over-ride
# earlier.
# These are just variable assignments stored in a map.

import time, os

expect = [
    'tone',
    'volume',
    'vibrate',
    'LED',
]

def read_profile(p, file):
    global expect
    try:
        f = open(file)
        l = f.readlines()
    except IOError:
        l = []
    for ln in l:
        ln = ln.strip()
        w = ln.split(':', 1)
        if len(w) == 2:
            k = w[0].strip()
            if k in expect:
                p[k] = w[1].strip()
            else:
                raise ValueError

def load_profile(p, t):
    if '=' in t:
        pr = t.split('=', 1)
        p[pr[0].strip()] = pr[1].strip()
    else:
        read_profile(p, "/data/profiles/"+t)

def load_rules(file, p, event, who):
    when = 0
    try:
        f = open(file)
        l = f.readlines()
        when = os.fstat(f.fileno()).st_mtime
    except IOError:
        l = []
    for ln in l:
        ln = ln.strip()
        w = ln.split(':', 1)
        if len(w) == 2 and rule_matches(w[0].strip(), event, who, when):
            load_profile(p, w[1].strip())

def get_profile(event, who):
    p = {}
    load_rules("/data/rules", p, event, who)
    try:
        d = os.listdir('/data/rules.d')
        d = filter(lambda x: x[0] != '.', d)
        d.sort()
        for b in d:
            load_rules(os.path.join('/data/rules.d',b), p, event, who)
    except IOError:
        pass
    except OSError:
        pass
    return p

def rule_matches(rule, event, who, when):
    """A rule can contain several words. If none fail,
       the rule succeeds (so an empty rule always succeeds)
       words can match:
         - The event:  ring sms alarm
         - who:   last 8 digits must match, "who" can be a list
         - <N:   rule file was changed less than N minutes ago
         - day-of-week:  mo,we,fr-su
         - time-of-day: 10.00-11.30,23.30-01.30
    """
    for r in rule.split():
        if r == '':
            continue
        if r == event:
            continue
        if len(r) > 1 and r[0] == '<' and r[1:].isdigit():
            if time.time() >= when and time.time() < when + int(r[1:])*60:
                continue
            return False

        c = False
        for w in who.split():
            if w[-8:] == r[-8:]:
                c = True
        if c:
            continue

        tm = time.localtime()
        a = day_match(r, tm)
        if a == False:
            return False
        if a == True:
            continue
        a = time_match(r, tm)
        if a == False:
            return False
        if a == True:
            continue
        return False

    return True


# python is broken: tm_wday==0 means monday!!!
days = ['mo','tu','we','th','fr','sa','su']
def day_match(d, tm):
    global days
    dl = d.split(',')
    for d1 in dl:
        if d1 in days:
            if days.index(d1) == tm.tm_wday:
                return True
        elif len(d1)==5 and d1[2] == '-':
            da = d1[0:2]
            db = d1[3:5]
            if da not in days or db not in days:
                return None
            dan = days.index(da)
            dbn = days.index(db)
            if dan <= dbn and dan <= tm.tm_wday and tm.tm_wday <= dbn:
                return True
            if dan > dbn and (dan <= tm.tm_wday or dbn >= tm.tm_wday):
                return True
        else:
            return None
    return False


def time_match(t, tm):
    tl = t.split(',')
    for t1 in tl:
        if len(t1) != 11 or t1[5] != '-':
            return None
        m1 = to_min(t1[0:5])
        m2 = to_min(t1[6:11])
        if m1 < 0 or m2 < 0:
            return None
        mn = tm.tm_hour*60 + tm.tm_min
        if m1 <= m2 and m1 <= mn and mn <= m2:
            return True
        if m1 > m2 and (mn >= m1 or mn <= m2):
            return True
    return False

def to_min(t):
    h=t[0:2]
    m=t[3:5]
    if not h.isdigit() or not m.isdigit():
        return -1
    h=int(h)
    m=int(m)
    if h >= 24 or m >= 60:
        return -1;
    return h*60 + m
