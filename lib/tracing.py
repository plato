
# trivial library for including tracing in programs
# It can be turned on with PYTRACE=1 in environment

import os,time,sys

tracing = False

if 'PYTRACE' in os.environ:
    if os.environ['PYTRACE']:
        tracing = True

def log(*mesg):
    if tracing:
        print time.ctime(),
        for m in mesg:
            print m,
        print
        sys.stdout.flush()
