
enum ical_interval {
	Secondly, Minutely, Hourly, Daily, Weekly, Monthly, Yearly,
	BadInterval
};
/*
 * A date/time/period is stored in a 'struct ical_time'
 */
struct ical_time {
	short hr, min, sec;
	short yr, mon, day;
	short wday, yday;
	short time_set;
	short days;
	int secs;
	short dur_set;
	char *zone;
};

struct ical_list {
	int cnt, size;
	int *v;
};
struct ical_timelist {
	int cnt, size;
	struct ical_time *v;
};

/*
 * An ical_iter can step through dates given a unit and step size
 */
struct ical_iter {
	struct  ical_time now;
	enum ical_interval unit;
	int step;
};

/*
 * An ical_rrule holds the various fields from an RRULE
 */
struct ical_rrule {
	enum ical_interval unit;
	int step;
	int count;
	struct ical_time until; /* mon==0 if not set */
	int wkst;
	int any_bydays;
	struct ical_list bysec, bymin, byhr, bymonth;
	struct ical_list bydays[7];
	struct ical_list bymday, byyday, byweekno;
	struct ical_list setpos;
};

struct ical_dates {
	struct ical_time start;
	struct ical_rrule rr;
	struct ical_timelist rdate, exdate;
};

/* ical_times can be modified by adding/subtracting fields, but
 * must then be normalised
 */
void ical_set_wday(struct ical_time *t);
void ical_norm_mon(struct ical_time *t);
void ical_norm_day(struct ical_time *t);
void ical_norm_hr(struct ical_time *t);
void ical_norm_min(struct ical_time *t);
void ical_norm_sec(struct ical_time *t);

/* Advance it->now to next thing */
void ical_next(struct ical_iter *it);
int ical_ordered(const struct ical_time *a, const struct ical_time *b);

/* Parse various structures.  'arg' string will be destroyed */
int ical_parse_rrule(struct ical_rrule *rr, char *arg, char *tz);
int ical_parse_time(struct ical_time *tm, char *arg, char *tz);
int ical_parse_time_list(struct ical_timelist *tl, char *arg, char *tz);
int ical_parse_dates_line(struct ical_dates *id, char *arg);

void ical_rr_dates(struct ical_time *start, struct ical_rrule *rr,
		   struct ical_time *from, int max,
		   struct ical_timelist *rv);
int ical_strftime(char *buf, int max, const char *fmt, struct ical_time *itm);
void ical_localtime(struct ical_time *itm, const time_t *timep);
time_t ical_mktime(struct ical_time *itm);

void ical_list_free(struct ical_list *l);
void ical_timelist_free(struct ical_timelist *tl);
void ical_rrule_free(struct ical_rrule *rr);
void ical_dates_free(struct ical_dates *d);

int ical_fmt_time(char *buf, int size, struct ical_time *tm);
int ical_fmt_rr(char *buf, int size, struct ical_rrule *rr);
