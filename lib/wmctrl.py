
#
# manage a list of current windows and allow a selected
# window to be raised.
# I'm using Xlib for this, which doesn't have a built-in event
# mechanism like gtk does in gobject.
# So if you want to make sure property change notify events
# get handled, you need to arrange that read events on
# winlist.fd are passed to winlist.events.
# e.g. gobject.io_add_watch(winlist.fd, gobject.IO_IN, winlist.events)
#

import Xlib.X
import Xlib.display
import Xlib.protocol.event

class mywindow:
    def __init__(self, win, name, pid, id, list):
        self.id = id
        self.win = win
        self.name = name
        self.pid = pid
        self.list = list

    def raise_win(self):
        msg = Xlib.protocol.event.ClientMessage(window = self.win,
                                                client_type = self.list.ACTIVE_WINDOW,
                                                data = (32, [0,0,0,0,0])
                                                )
        msg.send_event = 1
        mask = (Xlib.X.SubstructureRedirectMask |
                Xlib.X.SubstructureNotifyMask)
        self.list.root.send_event(msg, event_mask = mask)
        self.win.map()
        self.win.raise_window()
        #p = w.query_tree().parent
        #if p:
        #    p.map()
        #    p.raise_window()
        self.list.display.flush()

    def close_win(self):
        msg = Xlib.protocol.event.ClientMessage(window = self.win,
                                                client_type = self.list.CLOSE_WINDOW,
                                                data = (32, [0,0,0,0,0])
                                                )
        msg.send_event = 1
        mask = (Xlib.X.SubstructureRedirectMask |
                Xlib.X.SubstructureNotifyMask)
        self.list.root.send_event(msg, event_mask = mask)
        self.list.display.flush()

class winlist:
    def __init__(self, add_handle = None):
        self.display = Xlib.display.Display()
        self.root = self.display.screen().root
        self.winfo = {}
        self.windows = ()
        self.WM_STRUT = self.display.intern_atom('_NET_WM_STRUT')
        self.CARDINAL = self.display.intern_atom('CARDINAL')
        self.ACTIVE_WINDOW = self.display.intern_atom('_NET_ACTIVE_WINDOW')
        self.CLOSE_WINDOW = self.display.intern_atom('_NET_CLOSE_WINDOW')
        self.NAME = self.display.intern_atom('WM_NAME')
        self.STRING = self.display.intern_atom('STRING')
        self.PID = self.display.intern_atom('_NET_WM_PID')
        self.LIST = self.display.intern_atom('_NET_CLIENT_LIST_STACKING')
        self.WINDOW = self.display.intern_atom('WINDOW')

        self.fd = self.display.fileno()
        self.change_handle = None
        self.add_handle = add_handle
        self.del_handle = None

        self.root.change_attributes(event_mask = Xlib.X.PropertyChangeMask )
        self.get_list()


    def add_win(self, id):
        if id in self.winfo:
            return self.winfo[id]
        w = self.display.create_resource_object('window', id)
        p = w.get_property(self.WM_STRUT, self.CARDINAL, 0, 100)
        self.winfo[id] = None
        if p:
            return None
        p = w.get_property(self.NAME, self.STRING, 0, 100)
        if p and p.format == 8:
            name = p.value
            name = name.replace('&','&amp;')
            name = name.replace('<','&lt;')
            name = name.replace('>','&gt;')
        else:
            return None

        p = w.get_property(self.PID, self.CARDINAL, 0, 100)
        if p and p.format == 32:
            pid = p.value[0]
        else:
            pid = 0

        self.winfo[id] = mywindow(w, name, pid, id, self)

        if self.add_handle:
            self.add_handle(self.winfo[id])
        return self.winfo[id]


    def get_list(self):
        l = self.root.get_property(self.LIST, self.WINDOW, 0, 100)
        windows = []
        for w in l.value:
            if self.add_win(w):
                windows.append(w)
        self.windows = windows
        self.clean_winfo()
        if self.change_handle:
            self.change_handle()

    def clean_winfo(self):
        togo = []
        for w in self.winfo:
            if w not in self.windows:
                togo.append(w)
        for w in togo:
            del self.winfo[w]
            if self.del_handle:
                self.del_handle(w)

    def events(self, *a):
        i = self.display.pending_events()
        while i > 0:
            event = self.display.next_event()
            self.handle_event(event)
            i = i - 1
        return True

    def handle_event(self, event):
        if event.atom != self.LIST:
            return False
        self.get_list()
        return True

    def top(self, num = 0):
        if num > len(self.windows) or num < 0:
            return None
        return self.winfo[self.windows[-1-num]]

    def on_change(self, func, add=None, delete=None):
        self.change_handle = func
        self.add_handle = add
        self.del_handle = delete


if __name__ == '__main__':
    w = winlist()
    for i in w.winfo:
        print i, w.winfo[i].name
    while 1:
        event = w.display.next_event()
        if w.handle_event(event):
            print "First is", w.top(1).name
            w.top(1).raise_win()

