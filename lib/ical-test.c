
#include <unistd.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

#include "ical-dates.h"

int main(int argc, char *argv[])
{
	struct ical_dates d;
	int a;
	int i;
	struct ical_timelist res;
	char buf[100];

	memset(&d, 0, sizeof(d));

	for (a = 1; a < argc; a++) {
		char *s = strdup(argv[a]);
		switch(ical_parse_dates_line(&d, s)) {
		case 0:
			printf("Bad line: %s\n", s);
			exit(2);
		case 1:
			/* cool */
			break;
		default:
			printf("Don't know about %s\n", argv[a]);
			break;
		}
		free(s);
	}
	ical_fmt_rr(buf, sizeof(buf), &d.rr);
	memset(&res, 0, sizeof(res));
	ical_rr_dates(&d.start, &d.rr, &d.start, 100, &res);
	printf("%d dates in %s\n", res.cnt, buf);
	for (i = 0; i < res.cnt; i++) {
		char buf[100];
		ical_strftime(buf, 100, "%Y/%m/%d %T %A", &res.v[i]);
		printf(" %s %ld\n", buf, ical_mktime(&res.v[i]));
	}
	return 0;
}
