/* TOFIX
 * Record where up to - and total length
 * Don't reconfigure between identical format sounds.
 * handle seek for ogg vorbis
 * ??handle MP3 in WAV files
 * long gap between sounds.??? maybe all those zeros?
 */

/*
 *
 * This is a daemon that waits for sound files to appear in a particular
 * directory, and when they do, it plays them.
 * Files can be WAV or OGG VORBIS
 * If there are multiple files, the lexically first is played
 * If a file has a suffix of -NNNNNNN, then play starts that many
 * milliseconds in to the file.
 * When a file disappear, play stops.
 * When the end of the sound is reached the file (typically a link) is removed.
 * However an empty file is treated as containing infinite silence, so
 * it is never removed.
 * When a new file appears which is lexically earlier than the one being
 * played, the played file is suspended until the earlier files are finished
 * with.
 * The current-play position (in milliseconds) is occasionally written to a file
 * with the same name as the sound file, but with a leading period.
 * This file starts with 'P' if the sound is playing, and 'S' if it has
 * stopped.  When playing, the actual position can be calculated using
 * the current time and mtime of the file.
 *
 * Expected use is that various alert tones are added to the directory with
 * early names, and a music file can be added with a later name for general
 * listening.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <sys/signal.h>
#include <sys/dir.h>
#include <asm/byteorder.h>
#include <vorbis/vorbisfile.h>
#include <event.h>

#include "list.h"
#include "libsus.h"

#define WAV_RIFF	COMPOSE_ID('R','I','F','F')
#define WAV_WAVE	COMPOSE_ID('W','A','V','E')
#define WAV_FMT		COMPOSE_ID('f','m','t',' ')
#define WAV_DATA	COMPOSE_ID('d','a','t','a')
#define WAV_PCM_CODE	1

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define COMPOSE_ID(a,b,c,d)	((a) | ((b)<<8) | ((c)<<16) | ((d)<<24))
#define LE_SHORT(v)	(v)
#define LE_INT(v)	(v)
#define BE_SHORT(v)	bswap_16(v)
#define BE_INT(v)	bswap_32(v)
#elif __BYTE_ORDER == __BIG_ENDIAN
#define COMPOSE_ID(a,b,c,d)	((d) | ((c)<<8) | ((b)<<16) | ((a)<<24))
#define LE_SHORT(v)	bswap_16(v)
#define LE_INT(v)	bswap_32(v)
#define BE_SHORT(v)	(v)
#define BE_INT(v)	(v)
#else
#error "Wrong endian"
#endif

typedef struct {
	u_int magic;	/* 'RIFF' */
	u_int length;	/* filelen */
	u_int type;	/* 'WAVE' */
} WaveHeader;

typedef struct {
	u_short format;		/* should be 1 for PCM-code */
	u_short modus;		/* 1 Mono, 2 Stereo */
	u_int sample_fq;	/* frequence of sample */
	u_int byte_p_sec;
	u_short byte_p_spl;	/* samplesize; 1 or 2 bytes */
	u_short bit_p_spl;	/* 8, 12 or 16 bit */
} WaveFmtBody;

typedef struct {
	u_int type;		/* 'data' or 'fmt ' */
	u_int length;		/* samplecount */
} WaveChunkHeader;

#ifndef LLONG_MAX
#define LLONG_MAX	9223372036854775807LL
#endif

#define DEFAULT_FORMAT		SND_PCM_FORMAT_U8
#define DEFAULT_SPEED		8000

#define FORMAT_DEFAULT		-1
#define FORMAT_RAW		0
#define FORMAT_VOC		1
#define FORMAT_WAVE		2
#define FORMAT_AU		3
#define FORMAT_VORBIS		4
#define FORMAT_UNKNOWN		9999
/* global data */

struct sound {
	int fd;
	int empty;
	struct list_head list;
	int seen;
	char *name;
	char scenario[20];
	int ino;
	long posn;
	int format; /* FORMAT_WAVE or FORMAT_OGG */
	char buf[8192];
	int period_bytes;
	int bytes, bytes_used, last_read;
	int eof;
	int attenuate; /* shift left */

	/* An audio file can contain multiple chunks.
	 * Here we record the remaining bytes expected
	 * before a header
	 */
	int chunk_bytes;

	OggVorbis_File vf;

	int pcm_format;
	int sample_bytes;
	int channels;
	int rate;
};

struct dev {
	snd_pcm_t	*handle;
	char		*period_buf;
	char		scenario[20];
	int		buf_size;
	int		period_bytes;
	int		sample_bytes;
	int		attenuate; /* shift left */
	int		present;
	int		pcm_format, channels, rate;
};

int dir_changed = 1;

void handle_change(int sig)
{
	dir_changed = 1;
}

static void nlog(char *m)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	printf("%d.%06d %s\n", tv.tv_sec, tv.tv_usec, m);
	fflush(stdout);
}

static void *raw_read(struct sound *s, int bytes)
{
	/* Return pointer to next 'bytes' bytes in the buffer.
	 * If there aren't that many, copy down and read some
	 * more.  If we hit EOF, return NULL and set ->eof.
	 */
	while (s->bytes - s->bytes_used < bytes && !s->eof) {
		if (s->bytes_used &&
		    s->bytes_used < s->bytes)
			memmove(s->buf, s->buf+s->bytes_used,
				s->bytes - s->bytes_used);
		s->bytes -= s->bytes_used;
		s->bytes_used = 0;
		while (s->bytes < bytes && !s->eof) {
			int n = read(s->fd, s->buf+s->bytes, sizeof(s->buf) - s->bytes);
			if (n <= 0)
				s->eof = 1;
			else
				s->bytes += n;
		}
	}
	if (s->bytes - s->bytes_used < bytes)
		return NULL;
	else {
		void *rv = s->buf + s->bytes_used;
		s->bytes_used += bytes;
		s->last_read = bytes;
		return rv;
	}
}

static void raw_unread(struct sound *s)
{
	s->bytes_used -= s->last_read;
	s->last_read = 0;
}

static void raw_skip(struct sound *s, int bytes)
{
	if (s->bytes - s->bytes_used >= bytes) {
		s->bytes_used += bytes;
		return;
	}
	bytes -= (s->bytes - s->bytes_used);
	s->bytes = 0;
	s->bytes_used = 0;
	lseek(s->fd, bytes, SEEK_CUR);
}

int parse_wave(struct sound *s)
{
	WaveHeader *h;
	WaveChunkHeader *c;
	WaveFmtBody *f;
	int n;
	int len;

	h = raw_read(s, sizeof(*h));
	if (!h)
		return 0;
	if (h->magic != WAV_RIFF || h->type != WAV_WAVE) {
		raw_unread(s);
		return 0;
	}
	/* Ignore the length - wait for EOF */
	while (1) {
		c = raw_read(s, sizeof(*c));
		if (!c)
			return 0;
		len = LE_INT(c->length);
		len += len % 2;
		if (c->type == WAV_FMT)
			break;
		raw_skip(s, len);
	}
	if (len < sizeof(WaveFmtBody))
		return 0;
	f = raw_read(s, len);
	if (!f)
		return 0;

	if (LE_SHORT(f->format) == 1 &&
	    LE_SHORT(f->bit_p_spl) == 16) {
		s->pcm_format = SND_PCM_FORMAT_S16_LE;
		s->sample_bytes = 2;
	} else if (LE_SHORT(f->format) == 1 &&
		   LE_SHORT(f->bit_p_spl) == 8) {
		s->pcm_format = SND_PCM_FORMAT_U8;
		s->sample_bytes = 1;
	} else
		return 0;
	s->channels = LE_SHORT(f->modus);
	if (s->channels < 1 || s->channels > 2)
		return 0;
	s->sample_bytes *= s->channels;
	s->rate = LE_INT(f->sample_fq);
	s->eof = 0;
	s->chunk_bytes = 0;
	return 1;
}

static int read_wave(struct sound *s, char *buf, int bytes)
{
	WaveChunkHeader *h;
	int len;
	int b = 0;
	char *rbuf;
	int i;

	while (b < bytes) {
		while (s->chunk_bytes == 0) {
			h = raw_read(s, sizeof(*h));
			if (!h)
				break;
			len = LE_INT(h->length);
			len += len % 2;
			if (h->type != WAV_DATA) {
				raw_skip(s, len);
				continue;
			}
			s->chunk_bytes = len;
		}
		if (s->chunk_bytes == 0)
			break;
		if (s->chunk_bytes >= bytes - b) {
			rbuf = raw_read(s, bytes - b);
			if (!rbuf)
				break;
			s->chunk_bytes -= bytes - b;
			memcpy(buf + b, rbuf, bytes - b);
			return bytes;
		}
		/* Remainder of chunk is less than we need */
		rbuf = raw_read(s, s->chunk_bytes);
		if (!rbuf)
			break;
		memcpy(buf + b, rbuf, s->chunk_bytes);
		b += s->chunk_bytes;
		s->chunk_bytes = 0;
	}

	rbuf = buf;
	for (i = b; i < bytes && s->chunk_bytes > 1; i++) {
		char *bf = raw_read(s, 1);
		if (bf)
			buf[i] = bf[0];
		else
			break;
		s->chunk_bytes--;
	}
	return i;
}

void seek_wave(struct sound *s, int msec)
{
	/* skip over 'msec' milliseconds of the wave */
	WaveChunkHeader *h;
	int bytes = ((long long)msec * s->rate / 1000) * s->sample_bytes;
	int len;

	while (bytes) {
		while (s->chunk_bytes == 0) {
			h = raw_read(s, sizeof(*h));
			if (!h)
				break;
			len = LE_INT(h->length);
			len += len % 2;
			if (h->type != WAV_DATA) {
				raw_skip(s, len);
				continue;
			}
			s->chunk_bytes = len;
		}
		if (s->chunk_bytes == 0)
			break;
		if (s->chunk_bytes >= bytes) {
			raw_skip(s, bytes);
			s->chunk_bytes -= bytes;
			return;
		}
		raw_skip(s, s->chunk_bytes);
		bytes -= s->chunk_bytes;
		s->chunk_bytes = 0;
	}
}

static int parse_vorbis(struct sound *s)
{
	FILE *f;
	vorbis_info *vi;
	int rv;

	lseek(s->fd, 0, 0);
	f = fdopen(s->fd, "r");
	if ((rv = ov_open_callbacks(f, &s->vf, NULL, 0, OV_CALLBACKS_DEFAULT)) < 0) {
		fclose(f);
		return 0;
	}

	vi = ov_info(&s->vf,-1);

	s->pcm_format = SND_PCM_FORMAT_S16_LE;
	s->channels = vi->channels;
	s->rate = vi->rate;
	s->sample_bytes = 2 * s->channels;
	s->eof = 0;
	return 1;
}

static int read_vorbis(struct sound *s, char *buf, int len)
{
	int section;
	int have = 0;
	if (s->eof)
		return 0;

	while(!s->eof && have < len) {
		long ret = ov_read(&s->vf,
				   buf + have,
				   len - have,
				   0,2,1,&section);
		if (ret == 0) {
			/* EOF */
			s->eof=1;
		} else if (ret < 0) {
			/* error in the stream.  Not a problem, just reporting it in
			   case we (the app) cares.  In this case, we don't. */
			s->eof = 1;
		} else {
			/* we don't bother dealing with sample rate changes, etc, but
			   you'll have to*/
			have += ret;
		}
	}

	if (s->eof)
		ov_clear(&s->vf);
	return have;
}

static void seek_vorbis(struct sound *s, int msec)
{
}

snd_pcm_t *open_dev(void)
{
	snd_pcm_t *handle;
	int rc;

	snd_config_update();
//	nlog("open device");
	rc = snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0/*SND_PCM_NONBLOCK*/);
	if (rc < 0)
		return NULL;
	else
		return handle;
}

void dev_close(struct dev *dev, int flush);
void set_scenario(struct dev *dev, char *scenario)
{
	char path[100];
	if (scenario[0] == 0)
		return;
	if (strcmp(dev->scenario, scenario) == 0)
		return;
	dev_close(dev, 0);
	strcpy(dev->scenario, scenario);
	snprintf(path, 100, "alsactl -f /data/scenarios/%s restore", scenario);
	system(path);
//	nlog(path);
}

void set_params(struct dev *dev, struct sound *sound)
{
	snd_pcm_hw_params_t *hwp;
	sigset_t blocked;

	if (sound->format == FORMAT_UNKNOWN)
		return;

	set_scenario(dev, sound->scenario);
	if (dev->handle == NULL) {
		dev->handle = open_dev();
		dev->pcm_format = 0;
	}

	if (dev->pcm_format == sound->pcm_format &&
	    dev->channels == sound->channels &&
	    dev->rate == sound->rate)
		return;

	sigemptyset(&blocked);
	sigaddset(&blocked, SIGIO);
	sigprocmask(SIG_BLOCK, &blocked, NULL);

	if (dev->pcm_format)
		snd_pcm_drop(dev->handle);

	snd_pcm_hw_params_alloca(&hwp);
	snd_pcm_hw_params_any(dev->handle, hwp);
	snd_pcm_hw_params_set_access(dev->handle, hwp, SND_PCM_ACCESS_RW_INTERLEAVED);
	snd_pcm_hw_params_set_format(dev->handle, hwp, sound->pcm_format);
	snd_pcm_hw_params_set_channels(dev->handle, hwp, sound->channels);

	snd_pcm_hw_params_set_rate(dev->handle, hwp, sound->rate, 0);
	snd_pcm_hw_params_set_period_size(dev->handle, hwp,
					  sound->period_bytes/sound->sample_bytes, 0);
	snd_pcm_hw_params_set_buffer_size(dev->handle, hwp,
					  sound->period_bytes*2/sound->sample_bytes);
	snd_pcm_hw_params(dev->handle, hwp);
	dev->pcm_format = sound->pcm_format;
	dev->channels = sound->channels;
	dev->rate = sound->rate;
	dev->attenuate = sound->attenuate;
	dev->sample_bytes = sound->sample_bytes;
	dev->period_bytes = sound->period_bytes;
	if (dev->buf_size < dev->period_bytes) {
		free(dev->period_buf);
		dev->period_buf = malloc(dev->period_bytes);
		dev->buf_size = dev->period_bytes;
	}
	sigprocmask(SIG_UNBLOCK, &blocked, NULL);
}

void load_some(struct dev *dev, struct sound *sound)
{
	int len;
	if (!dev || !dev->handle || !sound)
		return;

	switch(sound->format) {
	case FORMAT_WAVE:
		len = read_wave(sound,
				dev->period_buf + dev->present,
				dev->period_bytes - dev->present);
		break;
	case FORMAT_VORBIS:
		len = read_vorbis(sound,
				  dev->period_buf + dev->present,
				  dev->period_bytes - dev->present);
		break;
	default:
		sound->eof = 1;
		len = 0;
	}
	dev->present += len;
}

struct sound *open_sound(char *dir, char *name, int ino)
{
	char path[200];
	int fd;
	struct sound *s;
	char *eos, *eos1;

	strcpy(path, dir);
	strcat(path, "/");
	strcat(path, name);
	fd = open(path, O_RDONLY);
	if (fd < 0)
		return NULL;
	s = malloc(sizeof(*s));
	if (!s)
		return NULL;
	memset(s, 0, sizeof(*s));
	s->fd = fd;
	s->empty = 0;
	s->eof = 0;
	s->seen = 0;
	s->name = strdup(name);
	s->ino = ino;
	s->posn = 0;
	s->bytes = s->bytes_used = 0;

	/* Filename must contain the  'scenario' name which
	 * determines mixer settings, and may contain a single
	 * digit which reduces the volume, and a millisecond offset
	 * which is digits preceeded by a hyphen.
	 * Typically it starts with a numeric priority.
	 * So:  05-ring-alert5-20
	 * Would play the sound with the 'alert' scenario and 50% volume
	 * starting 20ms from start.
	 */
	/* check for millisecond suffix */
	eos = name + strlen(name);
	eos1 = eos;
	while (eos1 > name && isdigit(eos1[-1]))
		eos1--;
	if (eos1 > name && eos1[-1] == '-' && eos1[0]) {
		s->posn = atol(eos1);
		eos = eos1 - 1;
	}
	/* Now pick off volume and scenario name */
	if (eos > name && isdigit(eos[-1])) {
		int n = eos[-1] - '0';
		if (n == 0)
			n = 10;
		s->attenuate = 10 - n;
		eos--;
	} else
		s->attenuate = 0;
	eos1 = eos;
	while (eos1 > name && isalpha(eos1[-1]))
		eos1--;
	if (eos1 > name && eos1 < eos && eos - eos1 < 20) {
		strncpy(s->scenario, eos1, eos-eos1);
		s->scenario[eos-eos1] = 0;
	}

	if (lseek(fd, 0L, 2) == 0) {
		close(fd);
		s->fd = -1;
		s->empty = 1;
		s->format = FORMAT_UNKNOWN;
		return s;
	}
	lseek(fd, 0L, 0);
	/* Read header and set parameters */

	if (parse_wave(s))
		s->format = FORMAT_WAVE;
	else if (parse_vorbis(s))
		s->format = FORMAT_VORBIS;
	else
		s->format = FORMAT_UNKNOWN;

	if (s->rate <= 8000) {
		/* 100 ms == 800 samples, 1600 bytes */
		s->period_bytes = s->rate / 100 * s->sample_bytes;
	} else {
		/* 44100, 2 seconds, 4 bytes would be 160K !!
		 * Doesn't work yet.
		 */
		s->period_bytes = 2048;
	}

	if (s->posn)
		switch(s->format) {
		case FORMAT_WAVE:
			seek_wave(s, s->posn);
			break;
		case FORMAT_VORBIS:
			seek_vorbis(s, s->posn);
			break;
		}

	return s;

 fail:
	close(s->fd);
	free(s->name);
	free(s);
	return NULL;
}

void close_sound(struct sound *sound)
{
	close(sound->fd);
	free(sound->name);
	free(sound);
}

struct sound *find_match(struct list_head *list,
			 char *name, int ino,
			 int *matched)
{
	/* If name/ino is found in list, return it and set
	 * matched.
	 * else return previous entry (or NULL) and clear matched.
	 */
	struct sound *rv = NULL;
	struct sound *s;

	*matched = 0;
	list_for_each_entry(s, list, list) {
		int c = strcmp(s->name, name);
		if (c > 0)
			/* we have gone beyond */
			break;
		rv = s;
		if (c == 0) {
			if (s->ino == ino)
				*matched = 1;
			break;
		}
	}
	return rv;
}

void scan_dir(char *path, struct list_head *soundqueue)
{
	DIR *dir = opendir(path);
	struct dirent *de;
	struct sound *match;
	struct sound *pos;

	list_for_each_entry(match, soundqueue, list)
		match->seen = 0;

	while ((de = readdir(dir)) != NULL) {
		struct sound *new;
		int matched = 0;
		if (de->d_ino == 0 ||
		    de->d_name[0] == '.')
			continue;

		match = find_match(soundqueue, de->d_name, de->d_ino, &matched);
		if (matched) {
			match->seen = 1;
			continue;
		}
		new = open_sound(path, de->d_name, de->d_ino);
		if (! new)
			continue;
		new->seen = 1;
		if (match)
			list_add(&new->list, &match->list);
		else
			list_add(&new->list, soundqueue);
	}
	closedir(dir);

	list_for_each_entry_safe(match, pos, soundqueue, list)
		if (!match->seen) {
			list_del(&match->list);
			close_sound(match);
		}
}

static void scale_buf(struct dev *dev)
{
	int i;
	switch (dev->pcm_format) {
	case SND_PCM_FORMAT_U8:
		for (i = 0; i < dev->period_bytes; i++) {
			int v = dev->period_buf[i];
			v = v >> dev->attenuate;
			dev->period_buf[i] = v;
		}
		break;

	case SND_PCM_FORMAT_S16_LE:
		printf("period = %d\n", dev->period_bytes);
		for (i = 0; i < dev->period_bytes / 2; i++) {
			int v = *(short*)(dev->period_buf+i*2);
			v = v >> dev->attenuate;
			*(short*)(dev->period_buf+i*2) = v;
		}
		break;
	default:
		break;
	}
}

void play_buf(struct dev *dev)
{
	if (dev->present == dev->period_bytes) {
		sigset_t blocked;
		sigemptyset(&blocked);
		sigaddset(&blocked, SIGIO);
		sigprocmask(SIG_BLOCK, &blocked, NULL);
		if (dev->attenuate)
			scale_buf(dev);
		alarm(8);
		if(0){char b[100];sprintf(b, "write %d/%d=%d",
				     dev->period_bytes,dev->sample_bytes,
				     dev->period_bytes/dev->sample_bytes);

		nlog(b);
		}
		snd_pcm_writei(dev->handle,
			       dev->period_buf,
			       dev->period_bytes / dev->sample_bytes);
		alarm(0);
		dev->present = 0;
		sigprocmask(SIG_UNBLOCK, &blocked, NULL);
	}
}

void dev_close(struct dev *dev, int flush)
{
		sigset_t blocked;

	if (!dev->handle)
		return;

	sigemptyset(&blocked);
	sigaddset(&blocked, SIGIO);
	sigprocmask(SIG_BLOCK, &blocked, NULL);
	if (flush) {
//		nlog("purge");
		snd_pcm_drop(dev->handle);
		dev->present = 0;
	}
	if (dev->present) {
		memset(dev->period_buf + dev->present, 0,
		       dev->period_bytes - dev->present);
		if (dev->attenuate)
			scale_buf(dev);
//		nlog("flush last");
		snd_pcm_writei(dev->handle,
			       dev->period_buf,
			       dev->period_bytes / dev->sample_bytes);
		dev->present = 0;

		sigprocmask(SIG_UNBLOCK, &blocked, NULL);
	}
//	nlog("close");
	snd_pcm_drain(dev->handle);
	snd_pcm_close(dev->handle);
//	nlog("closed");
	dev->handle = NULL;
	sigprocmask(SIG_UNBLOCK, &blocked, NULL);
}

char *dir = "/run/sound";
int dfd;
struct dev dev;
int suspend_handle;
struct sound *last = NULL;
struct event work_ev;

static void do_scan(int fd, short ev, void *vp)
{
	struct list_head *soundqueue = vp;
	struct sound *next;
	struct timeval tv = {0, 0};

	fcntl(dfd, F_NOTIFY, DN_CREATE|DN_DELETE|DN_RENAME);
	scan_dir(dir, soundqueue);

	if (list_empty(soundqueue)) {
		event_del(&work_ev);
		dev_close(&dev, 1);
		set_scenario(&dev, "off");
		suspend_allow(suspend_handle);
		last = NULL;
		return;
	}
	next = list_entry(soundqueue->next,
			  struct sound, list);
	if (next->empty) {
		dev_close(&dev, 1);
		set_scenario(&dev, next->scenario);
		suspend_allow(suspend_handle);
		last = next;
		return;
	}
	suspend_block(suspend_handle);
	event_add(&work_ev, &tv);
}

static void do_work(int fd, short ev, void *vp)
{
	struct list_head *soundqueue = vp;
	struct sound *next;

	if (list_empty(soundqueue)) {
		set_scenario(&dev, "off");
		return;
	}
	next = list_entry(soundqueue->next,
			  struct sound, list);
	if (next->empty) {
		set_scenario(&dev, next->scenario);
		return;
	}

	if (next != last) {
		set_params(&dev, next);
		last = next;
	}
	if (next->eof) {
		char buf[1000];
		sprintf(buf, "%s/%s", dir, next->name);
		if (dev.present) {
			memset(dev.period_buf + dev.present, 0,
			       dev.period_bytes - dev.present);
			dev.present = dev.period_bytes;
			play_buf(&dev);
		}
		snd_pcm_drain(dev.handle);
		unlink(buf);
		list_del(&next->list);
		close_sound(next);
		do_scan(fd, ev, vp);
	} else {
		struct timeval tv = {0, 0};
		load_some(&dev, next);
		play_buf(&dev);
		event_add(&work_ev, &tv);
	}
}

static int suspend(void *vp)
{
	/* Don't need to do anything here, just as long as
	 * we cause suspend to block until check_alarms
	 * had a chance to run.
	 */
	do_scan(-1, 0, vp);
	return 1;
}

int main(int argc, char *argv[])
{
	struct list_head soundqueue;
	struct event ev;

	INIT_LIST_HEAD(&soundqueue);

	suspend_handle = suspend_block(-1);
	mkdir(dir, 0755);
	dfd = open(dir, O_RDONLY|O_DIRECTORY);
	if (dfd < 0) {
		fprintf(stderr, "sound: Cannot open %s\n", dir);
		exit(1);
	}

	event_init();

	suspend_watch(suspend, NULL, &soundqueue);
	signal_set(&ev, SIGIO, do_scan, &soundqueue);
	signal_add(&ev, NULL);

	event_set(&work_ev, -1, 0, do_work, &soundqueue);

	memset(&dev, 0, sizeof(dev));

	do_scan(-1, 0, (void*)&soundqueue);
	suspend_allow(suspend_handle);
	event_loop(0);
	exit(0);
}
