#!/usr/bin/env python

#
# Freerunner app to track petrol usage in new car.
# We need to keep a log of entries.  Each entry:
#
#  date  kilometers litres whether-full  price-paid
#
# These are displayed with l/100K number and can get
# overall l/100K and c/l between two points
#
# Must be able to edit old entries.
#
# So: 2 pages:
#
# 1/
#   summary line: l/100K, c/l from selected to mark
#   list of entries, scrollable and selectable
#   buttons:  new, edit, mark/unmark
#
# 2/ Entry fields for a new entry
#    date: default to 'today' with plus/minus button on either side
#    kilometer - simple text entry
#    litres  + 'fill' indicator
#    c/l
#    $
#    keyboard in number mode
#    Buttons:  Fill, not-fill, Discard, Save
#
# Should I be able to select between different vehicles?  Not now.

import sys, os, time
import pygtk, gtk, pango
from listselect import ListSelect
from tapboard import TapBoard


class petrol_list:
    def __init__(self, list):
        self.list = list
        self.mark = None
    def set_list(self, list):
        self.list = list
    def set_mark(self, ind):
        self.mark = ind

    def __len__(self):
        return len(self.list)
    def __getitem__(self, ind):
        i = self.list[ind]
        dt = i[0]
        tm = time.strptime(dt, '%Y-%m-%d')
        dt = time.strftime('%d/%b/%Y', tm)
        k = "%06d" % int(i[1])
        l = "%06.2f" % float(i[2])
        if len(i) > 3 and i[3] == "full":
            f = "F"
        else:
            f = "-"
        if len(i) > 4:
            p = "$%06.2f" % float(i[4])
        else:
            p = "----.--"
        str = "%s %s %s %s %s" % (dt, k, l, p, f)
        if self.mark == ind:
            type = 'mark'
        else:
            type = 'normal'
        return (str, type)

class Petrol(gtk.Window):
    def __init__(self, file):
        gtk.Window.__init__(self)
        self.connect("destroy",self.close_application)
        self.set_title("Petrol")

        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(35*pango.SCALE)
        vb = gtk.VBox(); self.add(vb); vb.show()
        self.isize = gtk.icon_size_register("petrol", 40, 40)

        self.listui = self.make_list_ui(fd)
        self.editui = self.make_edit_ui(fd)
        vb.add(self.listui); vb.add(self.editui)
        self.listui.show()
        self.editui.hide()

        self.active_entry = None
        self.colours={}

        self.filename = file
        self.load_file()
        self.pl.set_list(self.list)

    def close_application(self, ev):
        gtk.main_quit()

    def make_list_ui(self, fd):
        ui = gtk.VBox()
        l = gtk.Label("Petrol Usage")
        l.modify_font(fd)
        l.show(); ui.pack_start(l, expand=False)

        h = gtk.HBox(); h.show(); ui.pack_start(h, expand=False)
        h.set_homogeneous(True)
        ui.usage_summary = gtk.Label("??.??? l/CKm")
        ui.usage_summary.show()
        ui.usage_summary.modify_font(fd)
        h.add(ui.usage_summary)

        ui.price_summary = gtk.Label("???.? c/l")
        ui.price_summary.show()
        ui.price_summary.modify_font(fd)
        h.add(ui.price_summary)

        ui.list = ListSelect()
        ui.list.show()
        ui.pack_start(ui.list, expand=True)
        ui.list.set_format("normal","black", background="grey", selected="white")
        ui.list.set_format("mark", "black", bullet=True,
                           background="grey", selected="white")
        ui.list.set_format("blank", "black", background="lightblue")
        ui.list.connect('selected', self.selected)
        self.pl = petrol_list([])
        ui.list.list = self.pl
        ui.list.set_zoom(34)

        bbar = gtk.HBox(); bbar.show(); ui.pack_start(bbar, expand=False)
        self.button(bbar, "New", fd, self.new)
        self.button(bbar, "Edit", fd, self.edit)
        self.button(bbar, "Mark", fd, self.mark)
        return ui

    def make_edit_ui(self, fd):
        ui = gtk.VBox()

        # title
        l = gtk.Label("Petrol Event")
        l.modify_font(fd)
        l.show(); ui.pack_start(l, fill=False)

        # date - with prev/next buttons.
        h = gtk.HBox(); h.show(); ui.pack_start(h,fill=False)
        self.button(h, gtk.STOCK_GO_BACK, fd, self.date_prev, expand=False)
        l = gtk.Label("Today")
        l.modify_font(fd)
        l.show(); h.pack_start(l, expand=True)
        self.button(h, gtk.STOCK_GO_FORWARD, fd, self.date_next, expand=False)
        ui.date = l

        # text entry for kilometers
        h = gtk.HBox(); h.show(); ui.pack_start(h,fill=False)
        e = self.entry(h, "Km:", fd, self.KM)
        self.km_entry = e;

        # text entry for  litres, with 'fill' indicator
        h = gtk.HBox(); h.show(); ui.pack_start(h,fill=False)
        e = self.entry(h, "litres:", fd, self.Litres)
        self.l_entry = e;
        l = gtk.Label("(full)")
        l.modify_font(fd)
        l.show(); h.pack_start(l, expand=False)
        self.full_label = l

        # text entry for cents/litre
        h = gtk.HBox(); h.show(); ui.pack_start(h,fill=False)
        e = self.entry(h, "cents/l:", fd, self.Cents)
        self.cl_entry = e;

        # text entry for price paid
        h = gtk.HBox(); h.show(); ui.pack_start(h,fill=False)
        e = self.entry(h, "Cost: $", fd, self.Cost)
        self.cost_entry = e;

        self.entry_priority = [self.l_entry, self.cl_entry]

        # keyboard
        t = TapBoard('number'); t.show()
        ui.pack_start(t, fill=True)
        t.connect('key', self.use_key)

        # Buttons: fill/non-fill, Discard, Save
        bbar = gtk.HBox(); bbar.show(); ui.pack_start(bbar, fill=False)
        bbar.set_homogeneous(True)
        self.fill_button = self.button(bbar, "non-Fill", fd, self.fill)
        self.button(bbar, "Discard", fd, self.discard)
        self.button(bbar, "Save", fd, self.save)

        return ui

    def button(self, bar, label, fd, func, expand = True):
        btn = gtk.Button()
        if label[0:3] == "gtk" :
            img = gtk.image_new_from_stock(label, self.isize)
            img.show()
            btn.add(img)
        else:
            btn.set_label(label)
            btn.child.modify_font(fd)
        btn.show()
        bar.pack_start(btn, expand = expand)
        btn.connect("clicked", func)
        btn.set_focus_on_click(False)
        return btn

    def entry(self, bar, label, fd, func):
        l = gtk.Label(label)
        l.modify_font(fd)
        l.show(); bar.pack_start(l, expand=False)
        e = gtk.Entry(); e.show(); bar.pack_start(e, expand=True)
        e.modify_font(fd)
        e.set_events(e.get_events()|gtk.gdk.FOCUS_CHANGE_MASK)
        e.connect('focus-in-event', self.focus)
        e.connect('changed', func)
        return e

    def focus(self, ent, ev):
        self.active_entry = ent
        if (len(self.entry_priority) > 0 and
            self.check_entry(self.entry_priority[0]) == None):
            # nothing here - drop it
            self.entry_priority = self.entry_priority[1:]

        if (len(self.entry_priority) == 0 or
            self.entry_priority[0] != ent):
            # Make this entry the most recent
            self.entry_priority = [ent] + self.entry_priority[:1]

    def calc_other(self):
        if len(self.entry_priority) != 2:
            return
        if self.l_entry not in self.entry_priority:
            cl = self.check_entry(self.cl_entry)
            cost = self.check_entry(self.cost_entry)
            if cl != None and cost != None:
                self.force_entry(self.l_entry, "%.2f" %(cost * 100.0 / cl))
        if self.cl_entry not in self.entry_priority:
            l = self.check_entry(self.l_entry)
            cost = self.check_entry(self.cost_entry)
            if l != None and l > 0 and cost != None:
                self.force_entry(self.cl_entry, "%.2f" %(cost * 100.0 / l))
        if self.cost_entry not in self.entry_priority:
            cl = self.check_entry(self.cl_entry)
            l = self.check_entry(self.l_entry)
            if cl != None and l != None:
                self.force_entry(self.cost_entry, "%.2f" %(cl * l / 100.0))

    def force_entry(self, entry, val):
        entry.set_text(val)
        entry.modify_base(gtk.STATE_NORMAL, self.get_colour('yellow'))

    def use_key(self, tb, str):
        if not self.active_entry:
            return
        if str == '\b':
            self.active_entry.emit('backspace')
        elif str == '\n':
            self.active_entry.emit('activate')
        else:
            self.active_entry.emit('insert-at-cursor', str)

    def set_colour(self, name, col):
        self.colours[name] = col
    def get_colour(self, col):
        # col is either a colour name, or a pre-set colour.
        # so if it isn't in the list, add it
        if col == None:
            return None
        if col not in self.colours:
            self.set_colour(col, col)
        if type(self.colours[col]) == str:
            self.colours[col] = \
                self.get_colormap().alloc_color(gtk.gdk.color_parse(self.colours[col]))
        return self.colours[col]

    def check_entry(self, entry):
        txt = entry.get_text()
        v = None
        try:
            if txt != "":
                v = eval(txt)
            colour = None
        except:
            colour = 'red'
        entry.modify_base(gtk.STATE_NORMAL, self.get_colour(colour))
        return v

    def KM(self,entry):
        v = self.check_entry(entry)
    def Litres(self,entry):
        self.calc_other()
    def Cents(self,entry):
        self.calc_other()
    def Cost(self,entry):
        self.calc_other()

    def load_file(self):
        # date:km:litre:full?:price
        list = []
        try:
            f = open(self.filename)
            l = f.readline()
            while len(l) > 0:
                l = l.strip()
                w = l.split(':')
                if len(w) >= 3:
                    list.append(w)
                l = f.readline()
        except:
            pass

        list.sort(reverse=True)
        self.list = list
        self.curr = 0
        self.mark = None

    def save_file(self):
        try:
            f = open(self.filename + '.tmp', 'w')
            for l in self.list:
                f.write(':'.join(l) + '\n')
            f.close()
            os.rename(self.filename + '.tmp', self.filename)
        except:
            pass

    def selected(self, ev, ind):
        self.curr = ind
        l = self.list[ind]
        ind+= 1
        ltr = float(l[2])
        while ind < len(self.list) and \
                (len(self.list[ind]) <= 3 or self.list[ind][3] != 'full'):
            ltr += float(self.list[ind][2])
            ind += 1
        if ind >= len(self.list) or len(l) <= 3 or l[3] != 'full':
            lckm = "??.??? l/CKm"
        else:
            km = float(l[1]) - float(self.list[ind][1])
            lckm = "%6.3f l/CKm" % (ltr*100/km)
        self.listui.usage_summary.set_text(lckm)

        if len(l) >= 5 and float(l[2]) > 0:
            cl = "%6.2f c/l" % (float(l[4])*100/float(l[2]))
        else:
            cl = "???.? c/l"
        self.listui.price_summary.set_text(cl)

    def new(self, ev):
        self.curr = None
        self.editui.date.set_text('Today')
        self.km_entry.set_text('')
        self.l_entry.set_text('')
        self.full_label.set_text('(full)')
        self.cl_entry.set_text('')
        self.cost_entry.set_text('')
        self.listui.hide()
        self.editui.show()

    def edit(self, ev):
        if self.curr == None:
            self.curr = 0
        l = self.list[self.curr]
        self.editui.date.set_text(time.strftime('%d/%b/%Y', time.strptime(l[0], "%Y-%m-%d")))
        self.km_entry.set_text(l[1])
        self.l_entry.set_text(l[2])
        self.full_label.set_text('(full)' if l[3]=='full' else '(not full)')
        self.cost_entry.set_text(l[4])
        self.entry_priority=[self.l_entry, self.cost_entry];
        self.calc_other()
        self.listui.hide()
        self.editui.show()

    def mark(self, ev):
        pass

    def date_get(self):
        x = self.editui.date.get_text()
        try:
            then = time.strptime(x, "%d/%b/%Y")
        except ValueError:
            then = time.localtime()
        return then
    def date_prev(self, ev):
        tm = time.localtime(time.mktime(self.date_get()) - 12*3600)
        self.editui.date.set_text(time.strftime("%d/%b/%Y", tm))

    def date_next(self, ev):
        t = time.mktime(self.date_get()) + 25*3600
        if t > time.time():
            t = time.time()
        tm = time.localtime(t)
        self.editui.date.set_text(time.strftime("%d/%b/%Y", tm))

    def fill(self, ev):
        if self.full_label.get_text() == "(full)":
            self.full_label.set_text("(not full)")
            self.fill_button.child.set_text("Fill")
        else:
            self.full_label.set_text("(full)")
            self.fill_button.child.set_text("non-Fill")

    def discard(self, ev):
        self.listui.show()
        self.editui.hide()
        pass

    def save(self, ev):
        self.listui.show()
        self.editui.hide()
        date = time.strftime('%Y-%m-%d', self.date_get())
        km = "%d" % self.check_entry(self.km_entry)
        ltr = "%.2f" % self.check_entry(self.l_entry)
        full = 'full' if self.full_label.get_text() == "(full)"  else 'notfull'
        price = "%.2f" % self.check_entry(self.cost_entry)
        if self.curr == None:
            self.list.append([date,km,ltr,full,price])
        else:
            self.list[self.curr] = [date,km,ltr,full,price]
        self.list.sort(reverse=True)
        self.pl.set_list(self.list)
        self.listui.list.list_changed()
        if self.curr == None:
            self.listui.list.select(0)
            self.curr = 0
        else:
            self.listui.list.select(self.curr)
        self.save_file()
        self.selected(None, self.curr)

if __name__ == "__main__":

    p = Petrol("/data/RAV4")
    p.set_default_size(480, 640)
    p.show()
    gtk.main()
