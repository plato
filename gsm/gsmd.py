#!/usr/bin/env python

#
# Calls can be made by writing a number to
#  /run/gsm-state/call
# Status get set call 'Calling' and then 'BUSY' or ''
# Call can be answered by writing 'answer' to 'call'
# or can be cancelled by writing ''.
# During a call, chars can be written to
#  /run/gsm-state/dtmf
# to send tones.

## FIXME
# e.g. receive AT response +CREG: 1,"08A7","6E48"
#  show that SIM is now ready
# cope with /var/lock/suspend not existing yet
#  define 'reset'

import re, time, gobject, os
from atchan import AtChannel
import dnotify, suspend
from tracing import log
from subprocess import Popen

recording = {}
def record(key, value):
    global recording
    try:
        f = open('/run/gsm-state/.new.' + key, 'w')
        f.write(value)
        f.close()
        os.rename('/run/gsm-state/.new.' + key,
                  '/run/gsm-state/' + key)
    except OSError:
        # I got this once on the rename, don't know why
        pass
    recording[key] = value

def recall(key, nofile = ""):
    try:
        fd = open("/run/gsm-state/" + key)
        l = fd.read(1000)
        l = l.strip()
        fd.close()
    except IOError:
        l = nofile
    return l

def set_alert(key, value):
    path = '/run/alert/' + key
    if value == None:
        try:
            os.unlink(path)
        except OSError:
            pass
    else:
        try:
            f = open(path, 'w')
            f.write(value)
            f.close()
        except IOError:
            pass

lastlog={}
def calllog(key, msg):
    f = open('/var/log/' + key, 'a')
    now = time.strftime("%Y-%m-%d %H:%M:%S")
    f.write(now + ' ' + msg + "\n")
    f.close()
    lastlog[key] = msg

def calllog_end(key):
    if key in lastlog:
        calllog(key, '-end-')
        del lastlog[key]

class Task:
    def __init__(self, repeat):
        self.repeat = repeat
        pass
    def start(self, channel):
        # take the first action for this task
        pass
    def takeline(self, channel, line):
        # a line has arrived that is presumably for us
        pass
    def timeout(self, channel):
        # we asked for a timeout and got it
        pass

class AtAction(Task):
    # An AtAction involves:
    #   optionally sending an AT command to check some value
    #      matching the result against a string, possibly storing the value
    #   if there is no match send some other AT command, probably to set a value
    #
    # States are 'init' 'checking', 'setting', 'done'
    ok = re.compile("^OK")
    busy = re.compile("\+CMS ERROR.*SIM busy")
    not_ok = re.compile("^(ERROR|\+CM[SE] ERROR:)")
    def __init__(self, check = None, ok = None, record = None, at = None,
                 timeout=None, handle = None, repeat = None, arg = None,
                 critical = True, noreply = None, retries = 5):
        Task.__init__(self, repeat)
        self.check = check
        self.okstr = ok
        if ok:
            self.okre = re.compile(ok)
        self.record = record
        self.at = at
        self.arg = arg
        self.retries = retries
        self.timeout_time = timeout
        self.handle = handle
        self.critical = critical
        self.noreply = noreply

    def start(self, channel):
        channel.state['retries'] = 0
        channel.state['stage'] = 'init'
        self.advance(channel)

    def takeline(self, channel, line):
        if line == None:
            channel.force_state('reset')
            channel.advance()
            return
        m = self.ok.match(line)
        if m:
            channel.cancel_timeout()
            if self.handle:
                self.handle(channel, line, None)
            return self.advance(channel)

        if self.busy.match(line):
            channel.cancel_timeout()
            channel.set_timeout(5000)
            return
        if self.not_ok.match(line):
            return channel.abort_timeout()

        if channel.state['stage'] == 'checking':
            m = self.okre.match(line)
            if m:
                channel.state['matched'] = True
                if self.record:
                    record(self.record[0], m.expand(self.record[1]))
                    if len(self.record) > 3:
                        record(self.record[2], m.expand(self.record[3]))
                if self.handle:
                    self.handle(channel, line, m)
                return

        if channel.state['stage'] == 'setting':
            # didn't really expect anything here..
            pass

    def timeout(self, channel):
        if channel.state['retries'] >= self.retries:
            if self.critical:
                channel.force_state('reset')
            channel.advance()
            return
        channel.state['retries'] += 1
        channel.state['stage'] = 'init'
        channel.atcmd('')

    def advance(self, channel):
        st = channel.state['stage']
        if st == 'init' and self.check:
            channel.state['stage'] = 'checking'
            if self.timeout_time:
                channel.atcmd(self.check, timeout = self.timeout_time)
            else:
                channel.atcmd(self.check)
        elif (st == 'init' or st == 'checking') and self.at and not 'matched' in channel.state:
            channel.state['stage'] = 'setting'
            at = self.at
            if self.arg:
                at = at % channel.args[self.arg]
            if self.timeout_time:
                channel.atcmd(at, timeout = self.timeout_time)
            else:
                channel.atcmd(at)
            if self.noreply:
                channel.cancel_timeout()
                channel.advance()
        else:
            channel.advance()

def gpio_set(line, val):
    file = "/sys/class/gpio/gpio%d/value" % line
    try:
        fd = open(file, "w")
        fd.write("%d\n" % val)
        fd.close()
    except IOError:
        pass
def hs_on():
    try:
        os.stat("/dev/ttyHS_Application")
        return
    except OSError:
        pass
    gpio_set(186,0)
    time.sleep(0.1)
    gpio_set(186,1)
    time.sleep(6)
def hs_off():
    log("hs_off")
    gpio_set(186,0)
    time.sleep(0.1)

class PowerAction(Task):
    # A PowerAction ensure that we have a connection to the modem
    #  and sets the power on or off, or resets the modem
    def __init__(self, cmd):
        Task.__init__(self, None)
        self.cmd = cmd

    def start(self, channel):
        if self.cmd == "on":
            hs_on()
            if not channel.connected:
                channel.connect()
            if not channel.altchan.connected:
                channel.altchan.connect()
            channel.check_flightmode()
        elif self.cmd == 'down':
            hs_off()
        elif self.cmd == "off":
            record('carrier', '')
            record('cell', '')
            record('signal_strength','-/32')
            channel.disconnect()
            channel.altchan.disconnect()
            #hs_off()
        elif self.cmd == 'reopen':
            record('status','')
            record('incoming','')
            record('carrier', '')
            record('cell', '')
            record('signal_strength','-/32')
            calllog_end('incoming')
            calllog_end('outgoing')
            channel.disconnect()
            channel.altchan.disconnect()
            #hs_off()
            #hs_on()
            channel.connect()
            channel.altchan.connect()
        elif self.cmd == 'wait':
            time.sleep(5)
        return channel.advance()

class ChangeStateAction(Task):
    # This action changes to a new state, like a goto
    def __init__(self, state):
        Task.__init__(self, None)
        self.newstate = state
    def start(self, channel):
        if self.newstate:
            state = self.newstate
        elif channel.nextstate:
            state = channel.nextstate.pop(0)
        else:
            state = None
        if state:
            channel.gstate = state
            channel.tasknum = None
            log("ChangeStateAction chooses", channel.gstate)
            n = len(control[channel.gstate])
            channel.lastrun = n * [0]
        return channel.advance()

class CheckSMS(Task):
    def __init__(self):
        Task.__init__(self, None)
    def start(self, channel):
        if 'incoming' in channel.nextstate:
            # now is not a good time
            return channel.advance()
        if channel.pending_sms:
            channel.pending_sms = False
            p = Popen('gsm-getsms -n', shell=True, close_fds = True)
            ok = p.wait()
        return channel.advance()

class RouteVoice(Task):
    def __init__(self, on):
        Task.__init__(self, None)
        self.request = on
    def start(self, channel):
        if self.request:
            channel.sound_on = True
            try:
                f = open("/run/sound/00-voicecall","w")
                f.close()
            except:
                pass
            p = Popen('/usr/local/bin/gsm-voice-routing', close_fds = True)
            log('Running gsm-voice-routing pid', p.pid)
            channel.voice_route = p
        elif channel.sound_on:
            if channel.voice_route:
                channel.voice_route.send_signal(15)
                channel.voice_route.wait()
                channel.voice_route = None
            try:
                os.unlink("/run/sound/00-voicecall")
            except OSError:
                pass
            channel.sound_on = False
        return channel.advance()

class BlockSuspendAction(Task):
    def __init__(self, enable):
        Task.__init__(self, None)
        self.enable = enable
    def start(self, channel):
        print "BlockSuspendAction sets", self.enable
        if self.enable:
            channel.suspend_blocker.block()
            # No point holding a pending suspend any more
            if channel.suspend_pending:
                channel.suspend_pending = False
                print "BlockSuspendAction calls release"
                suspend.abort_cycle()
                channel.suspend_handle.release()
        if not self.enable:
            channel.suspend_blocker.unblock()

        channel.advance()

class Async:
    def __init__(self, msg, handle, handle_extra = None):
        self.msg = msg
        self.msgre = re.compile(msg)
        self.handle = handle
        self.handle_extra = handle_extra

    def match(self, line):
        return self.msgre.match(line)

# async handlers...
LAC=0
CELLID=0
cellnames={}
def status_update(channel, line, m):
    if m and m.groups()[3] != None:
        global LAC, CELLID, cellnames
        LAC = int(m.groups()[2],16)
        CELLID = int(m.groups()[3],16)
        record('cellid', "%04X %06X" % (LAC, CELLID));
        if CELLID in cellnames:
            record('cell', cellnames[CELLID])
            log("That one is", cellnames[CELLID])

def new_sms(channel, line, m):
    if m:
        channel.pending_sms = False
        record('newsms', m.groups()[1])
        p = Popen('gsm-getsms -n', shell=True, close_fds = True)
        ok = p.wait()

def maybe_sms(line, channel):
    channel.pending_sms = True

def sigstr(channel, line, m):
    if m:
        record('signal_strength', m.groups()[0] + '/32')

global incoming_cell_id
def cellid_update(channel, line, m):
    # get something like +CBM: 1568,50,1,1,1
    # don't know what that means, just collect the 'extra' line
    # I think the '50' means 'this is a cell id'.  I should
    # probably test for that.
    #
    # response can be multi-line
    global incoming_cell_id
    incoming_cell_id = ""

def cellid_new(channel, line):
    global CELLID, cellnames, incoming_cell_id
    if not line:
        # end of message
        if incoming_cell_id:
            l = re.sub('[^!-~]+',' ',incoming_cell_id)
            if CELLID:
                cellnames[CELLID] = l
            record('cell', l)
            return False
    line = line.strip()
    if incoming_cell_id:
        incoming_cell_id += ' ' + line
    else:
        incoming_cell_id = line
    return True

incoming_num = None
def incoming(channel, line, m):
    global incoming_num
    if incoming_num:
        record('incoming', incoming_num)
    else:
        record('incoming', '-')
    set_alert('ring', 'new')
    if channel.gstate not in ['on-call', 'incoming', 'answer']:
        calllog('incoming', '-call-')
        channel.set_state('incoming')
        record('status', 'INCOMING')
        global cpas_zero_cnt
        cpas_zero_cnt = 0

def incoming_number(channel, line, m):
    global incoming_num
    if m:
        num = m.groups()[0]
        if incoming_num == None:
            calllog('incoming', num);
        incoming_num = num
        record('incoming', incoming_num)

def no_carrier(channel, line, m):
    record('status', '')
    record('call', '')
    if channel.gstate != 'idle':
        channel.set_state('idle')

def busy(channel, line, m):
    record('status', 'BUSY')
    record('call', '')

def ussd(channel, line, m):
    pass

cpas_zero_cnt = 0
def call_status(channel, line, m):
    global cpas_zero_cnt
    global calling
    log("call_status got", line)
    if not m:
        return
    s = int(m.groups()[0])
    log("s = %d" % s)
    if s == 0:
        if calling:
            return
        cpas_zero_cnt += 1
        if cpas_zero_cnt <= 3:
            return
        # idle
        global incoming_num
        incoming_num = None
        record('incoming', '')
        if channel.gstate in ['on-call','incoming','call']:
            calllog_end('incoming')
            calllog_end('outgoing')
            record('status', '')
        if channel.gstate != 'idle' and channel.gstate != 'suspend':
            channel.set_state('idle')
    cpas_zero_cnt = 0
    calling = False
    if s == 3:
        # incoming call
        if channel.gstate not in  ['incoming', 'answer']:
            # strange ..
            channel.set_state('incoming')
            record('status', 'INCOMING')
            set_alert('ring', 'new')
            record('incoming', '-')
    if s == 4:
        # on a call - but could be just a data call, so don't do anything
        #if channel.gstate != 'on-call' and channel.gstate != 'hangup':
        #    channel.set_state('on-call')
        pass

def check_cfun(channel, line, m):
    # response to +CFUN?
    # If '1', then advance from init1 to init2
    # else if not 0, possibly do a reset
    if not m:
        return
    if m.groups()[0] == '1':
        if channel.gstate == 'init1':
            channel.set_state('init2')
        return
    if m.groups()[0] != '0' or channel.gstate == 'idle':
        global recording
        if 'signal_strength' in recording:
            s = recording['signal_strength'].split('/')
            if s[0] not in  '0-':
                return

        if channel.last_reset + 100 < time.time():
            channel.last_reset = time.time()
            channel.set_state('refresh')
        return

def data_handle(channel, line, m):
    # Response to _OWANDATA - should contain IP address etc
    if not m:
        if 'matched' in channel.state:
            # already handled match
            return
        # no connection active
        data_hungup(channel, failed=False)
        return
    # m[0] is gateway/IP addr.  m[1] and m[2] are DNS servers
    dns = (m.groups()[1], m.groups()[2])
    if channel.data_DNS != dns:
        record('dns', '%s %s' % dns)
        channel.data_DNS = dns
    ip = m.groups()[0]
    if channel.data_IP != ip:
        channel.data_IP = ip
        os.system('/sbin/ifconfig hso0 up %s' % ip)
        record('data', ip)
    data_log_update()

def data_call(channel, line, m):
    # delayed reponse to _OWANCALL.  Maybe be async, may be
    # polled with '_OWANCALL?'
    if not m:
        return
    if m.groups()[0] != '1':
        return
    s = int(m.groups()[1])
    #   0 = Disconnected, 1 = Connected, 2 = In setup,  3 = Call setup failed.
    if s == 0:
        data_hungup(channel, failed=False)
    elif s == 1:
        channel.set_state('idle')
    elif s == 2:
        # try again soon
        pass
    elif s == 3:
        data_hungup(channel, failed=True)

def data_hungup(channel, failed):
    if channel.data_IP:
        os.system('/sbin/ifconfig hso0 0.0.0.0 down')
    record('dns', '')
    record('data', '')
    channel.data_IP = None
    channel.data_DNS = None
    # FIXME should I retry, or reset?
    if channel.data_APN:
        # We still want a connection
        if failed:
            channel.set_state('refresh')
            return
        elif channel.next_data_call <= time.time():
            channel.next_data_call = (time.time() +
                                      time.time() - channel.last_data_call);
            channel.last_data_call = time.time()
            data_log_reset()
            channel.set_state('data-call')
            return
    if channel.gstate == 'data-call':
        channel.set_state('idle')

# DATA traffic logging - goes to /var/log/gsm-data
def read_bytes(dev = 'hso0'):
    f = file('/proc/net/dev')
    rv = None
    for l in f:
        w = l.strip().split()
        if w[0] == dev + ':':
            rv = ( int(w[1]), int(w[9]) )
            break
    f.close()
    return rv

last_data_usage = None
last_data_time = 0
SIM = None
def data_log_reset():
    global last_data_usage, last_data_time
    last_data_usage = read_bytes()
    last_data_time = time.time()
    record('data-last-usage', '%s %s' % last_data_usage)

def data_log_update(force = False):
    global last_data_usage, last_data_time, SIM

    if not SIM:
        SIM = recall('sim')
    if not SIM:
        return
    if not last_data_usage:
        data_log_reset()

    if not force and time.time() - last_data_time < 10*60:
        return

    data_usage = read_bytes()

    calllog('gsm-data', '%s %d %d' %
            (SIM,
             data_usage[0] - last_data_usage[0],
             data_usage[1] - last_data_usage[1]))

    last_data_usage = data_usage
    last_data_time = time.time()
    record('data-last-usage', '%s %s' % last_data_usage)

control = {}

# For flight mode, we turn the power off.
control['to-flight'] = [
    AtAction(at='+CFUN=0'),
    PowerAction('down'),
    AtAction(at='$QCPWRDN'),
    PowerAction('off'),
    ChangeStateAction('flight')
]
control['flight'] = [
    BlockSuspendAction(False),
    ]

control['refresh'] = [
    # Soft reset: just use CFUN to turn off/on
    # also reopen to clear status and just in case.
    AtAction(at='+CFUN=0',critical=False, retries=0),
    BlockSuspendAction(False),
    PowerAction('reopen'),
    BlockSuspendAction(True),
    AtAction(at='V1E0', timeout=30000),
    ChangeStateAction('init1'),
    ]

control['reset'] = [
    # Harder reset - use _ORESET and re-open devices.
    # Don't block suspend if we cannot re-open.
    #AtAction(at='_ORESET', critical = False),
    #AtAction(at='$QCPWRDN', critical = False, retries = 0),
    BlockSuspendAction(False),
    PowerAction('wait'),
    PowerAction('reopen'),
    BlockSuspendAction(True),
    AtAction(at='V1E0', timeout=30000),
    ChangeStateAction('init1'),
    ]

# For suspend, we want power on, but no wakups for status or cellid
control['suspend'] = [
    AtAction(check='+CPAS', ok='\+CPAS: (\d)', handle = call_status),
    CheckSMS(),
    ChangeStateAction(None), # allow async state change
    AtAction(at='+CNMI=1,1,0,0,0'),
    AtAction(at='_OSQI=0'),
    AtAction(at='_OEANT=0'),
    AtAction(at='_OSSYS=0'),
    AtAction(at='_OPONI=0'),
    AtAction(at='+CREG=0'),
    ]
control['resume'] = [
    BlockSuspendAction(True),
    AtAction(check='+CPAS', ok='\+CPAS: (\d)', handle = call_status),
    AtAction(at='+CNMI=1,1,2,0,0', critical=False),
    AtAction(at='_OSQI=1', critical=False),
    AtAction(at='+CREG=2'),
    CheckSMS(),
    ChangeStateAction(None),
    ChangeStateAction('idle'),
    ]

control['listenerr'] = [
    PowerAction('on'),
    AtAction(at='V1E0'),
    AtAction(at='+CMEE=2;+CRC=1')
    ]

# init1 checks phone status and once we are online
# we switch to init2, then idle
control['init1'] = [
    BlockSuspendAction(True),
    PowerAction('on'),
    AtAction(at='V1E0'),
    AtAction(at='+CMEE=2;+CRC=1'),
    # Turn the device on.
    AtAction(check='+CFUN?', ok='\+CFUN: 1', at='+CFUN=1', timeout=10000),
    # Report carrier as long name
    AtAction(at='+COPS=3,0'),
    # register with a carrier
    #AtAction(check='+COPS?', ok='\+COPS: \d+,\d+,"([^"]*)"', at='+COPS',
    #         record=('carrier', '\\1'), timeout=10000),
    AtAction(check='+COPS?', ok='\+COPS: \d+,\d+,"([^"]*)"', at='+COPS=0',
             record=('carrier', '\\1'), timeout=10000),
    AtAction(check='+CFUN?', ok='\+CFUN: (\d)', at='+CFUN=1', timeout=10000, handle=check_cfun, repeat=5000),
]

control['init2'] = [
    # text format for various messages such SMS
    AtAction(check='+CMGF?', ok='\+CMGF: 0', at='+CMGF=0'),
    # get location status updates
    AtAction(at='+CREG=2'),
    AtAction(check='+CREG?', ok='\+CREG: 2,(\d)(,"([^"]*)","([^"]*)")',
             handle=status_update, timeout=4000),
    # Enable collection of  Cell Info message
    #AtAction(check='+CSCB?', ok='\+CSCB: 1,.*', at='+CSCB=1'),
    #AtAction(at='+CSCB=0'),
    AtAction(at='+CSCB=1', critical=False),
    # Enable async reporting of TXT and Cell info messages
    #AtAction(check='+CNMI?', ok='\+CNMI: 1,1,2,0,0', at='+CNMI=1,1,2,0,0'),
    AtAction(at='+CNMI=1,0,0,0,0', critical=False),
    AtAction(at='+CNMI=1,1,2,0,0', critical=False),
    # Enable async reporting of signal strength
    AtAction(at='_OSQI=1', critical=False),
    AtAction(check='+CIMI', ok='(\d\d\d+)', record=('sim','\\1')),
    #_OSIMOP: "YES OPTUS","YES OPTUS","50502"
    AtAction(check='_OSIMOP', ok='_OSIMOP: "(.*)",".*","(.*)"',
             record=('sid','\\2', 'carrier','\\1'), critical=False),

    # Make sure to use both 2G and 3G
    # 0=only2g 1=only3g 2=prefer2g 3=prefer3g 4=staywhereyouare 5=auto
    AtAction(at='_OPSYS=%s,2', arg='mode', critical=False),

    # Enable reporting of Caller number id.
    AtAction(check='+CLIP?', ok='\+CLIP: 1,[012]', at='+CLIP=1', timeout=10000,
             critical = False),
    AtAction(check='+CPAS', ok='\+CPAS: (\d)', handle = call_status),
    ChangeStateAction('idle')
    ]

def if_data(channel):
    if not channel.data_APN and not channel.data_IP:
        # no data happening
        return 0
    if channel.data_APN and channel.data_IP:
        # connection is set up - slow watch
        return 30000
    if channel.data_IP:
        # must be shutting down - poll quickly, it shouldn't take long
        return 2000
    # we want a connection but don't have one, so we retry
    if time.time() < channel.next_data_call:
        return int((channel.next_data_call - time.time()) * 1000)
    return 1000

control['idle'] = [
    RouteVoice(False),
    CheckSMS(),
    BlockSuspendAction(False),
    AtAction(check='+CFUN?', ok='\+CFUN: (\d)', timeout=10000, handle=check_cfun, repeat=30000),
    AtAction(check='+COPS?', ok='\+COPS: \d+,\d+,"([^"]*)"', at='+COPS',
             record=('carrier', '\\1'), timeout=10000),
    AtAction(check='_OSIMOP', ok='_OSIMOP: "(.*)",".*","(.*)"',
             record=('sid','\\2', 'carrier','\\1'), critical=False, repeat=37000),
    #AtAction(check='+COPS?', ok='\+COPS: \d+,\d+,"([^"]*)"', at='+COPS=0',
    #         record=('carrier', '\\1'), timeout=10000, repeat=37000),
    # get signal string
    AtAction(check='+CSQ', ok='\+CSQ: (\d+),(\d+)',
             record=('signal_strength','\\1/32'), repeat=29000),
    AtAction(check='_OWANDATA?',
             ok='_OWANDATA: 1, ([0-9.]+), [0-9.]+, ([0-9.]+), ([0-9.]+), [0-9.]+, [0-9.]+,\d+$',
             handle=data_handle, repeat=if_data, critical=False),
    ]

control['data-call'] = [
    AtAction(at='+CGDCONT=1,"IP","%s"', arg='APN'),
    AtAction(at='_OWANCALL=1,1,0'),
    AtAction(at='_OWANCALL?', handle=data_call, repeat=2000),
    #ChangeStateAction('idle')
]

control['data-hangup'] = [
    AtAction(at='_OWANCALL=1,0,0'),
    ChangeStateAction('idle')
]

control['incoming'] = [
    BlockSuspendAction(True),
    AtAction(check='+CPAS', ok='\+CPAS: (\d)', handle = call_status, repeat=500),

    # monitor signal strength
    AtAction(check='+CSQ', ok='\+CSQ: (\d+),(\d+)',
             record=('signal_strength','\\1/32'), repeat=30000)
    ]

control['answer'] = [
    AtAction(at='A'),
    RouteVoice(True),
    ChangeStateAction('on-call')
    ]

control['call'] = [
    AtAction(at='D%s;', arg='number'),
    RouteVoice(True),
    ChangeStateAction('on-call')
    ]

control['dtmf'] = [
    AtAction(at='+VTS=%s', arg='dtmf', noreply=True),
    ChangeStateAction('on-call')
    ]

control['hangup'] = [
    AtAction(at='+CHUP', critical=False, retries=0),
    ChangeStateAction('idle')
    ]

control['on-call'] = [
    BlockSuspendAction(True),
    AtAction(check='+CPAS', ok='\+CPAS: (\d)', handle = call_status, repeat=2000),

    # get signal strength
    AtAction(check='+CSQ', ok='\+CSQ: (\d+),(\d+)',
             record=('signal_strength','\\1/32'), repeat=30000)
    ]

async = [
    Async(msg='\+CREG: ([01])(,"([^"]*)","([^"]*)")?', handle=status_update),
    Async(msg='\+CMTI: "([A-Z]+)",(\d+)', handle = new_sms),
    Async(msg='\+CBM: \d+,\d+,\d+,\d+,\d+', handle=cellid_update,
          handle_extra = cellid_new),
    Async(msg='\+CRING: (.*)', handle = incoming),
    Async(msg='RING', handle = incoming),
    Async(msg='\+CLIP: "([^"]+)",[0-9,]*', handle = incoming_number),
    Async(msg='NO CARRIER', handle = no_carrier),
    Async(msg='BUSY', handle = busy),
    Async(msg='\+CUSD: ([012])(,"(.*)"(,[0-9]+)?)?$', handle = ussd),
    Async(msg='_OSIGQ: ([0-9]+),([0-9]*)$', handle = sigstr),

    Async(msg='_OWANCALL: (\d), (\d)', handle = data_call),
    ]

class GsmD(AtChannel):

    # gsmd works like a state machine
    # the high level states are: flight suspend idle incoming on-call
    #   Note that the whole 'call-waiting' experience is not coverred here.
    #     That needs to be handled by whoever answers calls and allows interaction
    #     between user and phone system.
    #
    # Each state contains a list of tasks such as setting and
    # checking config options and monitoring state (e.g. signal strength)
    # Some tasks are single-shot and only need to complete each time the state is
    # entered.  Others are repeating (such as status monitoring).
    # We take the first task of the current list and execute it, or wait
    # until one will be ready.
    # Tasks themselves can be state machines, so we keep track of what 'stage'
    # we are up to in the current task.
    #
    # The system is (naturally) event driven.  The main two events that we
    # receive are:
    # 'takeline' which presents one line of text from the GSM device, and
    # 'timeout' which indicates that a timeout set when a command was sent has
    # expired.
    # Other events are:
    #   'taskready'  when the time of the next pending task arrives.
    #   'flight'     when the state of the 'flight mode' has changed
    #   'suspend'    when a suspend has been requested.
    #
    # Each event does some event specific processing to modify the state,
    # Then calls 'self.advance' to progress the state machine.
    # When high level state changes are requested, any pending task is discarded.
    #
    # If a task detects an error (gsm device not responding properly) it might
    # request a reset.  This involves sending a modem_reset command and then
    # restarting the current state from the top.
    # A task can also indicate:
    #  The next stage to try
    #  How long to wait before retrying (or None)
    #

    def __init__(self, path, altpath):
        AtChannel.__init__(self, path = path)

        self.extra = None
        self.flightmode = True
        self.state = None
        self.args = {}
        self.args['mode'] = 3 # prefer 3g
        self.suspend_pending = False
        self.pending_sms = False
        self.sound_on = True
        self.voice_route = None
        self.tasknum = None
        self.altpath = altpath
        self.altchan = CarrierDetect(altpath, self)
        self.data_APN = None
        self.data_IP =  None
        self.data_DNS = None
        self.last_data_call = 0
        self.next_data_call = 0
        self.gstate = None
        self.nextstate = []
        self.last_reset = time.time()

        record('carrier','')
        record('cell','')
        record('incoming','')
        record('signal_strength','')
        record('status', '')
        record('sim','')
        record('sid','')
        record('data-APN', '')
        record('data', '')
        record('dns', '')

        # set the initial state
        self.set_state('flight')

	# Monitor other external events which affect us
        d = dnotify.dir('/var/lib/misc/flightmode')
        self.flightmode_watcher = d.watch('active', self.check_flightmode)
        d = dnotify.dir('/var/lib/misc/gsm')
        self.gsmmode_watcher = d.watch('mode', self.check_gsmmode)
        d = dnotify.dir('/run/gsm-state')
        self.call_watcher = d.watch('call', self.check_call)
        self.dtmf_watcher = d.watch('dtmf', self.check_dtmf)
        self.data_watcher = d.watch('data-APN', self.check_data)

        self.suspend_handle = suspend.monitor(self.do_suspend, self.do_resume)
        self.suspend_blocker = suspend.blocker()

        # Check the externally imposed state
        self.check_flightmode(self.flightmode_watcher)
        self.check_gsmmode(self.gsmmode_watcher)

        # and GO!
        self.advance()

    def check_call(self, f = None):
        l = recall('call')
        log("Check call got", l)
        if l == "":
            if self.gstate != 'idle':
                global incoming_num
                incoming_num = None
                self.set_state('hangup')
                record('status','')
                record('incoming','')
                calllog_end('incoming')
                calllog_end('outgoing')
        elif l == 'answer':
            if self.gstate == 'incoming':
                record('status', 'on-call')
                record('incoming','')
                set_alert('ring', None)
                self.set_state('answer')
        else:
            if self.gstate == 'idle':
                global calling
                calling = True
                self.args['number'] = l
                self.set_state('call')
                calllog('outgoing',l)
                record('status', 'on-call')

    def check_dtmf(self, f = None):
        l = recall('dtmf')
        log("Check dtmf got", l)
        if len(l):
            self.args['dtmf'] = l
            self.set_state('dtmf')
            record('dtmf','')

    def check_data(self, f = None):
        l = recall('data-APN')
        log("Check data got", l)
        if l == "":
            l = None
        if self.data_APN != l:
            self.data_APN = l
            self.args['APN'] = self.data_APN
            if self.data_IP:
                self.set_state('data-hangup')
                data_log_update(True)
            elif self.gstate == 'idle' and self.data_APN:
                self.last_data_call = time.time()
                self.next_data_call = time.time()
                data_log_reset()
                self.set_state('data-call')

    def check_flightmode(self, f = None):
        try:
            fd = open("/var/lib/misc/flightmode/active")
            l = fd.read(1)
            fd.close()
        except IOError:
            l = ""
        log("check flightmode got", len(l))
        if len(l) == 0:
            if self.flightmode:
                self.flightmode = False
                if self.suspend_handle.suspended:
                    self.set_state('suspend')
                else:
                    self.set_state('init1')
        else:
            if not self.flightmode:
                self.flightmode = True
                self.set_state('to-flight')

    def check_gsmmode(self, f = None):
        try:
            fd = open("/var/lib/misc/gsm/mode")
            l = fd.readline()
            fd.close()
        except IOError:
            l = "3"
        log("check gsmmode got", l)
        if len(l) and l[0] in "012345":
            self.args['mode'] = l[0]
            if self.gstate == 'idle':
                self.set_state('refresh')

    def do_suspend(self):
        self.suspend_pending = True
        if self.gstate not in ['flight', 'resume']:
            print "do suspend sets suspend"
            self.set_state('suspend')
        else:
            print "do suspend avoids suspend"
            self.abort_timeout()
        return False

    def do_resume(self):
        if self.gstate == 'suspend':
            self.set_state('resume')

    def set_state(self, state):
        # this happens asynchronously so we must be careful
        # about changing things.  Just record the new state
        # and abort any timeout
        if state == self.gstate or state in self.nextstate:
            log("state already destined to be", state)
            return
        log("state should become", state)
        self.nextstate.append(state)
        self.abort_timeout()

    def force_state(self, state):
        # immediately go to new state - must be called carefully
        log("Force state to", state);
        self.cancel_timeout()
        self.nextstate = []
        n = len(control[state])
        self.lastrun = n * [0]
        self.tasknum = None
        self.gstate = state

    def advance(self):
        # 'advance' is called by a 'Task' when it has finished
        # It may have called 'set_state' first either to report
        # an error or to effect a regular state change
        now = int(time.time()*1000)
        if self.tasknum != None:
            self.lastrun[self.tasknum] = now
            self.tasknum = None
        (t, delay) = self.next_cmd()
        log("advance %s chooses %d, %d" % (self.gstate, t, delay))
        if delay and self.nextstate:
            # time to effect 'set_state' synchronously
            self.gstate = self.nextstate.pop(0)
            log("state becomes", self.gstate)
            n = len(control[self.gstate])
            self.lastrun = n * [0]
            t, delay = self.next_cmd()

        if delay:
            log("Sleeping for %f seconds" % (delay/1000.0))
            self.set_timeout(delay)
            if self.suspend_pending:
                # It is important that this comes after set_timeout
                # as we might get an abort_timeout as a result of the
                # release, and there needs to be a timeout to abort
                self.suspend_pending = False
                print "advance calls release"
                self.suspend_handle.release()
        else:
            self.tasknum = t
            self.state = {}
            control[self.gstate][t].start(self)

    def takeline(self, line):

        if self.extra:
            # an async message is multi-line and we need to handle
            # the extra line.
            if not self.extra.handle_extra(self, line):
                self.extra = None
            return False

        if line == None:
            self.force_state('reset')
            self.advance()
        if not line:
            return False

        # Check for an async message
        for m in async:
            mt = m.match(line)
            if mt:
                m.handle(self, line, mt)
                if m.handle_extra:
                    self.extra = m
                return False

        # else pass it to the task
        if self.tasknum != None:
            control[self.gstate][self.tasknum].takeline(self, line)

    def timedout(self):
        if self.tasknum == None:
            self.advance()
        else:
            control[self.gstate][self.tasknum].timeout(self)

    def next_cmd(self):
        # Find a command to execute, or a delay
        # return (cmd,time)
        # cmd is an index into control[state],
        # time is seconds until try something
        mindelay = 60*60*1000
        if self.gstate == None:
            return (0, mindelay)
        cs = control[self.gstate]
        n = len(cs)
        now = int(time.time()*1000)
        for i in range(n):
            if self.lastrun[i] == 0:
                return (i, 0)
            repeat = cs[i].repeat
            if repeat == None:
                repeat = 0
            elif type(repeat) != int:
                repeat = repeat(self)

            if repeat and self.lastrun[i] + repeat <= now:
                return (i, 0)
            if repeat:
                delay = (self.lastrun[i] + repeat) - now;
                if delay < mindelay:
                    mindelay = delay
        return (0, mindelay)

class CarrierDetect(AtChannel):
    # on the hso modem in the GTA04, the 'NO CARRIER' signal
    # arrives on the 'Modem' port, not on the 'Application' port.
    # So we listen to the 'Modem' port, and report any
    # 'NO CARRIER' we see - or indeed anything that we see.
    def __init__(self, path, main):
        AtChannel.__init__(self, path = path)
        self.main = main

    def takeline(self, line):
        self.main.takeline(line)

class SysfsWatcher:
    # watch for changes on a sysfs file and report them
    # We read the content, report that, wait for a change
    # and report again
    def __init__(self, path, action):
        self.path = path
        self.action = action
        self.fd = open(path, "r")
        self.watcher = gobject.io_add_watch(self.fd, gobject.IO_PRI, self.read)
        self.read()

    def read(self, *args):
        self.fd.seek(0)
        try:
            r = self.fd.read(4096)
        except IOerror:
            return True
        self.action(r)
        return True

try:
    os.mkdir("/run/gsm-state")
except:
    pass

calling = False
a = GsmD('/dev/ttyHS_Application', '/dev/ttyHS_Modem')
print "GsmD started"

try:
    f = open("/sys/class/gpio/gpio176/edge", "w")
except IOError:
    f = None
if f:
    f.write("rising")
    f.close()
    w = SysfsWatcher("/sys/class/gpio/gpio176/value",
                     lambda l: maybe_sms(l, a))
else:
    import evdev
    def check_evt(dc, mom, typ, code, val):
        if typ == 1 and val == 1:
            # keypress
            maybe_sms("", a)
    try:
        f = evdev.EvDev("/dev/input/incoming", check_evt)
    except:
        f = None
c = gobject.main_context_default()
while True:
    c.iteration()
