
#
# Handle a connection to an AT device via /dev/ttyXX
#
# We directly support high level commands (reset_modem
# etc) but don't know anything about AT commands - we just send them
# through and hand back reply.  Replies also go via a callback
# We also provide timeout support, but someone else needs to tell us
# when to set a timeout, and when to clear it.
#
# This is usually subclassed by code with an agenda.

import gobject, sys, os, time
import termios
from tracing import log
from socket import *

class AtChannel:
    def __init__(self, path):
        self.path = path
        self.connected = False
        self.watcher = None
        self.sock = None
        self.buf = ""
        self.linelist = []

        self.pending = False
        self.timer = None

    def disconnect(self):
        if self.watcher:
            gobject.source_remove(self.watcher)
            self.watcher = None
        if self.sock:
            self.sock.close()
            self.sock = None
        self.connected = False

    def connect(self, retry=10000):
        if self.sock != None:
            return
        log("connect to", self.path)
        s = None
        while s == None and retry > 0:
            retry -= 1
            try:
                s = open(self.path,"r+")
            except IOError:
                time.sleep(0.4)
                s = None
        if not s:
            return False
        #s.setblocking(0)
        fd = s.fileno()
        attr = termios.tcgetattr(fd)
        attr[3] = attr[3] & ~termios.ICANON & ~termios.ECHO & ~termios.ISIG
        attr[6][termios.VMIN] = 0
        attr[6][termios.VTIME] = 0
        termios.tcsetattr(fd, termios.TCSANOW, attr)

        self.watcher = gobject.io_add_watch(s, gobject.IO_IN, self.readdata)
        self.sock = s
        self.connected = True
        return True

    def readdata(self, io, arg):
        try:
            r = self.sock.read(1000)
        except IOError:
            # no data there really.
            return True
        if not r:
            # pipe closed
            if io != None:
                self.getline(None)
            return False
        r = self.buf + r
        ra = r.split('\n')
        self.buf = ra[-1];
        del ra[-1]
        for ln in ra:
            ln = ln.strip('\r')
            self.getline(ln)
        # FIXME this should be configurable
        if self.buf == '> ':
            self.getline(self.buf)
            self.buf = ''
        return True

    def getline(self, line):
        if line == None:
            log("received EOF")
            self.takeline(line)
            if self.pending:
                self.pending = False
                gobject.source_remove(self.timer)
                self.timer = None
            return
        if len(line):
            log("received AT response", line)
        if self.takeline(line):
            if self.pending:
                self.pending = False
                gobject.source_remove(self.timer)
                self.timer = None

    def atcmd(self, cmd, timeout = 2000):
        """
        Send the command, preceeded by 'AT' and set a timeout.
        self.takeline() should return True when the command
        has been responded to, otherwise we will call
        self.timedout() after the time.
        """
        self.set_timeout(timeout)
        log("send AT command", cmd, timeout)
        try:
            self.sock.write('AT' + cmd + '\r')
            self.sock.flush()
        except IOError:
            self.cancel_timeout()
            self.set_timeout(10)

    def timer_fired(self):
        log("Timer Fired")
        self.pending = False
        self.timer = None
        self.timedout()
        return False

    def set_timeout(self, delay):
        if self.pending:
            raise ValueError
        self.timer = gobject.timeout_add(delay, self.timer_fired)
        self.pending = True

    def cancel_timeout(self):
        if self.pending:
            gobject.source_remove(self.timer)
            self.pending = False

    def abort_timeout(self):
        if self.pending:
            self.cancel_timeout()
            self.set_timeout(0)

    # these are likely to be over-ridden by a child class
    def takeline(self, line):
        self.linelist.append(line)

    def wait_line(self, timeout):
        self.cancel_timeout()
        self.set_timeout(timeout)
        if len(self.linelist) == 0:
            self.readdata(None, None)
        c = gobject.main_context_default()
        while not self.linelist and self.pending:
            c.iteration()
        if self.linelist:
            self.cancel_timeout()
            l = self.linelist[0]
            del self.linelist[0]
            return l
        else:
            return None
    def timedout(self):
        pass


    def chat(self, mesg, resp, timeout = 1000):
        """
        Send the message (if not 'None') and wait up to
        'timeout' for one of the responses (regexp)
        Return None on timeout, or number of response.
        combined with an array of the messages received.
        """
        if mesg:
            log("send command", mesg)
            try:
                self.sock.write(mesg + '\r\n')
                self.sock.flush()
            except error:
                timeout = 10

        conv = []
        while True:
            l = self.wait_line(timeout)
            if l == None:
                return (None, conv)
            conv.append(l)
            for i in range(len(resp)):
                ptn = resp[i]
                if type(ptn) == str:
                    if ptn == l.strip():
                        return (i, conv)
                else:
                    if resp[i].match(l):
                        return (i, conv)

    def chat1(self, mesg, resp, timeout=1000):
        n,c = self.chat(mesg, resp, timeout = timeout)
        return n


def found(list, patn):
    """
    see if patn can be found in the list of strings
    """
    for l in list:
        l = l.strip()
        if type(patn) == str:
            if l == patn:
                return True
        else:
            p = patn.match(l)
            if p:
                return p
    return False

