#!/usr/bin/env python

# get list of currently available carriers from Option 3G
#
from atchan import AtChannel, found
import sys, re

chan = AtChannel(path='/dev/ttyHS_Control')
chan.connect()

if chan.chat1('ATE0', ['OK','ERROR']) != 0:
	sys.exit(1)

n,c = chan.chat('AT+COPS=?', ['OK','ERROR'], timeout=45000)
if n == None and len(c) == 0:
	# need to poke to get a response
	n,c = chan.chat('', ['OK','ERROR'], timeout=10000)
if n == None and len(c) == 0:
	# need to poke to get a response
	n,c = chan.chat('', ['OK','ERROR'], timeout=10000)

if n != 0:
	sys.exit(1)


m = found(c, re.compile('^\+COPS: (.*)'))
if m:
	clist = re.findall('\((\d+),"([^"]*)","([^"]*)","([^"]*)",(\d+)\)', m.group(1))
	for (mode, long, short, num, type) in clist:
		print num, type, '"%s" "%s"' % (short, long)

