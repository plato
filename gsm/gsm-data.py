#!/usr/bin/env python

# establish data connection on Option 3G module.
#
# AT+CGDCONT=1,"IP","${APN}"
# OK
# AT_OWANCALL=1,1,1
# OK
# _OWANCALL: 1, 1
# AT_OWANDATA?
# _OWANDATA: 1, 10.6.177.182, 0.0.0.0, 211.29.132.12, 61.88.88.88, 0.0.0.0, 0.0.0.0,144000
# ifconfig hso0 10.6.177.182
# route add default dev hso0
# echo nameserver 211.29.132.12 > /etc/resolv.conf
# echo nameserver 61.88.88.88 >> /etc/resolv.conf
#
# ...but....
# we might already be connected, and we might not know, and just
# stopping/starting doesn't seem to do much - no notification.
# so:
# - first check status.  If up, configure and done
# - if not, enable and wait.
# - on timeout, check status again.

#APN="exetel1"
APN="INTERNET"
import atchan, sys, re, os

def check_status(chan):
    n,c = chan.chat('AT_OWANDATA?', ['OK','ERROR'])
    want = re.compile('_OWANDATA: 1, ([0-9.]+), [0-9.]+, ([0-9.]+), ([0-9.]+), [0-9.]+, [0-9.]+,\d+$')
    if n == 0:
        m = atchan.found(c, want)
    if n != 0 or not m:
        return False
    return m

def configure(m):
    g = m.groups()
    ip = g[0]
    ns1= g[1]
    ns2= g[2]
    print ip, ns1, ns2
    os.system("/sbin/ifconfig hso0 %s" % ip)
    os.system('route add default dev hso0')
    f = open("/etc/resolv.conf", "w")
    f.write("nameserver %s\n" % ns1)
    f.write("nameserver %s\n" % ns2)
    f.close()

def disconnect(chan):
    n,c = chan.chat('AT_OWANCALL=1,0,0', ['OK','ERROR'])

chan = atchan.AtChannel(path="/dev/ttyHS_Control")
chan.connect()

chan.chat1('ATE0', ['OK','ERROR'])
if chan.chat1('AT+CGDCONT=1,"IP","%s"' % APN, ['OK','ERROR']) != 0:
    print 'Could not set APN'
    sys.exit(1)

m = check_status(chan)

if sys.argv[1] == 'status':
    if m:
        print "Active: ", m.groups()[0]
    else:
        print "inactive"
    sys.exit(0)
if sys.argv[1] == "off":
    if m:
        disconnect(chan)
    else:
        print 'DATA already disconnected'
    os.system('route delete default dev hso0')
    os.system('/sbin/ifconfig hso0 down')
    sys.exit(0)

if m:
    print 'already active'
    configure(m)
    sys.exit(0)

want = re.compile('^_OWANCALL: *\d+, *(\d*)$')

if chan.chat1('AT_OWANCALL=1,1,1', ['OK','ERROR']) != 0:
    print 'Could not start data connection'
    sys.exit(1)
l = chan.wait_line(10000)
if l == None:
    print 'No response for DATA connect'
else:
    m = want.match(l)
    if m and m.groups()[0] == '1':
        print 'Connected'
    else:
        print 'Connect failed'
        sys.exit(1)

m = check_status(chan)
if m:
    configure(m)
else:
    print 'Sorry, could not connect'
    #disconnect(chan)
