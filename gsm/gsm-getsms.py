#!/usr/bin/env python
# coding=UTF-8

# Collect SMS messages from the GSM device.
# We store a list of messages that are thought to be
# in the SIM card: number from date
#  e.g.   17 61403xxxxxx 09/02/17,20:28:36+44
# As we read messages, if we find one that is not in that list,
# we record it in the SMS store, then update the list
#
# An option can specify either 'new' or 'all.
# 'new' will only ask for 'REC UNREAD' and so will be faster and so
# is appropriate when we know that a new message has arrived.
# 'all' reads all messages and so is appropriate for an occasional
# 'sync' like when first turning the phone on.
#
# If we discover that the SMS card is more than half full, we
# deleted the oldest messages.
# We discover this by 'all' finding lots of messages, or 'new'
# finding a message with a high index.
# For now, we "know" that the SIM card can hold 30 messages.
#
# We need to be careful about long messages.  A multi-part message
# looks like e.g.
#+CMGL: 19,"REC UNREAD","61403xxxxxx",,"09/02/18,10:51:46+44",145,140
#0500031C0201A8E8F41C949E83C2207B599E07B1DFEE33A85D9ECFC3E732888E0ED34165FCB85C26CF41747419344FBBCFEC32A85D9ECFC3E732889D6EA7E9A0F91B444787E9A024681C7683E6E532E88E0ED341E939485E1E97D3F6321914A683E8E832E84D4797E5A0B29B0C7ABB41ED3CC8282F9741F2BADB5D96BB40D7329B0D9AD3D36C36887E2FBBE9
#+CMGL: 20,"REC UNREAD","61403xxxxxx",,"09/02/18,10:51:47+44",145,30
#0500031C0202F2A0B71C347F83D8653ABD2C9F83E86FD0F90D72B95C2E17

# what about:
# 0281F001000081000019C9531BF4AED3E769721944479741F4F7DD0D4287D96C
# 02 81F0 ??
#       010000810000
#                   19 (25 bytes)
#                     C9531BF4AED3E769721944479741F4F7DD0D4287D96C
#                     I'm outside the town hall
# This is a saved message.
#
# If that was just hex you could use
#  perl -e 'print pack("H*","050....")'
# to print it.. but no...
# Looks like it decodes as:
# 05  - length of header, not including this byte
# 00  - concatentated SMS with 8 bit ref number (08 means 16 bit ref number)
# 03  - length of rest of header
# 1C  - ref number for this concat-SMS
# 02  - number of parts in this SMS
# 01  - number of this part - counting starts from 1
# A8E8F41C949E83C22.... message, 7 bits per char. so:
#  A8  - 54 *2 + 0   54 == T           1010100 0     1010100
# 0E8  - 68 *2 + 0   68 == h           1 1101000     1101000
#  F4  -             69 == i           11 110100     1101001  1
#  1C                73 == s           000 11100     1110011  11
#  94                20 == space       1001 0100     0100000  000
#  9E                69 == i           10011 110     1101001  1001
#  83                73 == s           100000 11     1110011  10011
#                    20 == space                    0100000   0100000

# 153 characters in first message. 19*8 + 1
# that uses 19*7+1 == 134 octets
# There are 6 in the header so a total of 140
# second message has 27 letters - 3*8+3
# That requires 3*7+3 == 24 octets.  30 with the 6 octet header.

# then there are VCARD messages that look lie e.g.
#+CMGL: 2,"REC READ","61403xxxxxx",,"09/01/29,13:01:26+44",145,137
#06050423F40000424547494E3A56434152440D0A56455253494F4E3A322E310D0A4E3A....0D0A454E443A56434152440D0A
#which is
#06050423F40000
#then
#BEGIN:VCARD
#VERSION:2.1
#N: ...
#...
#END:VCARD
# The 06050423f40000
# might decode like:
#  06  - length of rest of header
#  05  - magic code meaning 'user data'
#  04  - length of rest of header...
#  23  - 
#  f4  -  destination port '23f4' means 'vcard'
#  00  -   
#  00  -  0000 is the origin port.
#
#in hex/ascii
#
# For now, ignore anything longer than the specified length.

import os
import suspend
#os.environ['PYTRACE'] = '1'

import atchan, sys, re
from storesms import SMSmesg, SMSstore


def load_mirror(filename):
    # load an array of index address date
    # from the file and store in a hash
    rv = {}
    try:
        f = file(filename)
    except IOError:
        return rv
    l = f.readline()
    while l:
        fields = l.strip().split(None, 1)
        rv[fields[0]] = fields[1]
        l = f.readline()
    return rv

def save_mirror(filename, hash):
    n = filename + '.new'
    f = open(n, 'w')
    for i in hash:
        f.write(i + ' ' + hash[i] + '\n')
    f.close()
    os.rename(n, filename)

# GSM uses a 7-bit code that is not the same as ASCII...
# -*- coding: utf8 -*- 
gsm = (u"@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞ\x1bÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?"
       u"¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ`¿abcdefghijklmnopqrstuvwxyzäöñüà")
ext = (u"````````````````````^```````````````````{}`````\\````````````[~]`"
       u"|````````````````````````````````````€``````````````````````````")

# Take a unicode string and produce a byte string for GSM
def gsm_encode(plaintext):
    res = ""
    for c in plaintext:
        idx = gsm.find(c);
        if idx != -1:
            res += chr(idx)
            continue
        idx = ext.find(c)
        if idx != -1:
            res += chr(27)
            res += chr(idx)
    return res
# take a GSM byte string (7-in-8 conversion already done) and produce unicode
def gsm_decode(code):
    uni = u''
    esc = False
    for c in code:
        n = ord(c)
        if esc:
            uni += ext[n]
            esc = False
        elif n == 27:
            esc = True
        else:
            uni += gsm[n]
    return uni


def sms_decode(msg,pos = 1):
    #msg is a 7-in-8 encoding of a longer message.
    carry = 0
    str = ''
    while msg:
        c = msg[0:2]
        msg = msg[2:]
        b = int(c, 16)
        if pos == 0:
            if carry:
                str += chr(carry + (b&1)*64)
                carry = 0
            b /= 2
        else:
            b = (b << (pos-1)) | carry
            carry = (b & 0xff80) >> 7
            b &= 0x7f
        if (b & 0x7f) != 0:
            str += chr(b&0x7f)
        pos = (pos+1) % 7
    return gsm_decode(str)

def sms_unicode_decode(msg):
    # 2bytes unicode numbers - 4 syms each
    m = u''
    for i in range(len(msg)/4):
        c = int(msg[i*4:i*4+4],16)
        m += unichr(c)
    return m

def cvt_telnum(type, msg, len):
    if type == '81' or type  == '91':
        n = ''
        for i in range(len):
            n += msg[i + 1 - (i%2)*2]
        if type == '91':
            return '+' + n
        else:
            return n
    if type == 'D0':
        return sms_decode(msg)
    return "?" + type + msg

def cvt_date(msg):
    #YYMMDDHHMMSSZZ -> 20YY/MM/DD HH:MM:SS+ZZ swapping letters
    sep='0//,::+'
    dt = '2'
    for i in range(len(msg)/2):
        dt += sep[i] + msg[i*2+1] + msg[i*2]
    return dt

def recall(key, nofile = ""):
    try:
        fd = open("/run/gsm-state/" + key)
        l = fd.read(1000)
        l = l.strip()
        fd.close()
    except IOError:
        l = nofile
    return l

def main():
    mode = 'all'
    for a in sys.argv[1:]:
        if a == '-n':
            mode = 'new'
        else:
            print "Unknown option:", a
            sys.exit(1)

    pth = None
    for p in ['/data','/media/card','/var/tmp']:
        if os.path.exists(os.path.join(p,'SMS')):
            pth = p
            break

    if not pth:
        print "Cannot find SMS directory"
        sys.exit(1)

    dir = os.path.join(pth, 'SMS')
    print "Using ", dir
    store = SMSstore(dir)

    chan = atchan.AtChannel(path = '/dev/ttyHS_Control')
    chan.connect()
    
    # consume any extra 'OK' that might be present
    chan.chat1('', ['OK', 'ERROR']);
    if chan.chat1('ATE0', ['OK', 'ERROR']) != 0:
        sys.exit(1)

    # get ID of SIM card
    CIMI = recall('sim')
    if not CIMI or CIMI == 'unknown':
        sys.exit(1)

    mfile = os.path.join(dir, '.sim-mirror-'+CIMI)
    #FIXME lock mirror file
    mirror = load_mirror(mfile)

    chan.chat('AT+CMGF=0', ['OK','ERROR'])
    if mode == 'new':
        chan.atcmd('+CMGL=0')
    else:
        chan.atcmd('+CMGL=4')

    # reading the msg list might fail for some reason, so
    # we always prime the mirror list with the previous version
    # and only replace things, never assume they aren't there
    # because we cannot see them
    newmirror = mirror
    mirror_seen = {}

    l = ''
    state = 'waiting'
    msg = ''
    # text format
    #                            indx  state    from          name       date          type len
    #+CMGL: 40,"REC READ","+61406022084",,"12/03/14,18:00:40+44"
    # PDU MODE
    #+CMGL: index, 0-4, unread read unsent sent all, ?? , byte len after header
    #+CMGL: 3,1,,40
    #07911614786007F0 040B911654999946F100002120217035534417CE729ACD02BDD7203A3AEC5ECF5D2062D9ED4ECF01
    #                       61450000641F000012021207533544
    # 07 is length (octets) of header (911614786007F0)
    #  91 is SMSC number type; 81 is local? 91 is international D0 is textual D0456C915A6402 == EXETEL
    #  1614786007F0 is the SMSC number: 61418706700
    #
    #   04 is "SMS-DELIVER" and some other details  44 == ?? 24??
    #    0B is length of sende number (11)
    #     91 is type as above
    #      1654999946F1 is number: 61459999641 (F is padding)
    #       00 is protocol id - always 0 ??
    #        00 is Data coding scheme.  00 is 7-bit default
    #         21202170355344 is stime stamp: 12/02/12 07:35:53+44
    #          17 is length of body (23)
    #           CE729ACD02BDD7203A3AEC5ECF5D2062D9ED4ECF01 is 7-in-8 message
    #
    # other coding schemes:
    #  08 is 16 bit unicode
    #  11 is VCARD: 06050400E20080 C2E231E9D459874129B1A170EA886176B9A1A016993A182D26B3E164B919AEA1283A893AEB3028253614
    #         looks like 7-in-8 with a 7/8 header 
    #      or can be just a message.
    #     or ??? (message from AUST POST)
    #  01 is much like 00 ??


    want = re.compile('^\+CMGL: (\d+),(\d+),("[^"]*")?,(\d+)$')

    found_one = False
    while state == 'reading' or not (l[0:2] == 'OK' or l[0:5] == 'ERROR' or
                                     l[0:10] == '+CMS ERROR'):
        l = chan.wait_line(1000)
        if l == None:
            sys.exit(1)
        print "Got (",state,")", l
        if state == 'reading':
            msg = l
            if designation != '0' and designation != '1':
                #send, not recv
                state = 'waiting'
                continue
            if len(msg) >= msg_len:
                state = 'waiting'
                hlen = int(msg[0:2], 16)
                hdr = msg[2:(2+hlen*2)]
                # hdr is the sending number - don't care
                msg = msg[2+hlen*2:]
                # typ == 04 - SMS-DELIVER
                typ = int(msg[0:2],16)
                nlen = int(msg[2:4], 16)
                ntype = msg[4:6]
                numlen = (nlen + nlen % 2)
                sender = cvt_telnum(ntype, msg[6:6+numlen], nlen)
                msg = msg[6+numlen:]
                proto = msg[0:2]
                coding = msg[2:4]
                date = cvt_date(msg[4:18])
                bdy_len = int(msg[18:20], 16)
                body = msg[20:]
                ref = None; part = None

                if body[0:6] == '050003':
                    # concatenated message with 8bit ref number
                    ref  = body[6:8]
                    part = ( int(body[10:12],16), int(body[8:10], 16))
                    if coding == '08':
                        txt = sms_unicode_decode(body[12:])
                    else:
                        txt = sms_decode(body[12:],0)
                elif body[0:6] == '060504':
                    # VCARD ??
                    txt = sms_decode(body[14:])
                elif coding == '00':
                    txt = sms_decode(body)
                elif coding == '11':
                    txt = sms_decode(body)
                elif coding == '01':
                    txt = sms_decode(body)
                elif coding == '08':
                    txt = sms_unicode_decode(body)
                else:
                    print "ignoring", index, sender, date, body
                    continue
                if ref == None:
                    print "found", index, sender, date, txt.encode('utf-8')
                else:
                    print "found", index, ref, part, sender, date, repr(txt)

                if index in mirror and mirror[index] == date[2:] + ' ' + sender:
                    print "Already have that one"
                else:
                    sms = SMSmesg(source='GSM', time=date[2:], sender=sender,
                                  text = txt.encode('utf-8'), state = 'NEW',
                                  ref= ref, part = part)
                    store.store(sms)
                    found_one = True
                newmirror[index] = date[2:] + ' ' + sender
                mirror_seen[index] = date[2:] + ' ' + sender
        else:
            m = want.match(l)
            if m:
                g = m.groups()
                index = g[0]
                designation = g[1]
                msg_len = int(g[3], 10)
                msg = ''
                state = 'reading'

    mirror = newmirror

    if len(mirror) > 10:
        rev = {}
        dlist = []
        for i in mirror:
            rev[mirror[i]+' '+str(i)] = i
            dlist.append(mirror[i]+' '+str(i))
        dlist.sort()
        for i in range(len(mirror) - 10):
            dt=dlist[i]
            ind = rev[dt]
            print 'del', i, dt, ind
            resp = chan.chat1('AT+CMGD=%s' % ind, ['OK', 'ERROR', '+CMS ERROR'],
                              timeout=3000)
            if resp == 0 or ind not in mirror_seen:
                del mirror[ind]

    save_mirror(mfile, mirror)
    if found_one:
        try:
            f = open("/run/alert/sms", "w")
            f.write("new")
            f.close()
            suspend.abort_cycle()
        except:
            pass
    if mode == 'new' and not found_one:
        sys.exit(1)
    sys.exit(0)

main()
