#
# FIXME
#  - remove old multipart files
#
# Store SMS messages is a bunch of files, one per month.
# Each message is stored on one line with space separated .
# URL encoding (%XX) is used to quote white space, unprintables etc
# We store 5 fields:
# - time stamp that we first saw the message.  This is in UTC.
#   This is the primary key.  If a second message is seen in the same second,
#   we quietly add 1 to the second.
# - Source, one of 'LOCAL' for locally composed, 'GSM' for recieved via GSM
#   or maybe 'EMAIL' if received via email??
# - Time message was sent, Localtime with -TZ.  For GSM messages this comes with the
#   message. For 'LOCAL' it might be '-', or will be the time we succeeded
#   in sending.
#   time is stored as a tupple (Y m d H M S Z) where Z is timezone in multiples
#   of 15 minutes.
# - The correspondent: sender if GSM, recipient if LOCAL, or '-' if not sent.
#     This might be a comma-separated list of recipients.
# - The text of the message
#
# Times are formatted %Y%m%d-%H%M%S and local time has a  GSM TZ suffix.
# GSM TZ is from +48 to -48 in units of 15 minutes. (0 is +00)
#
# We never modify a message once it has been stored.
# If we have a draft that we edit and send, we delete the draft and
# create a new sent-message
# If we forward a message, we will then have two copies.
#
# New messages are not distinguished by a flag (which would have to be cleared)
# but by being in a separate list of new messages.
# We havea list of 'new' messages and a list of 'draft' messages.
#
# Multi-part messages are accumulated as they are received.  The quoted message
# contains <N>text for each part of the message.
#  e.g. <1><2>nd%20so%20no.....<3>
# if we only have part 2 of 3.
# For each incomplete message there is a file (like 'draft' and 'newmesg') named
# for the message which provides an index to each incomplete message.
# It will be named e.g. 'multipart-1C' when 1C is the message id.
#
# This module defines 2 classes:
# SMSmesg
#   This holds a message and so has timestamp, source, time, correspondent
#    and text fields.  These are decoded.
#   SMSmesg also has 'state' which can be one of "NEW", "DRAFT" or None
#   Finally it might have a 'ref' and a 'part' which is a tuple (this,max)
#    This is only used when storing the message to link it up with
#    a partner
#
# SMSstore
#  This represents a collection of messages in a directory (one file per month)
#  and provides lists of 'NEW' and 'DRAFT' messages.
#  Operations:
#     store(SMSmesg,  NEW|DRAFT|) -> None
#       stores the message and sets the timestamp
#  lookup(latest-time, NEW|DRAFT|ALL) -> (earlytime, [SMSmesg])
#     collects a list of messages in reverse time order with times no later
#     than 'latest-time'.  Only consider NEW or DRAFT or ALL messages.
#     The list may not be complete (typically one month at a time are returnned)
#      If you want more, call again with 'earlytime' as 'latest-time').
#  delete(SMSmesg)
#     delete the given message (based on the timestamp only)
#  setstate(SMSmesg, NEW|DRAFT|None)
#     update the 'new' and 'draft' lists or container, or not container, this
#     message.
#
#

import os, fcntl, re, time, urllib

def umktime(tm):
    # like time.mktime, but tm is UTC
    # So we want a 't' where
    #  time.gmtime(t)[0:6] == tm[0:6]
    estimate = time.mktime(tm) - time.timezone
    t2 = time.gmtime(estimate)
    while t2[0:6] < tm[0:6]:
        estimate += 15*60
        t2 = time.gmtime(estimate)
    while t2[0:6] > tm[0:6]:
        estimate -= 15*60
        t2 = time.gmtime(estimate)
    return estimate

def parse_time(strg):
    return int(umktime(time.strptime(strg, "%Y%m%d-%H%M%S")))
def parse_ltime(strg):
    n=3
    if strg[-2] == '+' or strg[-2] == '-':
        n=2
    z = strg[-n:]
    return time.strptime(strg[:-n], "%Y%m%d-%H%M%S")[0:6] + (int(z),)
def format_time(t):
    return time.strftime("%Y%m%d-%H%M%S", time.gmtime(t))
def format_ltime(tm):
    return time.strftime("%Y%m%d-%H%M%S", tm[0:6]+(0,0,0)) + ("%+03d" % tm[6])

class SMSmesg:
    def __init__(self, **a):
        if len(a) == 1 and 'line' in a:
            # line read from a file, with 5 fields.
            #  stamp, source, time, correspondent, text
            line = a['line'].split()
            self.stamp = parse_time(line[0])
            self.source = line[1]
            self.time = parse_ltime(line[2])
            self.correspondents = []
            for c in line[3].split(','):
                if c != '-':
                    self.correspondents.append(urllib.unquote(c))
            self.set_corresponent()
            self.state = None

            self.parts = None
            txt = line[4]
            if txt[0] != '<':
                self.text = urllib.unquote(txt)
                return
            # multipart:   <1>text...<2>text...<3><4>
            m = re.findall('<(\d+)>([^<]*)', txt)
            parts = []
            for (pos, strg) in m:
                p = int(pos)
                while len(parts) < p:
                    parts.append(None)
                if strg:
                    parts[p-1] = urllib.unquote(strg)
            self.parts = parts
            self.reduce_parts()
        else:
            self.stamp = int(time.time())
            self.source = None
            lt = time.localtime()
            z = time.timezone/15/60
            if lt[8] == 1:
                z -= 4
            self.time = time.localtime()[0:6] + (z,)
            self.correspondents = []
            self.text = ""
            self.state = None
            self.ref = None
            self.parts = None
            part = None
            for k in a:
                if k == 'stamp':
                    self.stamp = a[k]
                elif k == 'source':
                    self.source = a[k]
                elif k == 'time':
                    # time can be a GSM string: 09/02/09,09:56:28+44 (ymd,HMS+z)
                    # or a tuple (y,m,d,H,M,S,z)
                    if type(a[k]) == str:
                        t = a[k][:-3]
                        z = a[k][-3:]
                        tm = time.strptime(t, "%y/%m/%d,%H:%M:%S")
                        self.time = tm[0:6] + (int(z),)
                elif k == 'to' or k == 'sender':
                    if self.source == None:
                        if k == 'to':
                            self.source = 'LOCAL'
                        if k == 'sender':
                            self.source = 'GSM'
                    self.correspondents = [ a[k] ]
                elif k == 'correspondents':
                    self.correspondents = a[k]
                elif k == 'text':
                    self.text = a[k]
                elif k == 'state':
                    self.state = a[k]
                elif k == 'ref':
                    if a[k] != None:
                        self.ref = a[k]
                elif k == 'part':
                    if a[k]:
                        part = a[k]
                else:
                    raise ValueError
            if self.source == None:
                self.source = 'LOCAL'
            if part:
                self.parts = [None for x in range(part[1])]
                self.parts[part[0]-1] = self.text
                self.reduce_parts()
            self.set_corresponent()

        self.month_re = re.compile("^[0-9]{6}$")

    def reduce_parts(self):
        def reduce_pair(a,b):
            if b == None:
                b = "...part of message missing..."
            if a == None:
                return b
            return a+b
        self.text = reduce(reduce_pair, self.parts)

    def set_corresponent(self):
        if len(self.correspondents) == 1:
            self.correspondent = self.correspondents[0]
        elif len(self.correspondents) == 0:
            self.correspondent = "Unsent"
        else:
            self.correspondent = "Multiple"

    def format(self):
        fmt =  "%s %s %s %s " % (format_time(self.stamp), self.source,
                                   format_ltime(self.time),
                                   self.format_correspondents())
        if not self.parts:
            return fmt + urllib.quote(self.text)

        for i in range(len(self.parts)):
            fmt += ("<%d>" % (i+1)) + urllib.quote(self.parts[i]) if self.parts[i] else ""
        return fmt

    def format_correspondents(self):
        r = ""
        for i in self.correspondents:
            if i:
                r += ',' + urllib.quote(i)
        if r:
            return r[1:]
        else:
            return '-'

class SMSstore:
    def __init__(self, dir):
        self.month_re = re.compile("^[0-9]{6}$")
        self.cached_month = None
        self.dirname = dir
        # find message files
        self.set_files()
        self.drafts = self.load_list('draft')
        self.newmesg = self.load_list('newmesg')

    def load_list(self, name, update = None, *args):

        l = []
        try:
            f = open(self.dirname + '/' + name, 'r+')
        except IOError:
            return l

        if update:
            fcntl.lockf(f, fcntl.LOCK_EX)
        else:
            fcntl.lockf(f, fcntl.LOCK_SH)
        for ln in f:
            l.append(parse_time(ln.strip()))
        l.sort()
        l.reverse()

        if update and update(l, *args):
            f2 = open(self.dirname + '/' + name + '.new', 'w')
            for t in l:
                f2.write(format_time(t)+"\n")
            f2.close()
            os.rename(self.dirname + '/' + name + '.new',
                      self.dirname + '/' + name)
        f.close()
        return l

    def load_month(self, f, do_lock):
        # load the messages from f, which is open for read
        rv = {}
        if do_lock:
            fcntl.lockf(f, do_lock)
        for l in f:
            l.strip()
            m = SMSmesg(line=l)
            rv[m.stamp] = m
            if m.stamp in self.drafts:
                m.state = 'DRAFT'
            elif m.stamp in self.newmesg:
                m.state = 'NEW'
        return rv

    def store_month(self, l, m):
        dm = self.dirname + '/' + m
        f = open(dm+'.new', 'w')
        for s in l:
            f.write(l[s].format() + "\n")
        f.close()
        os.rename(dm+'.new', dm)
        if not m in self.files:
            self.files.append(m)
            self.files.sort()
            self.files.reverse()
        if self.cached_month == m:
            self.cache = l

    def store(self, sms):
        orig = None
        if sms.ref != None:
            # This is part of a multipart.
            # If there already exists part of this
            # merge them together
            #
            times = self.load_list('multipart-' + sms.ref)
            if len(times) == 1:
                orig = self.load(times[0])
                if orig and orig.parts:
                    for i in range(len(sms.parts)):
                        if sms.parts[i] == None and i < len(orig.parts):
                            sms.parts[i] = orig.parts[i]
                else:
                    orig = None

        m = time.strftime("%Y%m", time.gmtime(sms.stamp))
        try:
            f = open(self.dirname + '/' + m, "r+")
        except:
            f = open(self.dirname + '/' + m, "w+")
        complete = True
        if sms.ref != None:
            for i in sms.parts:
                if i == None:
                    complete = False
            if complete:
                sms.reduce_parts()
                sms.parts = None

        fcntl.lockf(f, fcntl.LOCK_EX)
        l = self.load_month(f, 0)
        while sms.stamp in l:
            sms.stamp += 1
        l[sms.stamp] = sms
        self.store_month(l, m);
        f.close()

        if orig:
            self.delete(orig)
        if sms.ref != None:
            if complete:
                try:
                    os.unlink(self.dirname + '/multipart-' + sms.ref)
                except:
                    pass
            elif orig:
                def replacewith(l, tm):
                    while len(l):
                        l.pop()
                    l.append(tm)
                    return True
                self.load_list('multipart-' + sms.ref, replacewith, sms.stamp)
            else:
                f = open(self.dirname +'/multipart-' + sms.ref, 'w')
                fcntl.lockf(f, fcntl.LOCK_EX)
                f.write(format_time(sms.stamp) + '\n')
                f.close()

        if sms.state == 'NEW' or sms.state == 'DRAFT':
            s = 'newmesg'
            if sms.state == 'DRAFT':
                s = 'draft'
            f = open(self.dirname +'/' + s, 'a')
            fcntl.lockf(f, fcntl.LOCK_EX)
            f.write(format_time(sms.stamp) + '\n')
            f.close()
        elif sms.state != None:
            raise ValueError

    def set_files(self):
        self.files = []
        for f in os.listdir(self.dirname):
            if self.month_re.match(f):
                self.files.append(f)
        self.files.sort()
        self.files.reverse()

    def lookup(self, lasttime = None, state = None):
        if lasttime == None:
            lasttime = int(time.time() + 3600)
        if state == None:
            return self.getmesgs(lasttime)
        if state == 'DRAFT':
            self.drafts = self.load_list('draft')
            times = self.drafts
        elif state == 'NEW':
            self.newmesg = self.load_list('newmesg')
            times = self.newmesg
        else:
            raise ValueError

        self.set_files()
        self.cached_month = None
        self.cache = None
        rv = []
        for t in times:
            if t > lasttime:
                continue
            s = self.load(t)
            if s:
                s.state = state
                rv.append(s)
            elif state == 'NEW' or state == 'DRAFT':
                def del1(l, tm):
                    if tm in l:
                        l.remove(tm)
                        return True
                    return False
                self.load_list('draft', del1, t)
                self.load_list('newmesg', del1, t)

        return(0, rv)

    def getmesgs(self, last):
        rv = []
        for m in self.files:
            t = parse_time(m + '01-000000')
            if t > last:
                continue
            mon = self.load_month(open(self.dirname + '/' + m), fcntl.LOCK_SH)
            for mt in mon:
                if mt <= last:
                    rv.append(mon[mt])
            if rv:
                rv.sort(cmp = lambda x,y:cmp(y.stamp, x.stamp))
                return (t-1, rv)
        return (0, [])

    def load(self, t):
        m = time.strftime("%Y%m", time.gmtime(t))
        if not m in self.files:
            return None
        if self.cached_month != m:
            self.cached_month = m
            self.cache = self.load_month(open(self.dirname + '/' + m), fcntl.LOCK_SH)
        if t in self.cache:
            return self.cache[t]
        return None

    def delete(self, msg):
        if isinstance(msg, SMSmesg):
            tm = msg.stamp
        else:
            tm = msg
        m = time.strftime("%Y%m", time.gmtime(tm))
        try:
            f = open(self.dirname + '/' + m, "r+")
        except:
            return

        fcntl.lockf(f, fcntl.LOCK_EX)
        l = self.load_month(f, 0)
        if tm in l:
            del l[tm]
        self.store_month(l, m);
        f.close()

        def del1(l, tm):
            if tm in l:
                l.remove(tm)
                return True
            return False

        self.drafts = self.load_list('draft', del1, tm)
        self.newmesg = self.load_list('newmesg', del1, tm)

    def setstate(self, msg, state):
        tm = msg.stamp

        def del1(l, tm):
            if tm in l:
                l.remove(tm)
                return True
            return False

        if tm in self.drafts and state != 'DRAFT':
            self.drafts = self.load_list('draft', del1, tm)
        if tm in self.newmesg and state != 'NEW':
            self.newmesg = self.load_list('newmesg', del1, tm)

        if tm not in self.drafts and state == 'DRAFT':
            f = open(self.dirname +'/draft', 'a')
            fcntl.lockf(f, fcntl.LOCK_EX)
            f.write(format_time(sms.stamp) + '\n')
            f.close()
            self.drafts.append(tm)
            self.drafts.sort()
            self.drafts.reverse()

        if tm not in self.newmesg and state == 'NEW':
            f = open(self.dirname +'/newmesg', 'a')
            fcntl.lockf(f, fcntl.LOCK_EX)
            f.write(format_time(sms.stamp) + '\n')
            f.close()
            self.newmesg.append(tm)
            self.newmesg.sort()
            self.newmesg.reverse()

##
# load and save lists of known SMS messages from a 'mirror' file.
def load_mirror(filename):
    # load an array of index address date
    # from the file and store in a hash
    rv = {}
    try:
        f = file(filename)
    except IOError:
        return rv
    l = f.readline()
    while l:
        fields = l.strip().split(None, 1)
        rv[fields[0]] = fields[1]
        l = f.readline()
    return rv

def save_mirror(filename, hash):
    n = filename + '.new'
    f = open(n, 'w')
    for i in hash:
        f.write(i + ' ' + hash[i] + '\n')
    f.close()
    os.rename(n, filename)

def find_sms():
    pth = None
    for p in ['/data','/media/card','/var/tmp']:
        if os.path.exists(os.path.join(p,'SMS')):
            pth = p
            break
    return pth

def sms_update(messages, sim):
    '''The given messages were read from the given SIM.
    We need to compare against the 'mirror' for that SIM
    and store any new messages in an appropriate store.
    'messages' is a hash index by message number containing
    a tuple:  sender, date, txt, ref, part
    '''

    path = find_sms()
    if not path:
        return None, None
    dir = os.path.join(path, 'SMS')
    store = SMSstore(dir)
    mfile = os.path.join(dir, '.sim-mirror-'+sim)
    mirror = load_mirror(mfile)
    mirror_seen = {}
    found_one = []
    for index in messages:
        sender, date, txt, ref, part = messages[index]
        k = date[2:] + ' ' + sender
        if index not in mirror or mirror[index] != k:
            sms = SMSmesg(source='GSM', time=date[2:],
                          sender=sender, text=txt.encode('utf-8'),
                          state='NEW', ref=ref, part=part)
            store.store(sms)
            found_one.append(sender)
            mirror[index] = k
        mirror_seen[index] = k

    if len(mirror_seen) > 5:
        save_mirror(mfile, mirror_seen)
        mirror = mirror_seen
    else:
        save_mirror(mfile, mirror)
    # Now return list that could suitably be deleted
    rev = {}
    dlist = []
    for i in mirror:
        k = mirror[i]+' '+str(i)
        rev[k] = i
        dlist.append(k)
    dlist.sort()
    age_order = []
    for i in dlist:
        age_order.append(rev[i])
    return found_one, age_order
