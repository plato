/*
 * selectable, auto-scrolling list widget
 *
 * We display a list of texts and allow one to be selected,
 * normally with a tap.
 * The selected text is highlighted and the list auto-scrolls to
 * ensure it is not too close to the edge, so no other scroll
 * function is needed.
 *
 * The list is defined by a function that takes a number
 * and returns the element.
 * This element will point to a table of functions that
 * measure the size of the entry and render it.  Some standard
 * functions are provided to help with this.
 * Each element has space to store current size/location for
 * tap lookups.
 */
#include <unistd.h>
#include <stdlib.h>

#include <string.h>
#include <malloc.h>

#include <gtk/gtk.h>

#include "listsel.h"

void calc_layout_no(struct sellist *list)
{
	/* choose an appropriate 'top', 'last' and cols
	 * set x,y,width,height on each entry in that range.
	 *
	 * Starting at 'sel' (or zero) we walk backwards
	 * towards 'top' assessing size.
	 * If we hit 'top' while in first 1/3 of display
	 * we target being just above bottom third
	 * If we don't reach 'top' while above bottom
	 * 1/3, then we target just below the top 1/3,
	 * having recorded where 'top' would be (When we first
	 * reached 1/3 from 'sel')
	 * Once we have chosen top, we move to end to find
	 * bottom.  If we hit end before bottom,
	 * we need to move top backwards if possible.
	 *
	 * So:
	 * - walk back from 'sel' until find
	 *    'top' or distance above sel exceeds 2/3
	 *    record location where distance was last < 1/3
	 * - if found top and distance < 1/3 and top not start,
	 *    step back while that is still < 1/3
	 * - elif distance exceeds 2/3, set top et al to the
	 *   found 1/3 position
	 * - move down from 'sel' until total distance is
	 *   height, or hit end.
	 * - if hit end, move top backwards while total distance
	 *   still less than height.
	 *
	 * columns add a complication as we don't know how
	 * many columns to use until we know the max width of
	 * the display choices.
	 * I think we assume cols based on selected entry,
	 * and as soon as we find something that decreases
	 * col count, start again from top.
	 *
	 * That doesn't work so well, particularly with multiple
	 * columns - we don't see the col-breaks properly.
	 * To see them, we really need to chose a firm starting
	 * place.
	 * So:
	 *  Start with 'top' at beginning and walk along.
	 *   If we hit end of list before bottom and top!=0,
	 *   walk backwards from end above bottom to set top.
	 *  If we find 'sel' after first 1/3 and before last - good.
	 *  If before first third, set 'sel' above last third
	 *  and walk back until find beginning or start, set top there.
	 *  If after last third, set 'sel' past first third,
	 *  walk back to top, set top there.
	 *
	 */
	int top, last;
	int sel;
	int i;
	int cols = 1, col, row;
	struct list_entry *e;
	int w, h;
	int dist, pos, colwidth;
	int altdist, alttop;

	sel = list->selected;
	if (sel < 0)
		sel = 0;
	e = list->han->getitem(list->list, sel);
	if (e == NULL && sel > 0) {
		sel = 0;
		e = list->han->getitem(list->list, sel);
	}
	if (e == NULL)
		/* list is empty */
		return;

	top = list->top;
	if (top < 0)
		top = 0;
	if (top > sel)
		top = sel;

	/* lay things out from 'top' */
	i = top; col = 0; row = 0;
	while (col < cols) {
		e = list->han->getitem(list->list, i);
		if (e == NULL)
			break;
		list->han->get_size(e, &w, &h);
		e->x = col * list->width / cols;
		e->y = row;
		e->width = list->width / cols;
		row += h;
		if (row > list->height) {
			col++;
			e->x = col * list->width / cols;
			e->y = 0;
			row = h;
		}
		e->need_draw = 1;
	}

	list->han->get_size(e, &w, &h);
	cols = list->width / w;
	if (cols == 0)
		cols = 1;
col_retry:
	dist = 0;
	i = sel;
	e = list->han->getitem(list->list, i);
	list->han->get_size(e, &w, &h);
	dist += h;

	while (i > top && dist < (cols-1) * list->height + 2 * list->height/3) {
		//printf("X i=%d dist=%d cols=%d lh=%d at=%d\n", i, dist, cols, list->height, alttop);
		i--;
		e = list->han->getitem(list->list, i);
		list->han->get_size(e, &w, &h);
		dist += h;
		if (cols > 1 && w*cols > list->width) {
			cols --;
			goto col_retry;
		}
		if (dist * 3 < list->height) {
			alttop = i;
			altdist = dist;
		}
	}
	if (i == top) {
		if (dist*3 < list->height) {
			/* try to move to other end */
			while (i > 0 &&
			       dist < (cols-1)*list->height * list->height*2/3) {
				i--;
				e = list->han->getitem(list->list, i);
				list->han->get_size(e, &w, &h);
				dist += h;
				if (cols > 1 && w*cols > list->width) {
					cols--;
					goto col_retry;
				}
				top = i;
			}
		}
	} else {
		/* too near bottom */
		top = alttop;
		dist = altdist;
	}

	i = sel;
	e = list->han->getitem(list->list, i);
	list->han->get_size(e, &w, &h);
	while (dist + h <= list->height * cols) {
		dist += h;
		i++;
		//printf("Y i=%d dist=%d cols=%d\n", i, dist, cols);
		e = list->han->getitem(list->list, i);
		if (e == NULL)
			break;
		list->han->get_size(e, &w, &h);
		if (cols > 1 && w*cols > list->width) {
			cols --;
			goto col_retry;
		}
	}
	if (e == NULL) {
		/* near end, maybe move top up */
		i = top;
		while (i > 0) {
			i--;
			e = list->han->getitem(list->list, i);
			list->han->get_size(e, &w, &h);
			if (dist + h >= list->height * cols)
				break;
			dist += h;
			if (cols > 1 && w * cols > list->width) {
				cols--;
				goto col_retry;
			}
			top = i;
		}
	}

	/* OK, we are going with 'top' and 'cols'. */
	list->cols = cols;
	list->top = top;
	list->selected = sel;
	/* set x,y,width,height for each
	 * width and height are set - adjust width and set x,y
	 */
	col = 0;
	pos = 0;
	i = top;
	colwidth = list->width / cols;
	last = top;
	while (col < cols) {
		e = list->han->getitem(list->list, i);
		if (e == NULL)
			break;
		e->x = col * colwidth;
		e->y = pos;
		//printf("Set %d,%d  %d,%d\n", e->x, e->y, e->height, list->height);
		pos += e->height;
		if (pos > list->height) {
			pos = 0;
			col++;
			continue;
		}
		e->width = colwidth;
		e->need_draw = 1;
		last = i;
		i++;
	}
	list->last = last;
}

void calc_layout(struct sellist *list)
{
	/* Choose 'top', and set 'last' and 'cols'
	 * so that 'selected' is visible in width/height,
	 * and is not in a 'bad' position.
	 * If 'sel' is in first column and ->y < height/3
	 * and 'top' is not zero, that is bad, we set top to zero.
	 * If 'sel' is in last column and ->Y > height/3
	 * and 'last' is not end of list, that is bad, we set
	 *  top to 'last'
	 * If 'sel' is before 'top', that is bad, set 'top' to 'sel'.
	 * If 'sel' is after 'last', that is bad, set 'top' to 'sel'.
	 */
	int top = list->top;
	int sel = list->selected;
	int cols = 10000;
	int col, row, pos, last;
	struct list_entry *sele;
	int did_jump = 0;

 retry_cols:
	/* make sure 'top' and 'sel' are valid */
	top = list->top;
	sel = list->selected;
	did_jump = 0;
	if (top < 0 || list->han->getitem(list->list, top) == NULL)
		top = 0;
	if (sel < 0 || list->han->getitem(list->list, sel) == NULL)
		sel = 0;

 retry:
	//printf("try with top=%d cols=%d\n", top, cols);
	pos = top; col=0; row=0; last= -1;
	sele = NULL;
	while(1) {
		int w, h;
		struct list_entry *e;

		e = list->han->getitem(list->list, pos);
		if (e == NULL)
			break;
		if (pos == sel)
			sele = e;
		list->han->get_size(e, &w, &h);
		if (cols > 1 && w * cols > list->width) {
			/* too many columns */
			cols = list->width / w;
			if (cols <= 0)
				cols = 1;
			goto retry_cols;
		}
		e->x = col * list->width / cols;
		e->y = row;
		e->width = list->width / cols;
		e->need_draw = 1;
		row += h;
		if (row > list->height && e->y > 0) {
			col ++;
			if (col == cols)
				break;
			e->x = col * list->width / cols;
			e->y = 0;
			row = h;
		}
		last = pos;
		pos++;
	}
	/* Check if this is OK */
	if (sele == NULL) {
		//printf("no sel: %d %d\n", sel, top);
		if (last <= top)
			goto out;
		if (sel < top)
			top--;
		else
			top++;
		goto retry;
	}
	//printf("x=%d y=%d hi=%d lh=%d top=%d lw=%d cols=%d\n",
	//       sele->x, sele->y, sele->height, list->height, top, list->width, cols);
	if (sele->x == 0 && sele->y + sele->height < list->height / 3
	    && top > 0) {
		if (did_jump)
			top --;
		else {
			top = 0;
			did_jump = 1;
		}
		goto retry;
	}
	if (sele->x == (cols-1) * list->width / cols &&
	    sele->y + sele->height > list->height * 2 / 3 &&
	    col == cols) {
		if (did_jump)
			top ++;
		else {
			top = last;
			did_jump = 1;
		}
		goto retry;
	}
	if (col < cols && top > 0 &&
	    (col < cols - 1 || row < list->height / 2)) {
		top --;
		goto retry;
	}
 out:
	list->top = top;
	list->last = last;
	list->cols = cols;
	//printf("top=%d last=%d cols=%d\n", top, last, cols);
}

void configure(GtkWidget *draw, void *event, struct sellist *list)
{
	GtkAllocation alloc;

	gtk_widget_get_allocation(draw, &alloc);
#if 0
	if (list->width == alloc.width &&
	    list->height == alloc.height)
		return;
#endif
	list->width = alloc.width;
	list->height = alloc.height;
	calc_layout(list);
	gtk_widget_queue_draw(draw);
}

void expose(GtkWidget *draw, GdkEventExpose *event, struct sellist *list)
{
	int i;
	/* Draw all fields in the event->area */

	for (i = list->top; i <= list->last; i++) {
		struct list_entry *e = list->han->getitem(list->list, i);
		if (e->x > event->area.x + event->area.width)
			/* to the right */
			continue;
		if (e->x + e->width < event->area.x)
			/* to the left */
			continue;
		if (e->y > event->area.y + event->area.height)
			/* below */
			continue;
		if (e->y + e->height < event->area.y)
			/* above */
			continue;

		e->need_draw = 1;
		if (event->count == 0 && e->need_draw == 1) {
			e->need_draw = 0;
			list->han->render(e, i == list->selected, list->drawing);
		}
	}
}

void tap(GtkWidget *draw, GdkEventButton *event, struct sellist *list)
{
	int i;
	for (i=list->top; i <= list->last; i++) {
		struct list_entry *e = list->han->getitem(list->list, i);
		if (event->x >= e->x &&
		    event->x < e->x + e->width &&
		    event->y >= e->y &&
		    event->y < e->y + e->height)
		{
			//printf("found item %d\n", i);
			list->selected = i;
			calc_layout(list);
			gtk_widget_queue_draw(list->drawing);
			list->han->selected(list, i);
			break;
		}
	}
}

void *listsel_new(void *list, struct list_handlers *han)
{
	struct sellist *rv;

	rv = malloc(sizeof(*rv));
	rv->list = list;
	rv->han = han;
	rv->drawing = gtk_drawing_area_new();
	rv->top = 0;
	rv->selected = -1;
	rv->width = rv->height = 0;
	rv->cols = 1;

	g_signal_connect((gpointer)rv->drawing, "expose-event",
			 G_CALLBACK(expose), rv);
	g_signal_connect((gpointer)rv->drawing, "configure-event",
			 G_CALLBACK(configure), rv);

	gtk_widget_add_events(rv->drawing,
			      GDK_BUTTON_PRESS_MASK|
			      GDK_BUTTON_RELEASE_MASK);
	g_signal_connect((gpointer)rv->drawing, "button_release_event",
			 G_CALLBACK(tap), rv);

	return rv;
}

#ifdef MAIN

struct list_entry_text *entries;
int entcnt;
PangoFontDescription *fd;
PangoLayout *layout;
GdkGC *colour;

/*
 * gdk_color_parse("green", GdkColor *col);
 * gdk_colormap_alloc_color(GdkColormap, GdkColor, writeable?, bestmatch?)
 * gdk_colormap_get_system
 *
 * gdk_gc_new(drawable)
 * gdk_gc_set_foreground(gc, color)
 * gdk_gc_set_background(gc, color)
 * gdk_gc_set_rgb_fg_color(gc, color)
 */
struct list_entry *item(void *list, int n)
{
	if (n < entcnt)
		return &entries[n].head;
	else
		return NULL;
}

int size(struct list_entry *i, int *width, int*height)
{
	PangoRectangle ink, log;
	struct list_entry_text *item = (void*)i;

	if (i->height) {
		*width = item->true_width;
		*height = i->height;
		return 0;
	}
	//printf("calc for %s\n", item->text);
	pango_layout_set_text(layout, item->text, -1);
	pango_layout_get_extents(layout, &ink, &log);
	//printf("%s %d %d\n", item->text, log.width, log.height);
	*width = log.width / PANGO_SCALE;
	*height = log.height / PANGO_SCALE;
	item->true_width = i->width = *width;
	i->height = *height;
	return 0;
}

int render(struct list_entry *i, int selected, GtkWidget *d)
{
	PangoRectangle ink, log;
	struct list_entry_text *item = (void*)i;
	int x;
	GdkColor col;

	pango_layout_set_text(layout, item->text, -1);

	if (colour == NULL) {
		colour = gdk_gc_new(gtk_widget_get_window(d));
		gdk_color_parse("purple", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
	}
	if (selected) {
		gdk_color_parse("pink", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
		gdk_draw_rectangle(gtk_widget_get_window(d),
				   colour, TRUE,
				   item->head.x, item->head.y,
				   item->head.width, item->head.height);
		gdk_color_parse("purple", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
	}
	x = (i->width - item->true_width)/2;
	if (x < 0)
		x = 0;
	gdk_draw_layout(gtk_widget_get_window(d),
			colour,
			item->head.x+x, item->head.y, layout);
	return 0;
}
void selected(void *list, int n)
{
	printf("got %d\n", n);
}

struct list_handlers myhan =  {
	.getitem = item,
	.get_size = size,
	.render = render,
	.selected = selected,
};

main(int argc, char *argv[])
{
	int i;
	GtkWidget *w;
	struct sellist *l;
	PangoContext *context;

	entries = malloc(sizeof(*entries) * argc);
	memset(entries, 0, sizeof(*entries)*argc);
	for (i=1; i<argc; i++) {
		entries[i-1].text = argv[i];
		entries[i-1].bg = "white";
		entries[i-1].fg = "blue";
		entries[i-1].underline = 0;
	}
	entcnt = i-1;

	gtk_set_locale();
	gtk_init_check(&argc, &argv);

	fd = pango_font_description_new();
	pango_font_description_set_size(fd, 35 * PANGO_SCALE);

	w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(w), 480, 640);

	gtk_widget_modify_font(w, fd);
	context = gtk_widget_get_pango_context(w);
	layout = pango_layout_new(context);

	l = listsel_new(NULL, &myhan);
	gtk_container_add(GTK_CONTAINER(w), l->drawing);
	gtk_widget_show(l->drawing);
	gtk_widget_show(w);

	gtk_main();
}
#endif
