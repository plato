
struct sellist {
	void *list;
	struct list_handlers *han;
	GtkWidget *drawing;

	int top;	/* Index of first element displayed */
	int selected;	/* Index of currently selected element */
	int last;	/* Index of last displayed element */

	int width, height; /* Pixel size of widget */
	int cols;	/* Columns */
};

struct list_handlers {
	struct list_entry *(*getitem)(void *list, int n);
	int (*get_size)(struct list_entry *item, int *width, int *height);
	int (*render)(struct list_entry *item, int selected,
		      GtkWidget *d);
	void (*selected)(void *list, int element);
};

struct list_entry {
	int x, y, width, height;
	int need_draw;
};

struct list_entry_text {
	struct list_entry head;
	char *text;
	
	int true_width;
	char *bg, *fg;
	int underline;
};

extern void *listsel_new(void *list, struct list_handlers *han);


