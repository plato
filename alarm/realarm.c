
#define _XOPEN_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>
#include <sys/dir.h>
#include <event.h>
#include "libsus.h"


int seconds = 60;
struct event *wkev;

static void realarm(int fd, short ev, void *vp)
{
	if (wkev && !vp)
		wakealarm_destroy(wkev);
	wkev = wakealarm_set(time(0) + seconds, realarm, (void*)1);
}


main(int argc, char *argv[])
{

	if (argc >= 2) {
		seconds = atoi(argv[1]);
		if (seconds <= 0)
			exit(1);
	}
	event_init();
	realarm(0, 0, NULL);
	event_loop(0);
	exit(0);
}
