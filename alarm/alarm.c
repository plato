
/*
 * generate alarm messages as required.
 * Alarm information is stored in /data/alarms
 * format is:
 *   date:recurrence:message
 *
 * date is yyyy-mm-dd-hh-mm-ss
 *  recurrence is currently nndays
 * message is any text
 *
 * When the time comes, a txt message is generated
 *
 * We use dnotify to watch the file
 *
 * We never report any event that is before the timestamp
 * on the file - that guards against replaying lots of
 * events if the clock gets set backwards.
 */

#define _XOPEN_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>
#include <sys/dir.h>
#include <event.h>
#include "libsus.h"
#include "ical-dates.h"

struct alarm_ev {
	struct alarm_ev *next;
	time_t when;
	struct ical_dates idate;
	char *mesg;
};

void alarm_free(struct alarm_ev *ev)
{
	while (ev) {
		struct alarm_ev *n = ev->next;
		free(ev->mesg);
		ical_dates_free(&ev->idate);
		free(ev);
		ev = n;
	}
}

void update_event(struct alarm_ev *ev, time_t now)
{
	/* make sure 'when' is in the future if possible */
	struct ical_time inow;
	struct ical_timelist rv = {0};
	ical_localtime(&inow, &now);
	ical_rr_dates(&ev->idate.start, &ev->idate.rr, &inow, 1, &rv);
	if (rv.cnt >= 1)
		ev->when = ical_mktime(rv.v);
	else
		ev->when = ical_mktime(&ev->idate.start);
	ical_timelist_free(&rv);
/* FIXME what if it is a date, not a time*/
}

struct alarm_ev *event_parse(char *line)
{
	/* Space separated ical lines, except that SUMMARY: is the last
	 * and can hold spaces, and  provides the message.
	 */
	struct alarm_ev *rv;
	char *mesg = NULL;
	struct ical_dates dt = {{0}};

	ical_parse_rrule(&dt.rr, "", NULL);
	while (*line) {
		char *w = line;
		int l, err;
		if (strncmp(line, "SUMMARY:", 8) == 0)
			l = strlen(line);
		else
			l = strcspn(line, " ");
		if (line[l])
			line[l++] = 0;
		line += l;

		err = ical_parse_dates_line(&dt, w);
		if (err == 1)
			continue;
		if (err == 0)
			goto abort;
		if (strncmp(w, "SUMMARY:", 8) != 0 || mesg)
			goto abort;
		mesg = strdup(w+8);
	}
	if (dt.start.mon == 0 || dt.start.time_set == 0)
		goto abort;
	rv = malloc(sizeof(*rv));
	rv->next = NULL;
	rv->idate = dt;
	rv->when = ical_mktime(&dt.start);
	rv->mesg = mesg;
	return rv;
abort:
	free(mesg);
	ical_dates_free(&dt);
	return NULL;
}

struct alarm_ev *event_load_soonest(char *file, time_t now)
{
	/* load the file and return only the soonest event at or after now */
	char line[2048];
	struct stat stb;
	struct alarm_ev *rv = NULL, *ev;
	FILE *f;

	f = fopen(file, "r");
	if (!f)
		return NULL;
	if (fstat(fileno(f), &stb) == 0 &&
	    stb.st_mtime > now)
		now = stb.st_mtime;

	while (fgets(line, sizeof(line), f) != NULL) {
		char *eol = line + strlen(line);
		if (eol > line && eol[-1] == '\n')
			eol[-1] = 0;

		ev = event_parse(line);
		if (!ev)
			continue;
		update_event(ev, now);
		if (ev->when < now) {
			alarm_free(ev);
			continue;
		}
		if (rv == NULL) {
			rv = ev;
			continue;
		}
		if (rv->when < ev->when) {
			alarm_free(ev);
			continue;
		}
		alarm_free(rv);
		rv = ev;
	}
	fclose(f);
	return rv;
}

struct alarm_ev *event_load_soonest_dir(char *dir, time_t now)
{
	DIR *d = opendir(dir);
	struct dirent *de;
	struct alarm_ev *rv = NULL;

	if (!d)
		return NULL;
	while ((de = readdir(d)) != NULL) {
		char *p = NULL;
		struct alarm_ev *e;
		if (de->d_ino == 0)
			continue;
		if (de->d_name[0] == '.')
			continue;
		asprintf(&p, "%s/%s", dir, de->d_name);
		e = event_load_soonest(p, now);
		free(p);
		if (e == NULL)
			;
		else if (rv == NULL)
			rv = e;
		else if (rv->when < e->when)
			alarm_free(e);
		else {
			alarm_free(rv);
			rv = e;
		}
	}
	closedir(d);
	return rv;
}

void event_deliver(struct alarm_ev *ev)
{
	char line[2048];
	char file[1024];
	struct tm *tm;
	char *z;
	char *m;
	int f;
	static time_t last_ev;
	time_t now;

	if (!ev->mesg || !ev->mesg[0])
		return;
	time(&now);
	if (now <= last_ev)
		now = last_ev + 1;
	last_ev = now;
	tm = gmtime(&now);
	strftime(line, 2048, "%Y%m%d-%H%M%S", tm);
	tm = localtime(&ev->when);
	strftime(line+strlen(line), 1024, " ALARM %Y%m%d-%H%M%S", tm);
	z = line + strlen(line);
	sprintf(z, "%+03d alarm ", (int)tm->tm_gmtoff/60/15);

	z = line + strlen(line);
	m = ev->mesg;

	while (*m) {
		switch (*m) {
		default:
			*z++ = *m;
			break;
		case ' ':
		case '\t':
		case '\n':
			sprintf(z, "%%%02x", *m);
			z += 3;
			break;
		}
		m++;
	}
	*z++ = '\n';
	*z = 0;

	sprintf(file, "/data/SMS/%.6s", line);
	f = open(file, O_WRONLY | O_APPEND | O_CREAT, 0600);
	if (f >= 0) {
		write(f, line, strlen(line));
		close(f);
		f = open("/data/SMS/newmesg", O_WRONLY | O_APPEND | O_CREAT,
			 0600);
		if (f >= 0) {
			line[16] = '\n';
			write(f, line, 17);
			close(f);
		}
		f = open("/run/alert/alarm", O_WRONLY | O_CREAT, 0600);
		if (f) {
			write(f, "alarm\n", 4);
			close(f);
		}
		/* Abort any current suspend cycle */
		suspend_abort(-1);
	}
}


time_t now;
struct event *wkev;
int efd;
static void check_alarms(int fd, short ev, void *vp)
{
	struct alarm_ev *evt = event_load_soonest_dir("/data/alarms", now);
	if (wkev && !vp)
		wakealarm_destroy(wkev);
	wkev = NULL;
	while ((evt = event_load_soonest_dir("/data/alarms", now)) != NULL
	       && evt->when <= time(0)) {
		event_deliver(evt);
		now = evt->when + 1;
		alarm_free(evt);
	}
	if (!evt)
		return;
	wkev = wakealarm_set(evt->when, check_alarms, (void*)1);
	alarm_free(evt);
	fcntl(efd, F_NOTIFY, DN_MODIFY|DN_CREATE|DN_RENAME);
}

int main(int argc, char *argv[])
{
	struct event ev;

	efd = open("/data/alarms", O_DIRECTORY|O_RDONLY);
	if (efd < 0)
		exit(1);

	event_init();
	signal_set(&ev, SIGIO, check_alarms, NULL);
	signal_add(&ev, NULL);

	fcntl(efd, F_NOTIFY, DN_MODIFY|DN_CREATE|DN_RENAME);

	time(&now);
	check_alarms(0, 0, NULL);

	event_loop(0);
	exit(0);
}
