#!/usr/bin/env python

#
# Contacts manager
#  Currently a 'contact' is a name, a number, and a speed-dial letter
#
# We have a content pane and a row of buttons at the bottom.
# The content is either:
#   - list of contact names - highlighted minipane at bottom with number
#   - detailed contact info that can be editted (name, number, speed-pos)
#
# When list of contacts are displayed, typing a char adds that char to
# a  substring and only contacts containing that substring are listed.
# An extra entry at the start is given which matches exactly the substring
# and can be used to create a new entry.
# Alternately, the list can show only 'deleted' entries, still with substring
# matching.  Colour is different in this case.
#
# Buttons for contact list are:
#  When nothing selected (list_none):
#     New ALL Undelete
#  When contact selected (list_sel):
#     Call SMS Open ALL
#  When new-entry selected (list_new):
#    Create ALL
#  When viewing deleted entries and nothing or first is selected (del_none):
#    Return
#  When viewing deleted entries and one is selected (del_sel):
#    Undelete Open Return
#
# When the detailed contact info is displayed all fields can be
# edited and change background colour if they differ from stored value
# Fields are: Name Number
# Button for details are:
#  When nothing is changed (edit_nil):
#     Call SMS Close Delete
#  When something is changed on new entry (edit_new)
#     Discard  Create
#  When something is changed on old entry (edit old)
#     Restore Save Create
#
# 'delete' doesn't actually delete, but adds '!delete$DATE-' to the name which
# causes most lookups to ignore the entry.
#
# TODO
# - find way to properly reset 'current' pos after edit
# - have a global 'state' object which other things refer to
#   It has an 'updated' state which other objects can connect to
# - save file after an update

import gtk, pango, time, gobject, os
from scrawl import Scrawl
from listselect import ListSelect
from contactdb import contacts

def strip_prefix(num):
    if num[:2] == "02":
        return num[2:]
    if num[:2] == "04":
        return num[1:]
    if num[:4] == "+612":
        return num[4:]
    if num[:4] == "+614":
        return num[3:]
    return num

def match_num(num, str):
    """ 'num' is a phone number, and 'str' is a search string
    We want to allow matches that ignore the country/local prefix
    which might be "02" or "+612" or something else in other countries.
    But I'm not in other countries, so just strip those if present.
    """
    if num.find(str) >= 0:
        return True
    if "+612".find(str) == 0:
        return True
    if "02".find(str) == 0:
        return True
    return strip_prefix(num).find(strip_prefix(str)) == 0

class Contacts(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self)
        self.set_default_size(480,640)
        self.set_title("Contacts")
        self.connect('destroy', self.close_win)

        self.current = None
        self.timer = None
        self.make_ui()
        self.load_book()
        self.show()
        self.voice_cb = None
        self.sms_cb = None
        self.undeleting = False
        self.watch_clip('contact-find')

    def make_ui(self):
        # UI consists of:
        #   list of contacts -or-
        #   editable field
        #   -and-
        #   variable list of buttons.
        #
        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(35*pango.SCALE)
        self.fd = fd

        v = gtk.VBox(); v.show()
        self.add(v)

        s = self.listui()
        self.lst = s
        v.pack_start(s, expand=True)
        s.show()
        self.show()
        self.sc.set_colour('red')

        s = self.editui()
        v.pack_start(s, expand = True)
        s.hide()
        self.ed = s

        bv = gtk.VBox(); bv.show(); v.pack_start(bv, expand=False)
        def hide_some(w):
            for c in w.get_children():
                c.hide()
        bv.hide_some = lambda : hide_some(bv)
        self.buttons = bv

        b = self.buttonlist(bv)
        self.button(b, 'New', self.open)
        self.button(b, 'Undelete', self.undelete)
        self.button(b, 'ALL', self.all)
        self.list_none = b

        b = self.buttonlist(bv)
        self.button(b, 'Call', self.call)
        self.button(b, 'SMS', self.sms)
        self.button(b, 'Open', self.open)
        self.button(b, 'Delete', self.delete)
        self.list_sel = b

        b = self.buttonlist(bv)
        self.button(b, 'Create', self.open)
        self.button(b, 'ALL', self.all)
        self.list_new = b

        b = self.buttonlist(bv)
        self.button(b, 'Return', self.all)
        self.del_none = b

        b = self.buttonlist(bv)
        self.button(b, 'Undelete', self.delete)
        self.button(b, 'Open', self.open)
        self.button(b, 'Return', self.all)
        self.del_sel = b

        b = self.buttonlist(bv)
        self.button(b, 'Call', self.call)
        self.button(b, 'SMS', self.sms)
        self.button(b, 'Close', self.close)
        self.button(b, 'Delete', self.delete)
        self.edit_nil = b

        b = self.buttonlist(bv)
        self.button(b, 'Discard', self.close)
        self.button(b, 'Create', self.create)
        self.edit_new = b

        b = self.buttonlist(bv)
        self.button(b, 'Restore', self.open)
        self.button(b, 'Save', self.save)
        self.button(b, 'Create', self.create)
        self.edit_old = b

        self.list_none.show()

    def listui(self):
        s = ListSelect(markup=True); s.show()
        s.set_format("normal","black", background="grey", selected="white")
        s.set_format("deleted","red", background="grey", selected="white")
        s.set_format("virtual","blue", background="grey", selected="white")
        s.connect('selected', self.selected)
        s.set_zoom(37)
        self.clist = contact_list()
        s.list = self.clist
        s.list_changed()
        self.sel = s
        def gottap(p):
            x,y = p
            s.tap(x,y)
        self.sc = Scrawl(s, self.gotsym, gottap, None, None)

        s.set_events(s.get_events() | gtk.gdk.KEY_PRESS_MASK)
        def key(list, ev):
            if len(ev.string) == 1:
                self.gotsym(ev.string)
            elif ev.keyval == 65288:
                self.gotsym('<BS>')
            else:
                #print ev.keyval, len(ev.string), ev.string
                pass
        s.connect('key_press_event', key)
        s.set_flags(gtk.CAN_FOCUS)
        s.grab_focus()

        v = gtk.VBox(); v.show()
        v.pack_start(s, expand=True)
        l = gtk.Label('')
        l.modify_font(self.fd)
        self.number_label = l
        v.pack_start(l, expand=False)
        return v

    def editui(self):
        ui = gtk.VBox()

        self.fields = {}
        self.field(ui, 'Name')
        self.field(ui, 'Number')
        self.field(ui, 'Speed Number')
        return ui

    def field(self, v, lbl):
        h = gtk.HBox(); h.show()
        l = gtk.Label(lbl); l.show()
        l.modify_font(self.fd)
        h.pack_start(l, expand=False)
        e = gtk.Entry(); e.show()
        e.modify_font(self.fd)
        h.pack_start(e, expand=True)
        e.connect('changed', self.field_update, lbl)
        v.pack_start(h, expand=True, fill=True)
        self.fields[lbl] = e
        return e

    def buttonlist(self, v):
        b = gtk.HBox()
        b.set_homogeneous(True)
        b.hide()
        v.pack_end(b, expand=False)
        return b

    def button(self, bl, label, func):
        b = gtk.Button()
        if label[0:3] == "gtk" :
            img = gtk.image_new_from_stock(label, self.isize)
            img.show()
            b.add(img)
        else:
            b.set_label(label)
            b.child.modify_font(self.fd)
        b.show()
        b.connect('clicked', func)
        b.set_focus_on_click(False)
        bl.pack_start(b, expand = True)

    def close_win(self, widget):
        gtk.main_quit()

    def selected(self, list, item):
        self.buttons.hide_some()

        if item == None:
            item = -1
        if self.undeleting:
            if item < 0 or (item == 0 and self.clist.str != ''):
                self.del_none.show()
            else:
                self.del_sel.show()
                self.current = self.clist.getitem(item)
        elif item < 0:
            self.list_none.show()
            self.number_label.hide()
            self.current = None
        elif item == 0 and self.clist.str != '':
            self.list_new.show()
            self.number_label.hide()
            self.current = None
        else:
            self.list_sel.show()
            i = self.clist[item]
            self.current = self.clist.getitem(item)
            if i == None:
                self.number_label.hide()
            else:
                self.number_label.set_text(self.clist.list[self.current].num)
                self.number_label.show()

    def field_update(self, ev, fld):
        if self.flds[fld] == self.fields[fld].get_text():
            self.fields[fld].modify_base(gtk.STATE_NORMAL,None)
        else:
            self.fields[fld].modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('yellow'))

        same = True
        for i in ['Name', 'Number', 'Speed Number']:
            if self.fields[i].get_text() != self.flds[i]:
                same = False
        self.buttons.hide_some()
        if self.current == None:
            self.edit_new.show()
        elif same:
            self.edit_nil.show()
        else:
            self.edit_old.show()
        pass

    def load_book(self):
        self.book = contacts()
        self.clist.set_list(self.book.list)

    def queue_save(self):
        if self.timer:
            gobject.source_remove(self.timer)
        self.timer = gobject.timeout_add(15*1000, self.do_save)
    def do_save(self):
        if self.timer:
            gobject.source_remove(self.timer)
        self.timer = None
        self.book.save()

    def gotsym(self,sym):
        if sym == '<BS>':
            s = self.clist.str[:-1]
        elif sym == '<escape>' or sym == '<newline>':
            s = ''
        elif len(sym) > 1:
            s = self.clist.str
        elif ord(sym) >= 32:
            s = self.clist.str + sym
        else:
            return
        self.clist.set_str(s)
        self.sel.list_changed()
        self.sel.select(None)

    def watch_clip(self, board):
        self.cb = gtk.Clipboard(selection=board)
        self.targets = [ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0) ]
        self.got_clip(self.cb, None)
        self.cb.set_with_data(self.targets,
                              self.get_clip, self.got_clip, None)


    def got_clip(self, clipb, data):
        s = clipb.wait_for_text()
        if not s:
            return
        if s != '-':
            self.clist.set_str(s)
            self.lst.show()
            self.sel.grab_focus()
            self.ed.hide()
            self.sel.list_changed()
            self.sel.select(None)
        self.cb.set_with_data(self.targets, self.get_clip, self.got_clip, None)
        self.present()

    def get_clip(self, sel, seldata, info, data):
        seldata.set_text("Contact Please")

    def open(self, ev):
        self.lst.hide()
        self.ed.show()

        self.buttons.hide_some()
        if self.current == None:
            self.edit_new.show()
        else:
            self.edit_nil.show()
        self.flds = {}
        self.flds['Name'] = ''
        self.flds['Number'] = ''
        self.flds['Speed Number'] = ''
        if self.current != None:
            current = self.clist.list[self.current]
            self.flds['Name'] = current.name
            self.flds['Number'] = current.num
            self.flds['Speed Number'] = current.speed
            if current.speed == None:
                self.flds['Speed Number'] = ""
        elif self.clist.str:
            num = True
            for d in self.clist.str:
                if not d.isdigit() and d != '+':
                    num = False
            if num:
                self.flds['Number'] = self.clist.str
            else:
                self.flds['Name'] = self.clist.str
            
        for f in ['Name', 'Number', 'Speed Number'] :
            c = self.flds[f]
            if not c:
                c = ""
            self.fields[f].set_text(c)
            self.fields[f].modify_base(gtk.STATE_NORMAL,None)

    def all(self, ev):
        self.clist.set_str('')
        self.undeleting = False
        self.current = None
        self.clist.set_list(self.book.list)
        self.sel.select(None)
        self.sel.list_changed()
        pass
    def create(self, ev):
        self.save(None)
    def save(self,ev):
        name = self.fields['Name'].get_text()
        num = self.fields['Number'].get_text()
        speeds = self.fields['Speed Number'].get_text()
        speed = speeds
        if speed == "":
            speed = None
        if name == '' or name.find(';') >= 0:
            self.fields['Name'].modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('pink'))
            return
        if num == '' or num.find(';') >= 0:
            self.fields['Number'].modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('pink'))
            return
        if len(speeds) > 1 or speeds.find(';') >= 0:
            self.fields['Speed Number'].modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('pink'))
            return
        if self.current == None or ev == None:
            self.book.add(name, num, speed)
        else:
            current = self.book.list[self.current]
            current.name = name
            current.num = num
            current.speed = speed
        self.flds['Name'] = name
        self.flds['Number'] = num
        self.flds['Speed Number'] = speeds
        self.book.resort()
        self.clist.set_list(self.book.list)
        self.sel.list_changed()
        self.close(ev)
        self.queue_save()

    def delete(self, ev):
        if self.current != None:
            if self.undeleting:
                self.book.undelete(self.current)
                self.clist.set_list(self.book.deleted)
            else:
                self.book.delete(self.current)
                self.clist.set_list(self.book.list)
            self.sel.list_changed()
            self.close(ev)
            self.queue_save()
        pass

    def undelete(self, ev):
        self.undeleting = True
        self.clist.set_str('')
        self.clist.set_list(self.book.deleted)
        self.current = None
        self.sel.list_changed()
        self.sel.select(None)
        pass
    def call(self, ev):
        if not self.voice_cb:
            self.voice_cb = gtk.Clipboard(selection='voice-dial')
        self.call_str = self.clist.list[self.current].num
        self.voice_cb.set_with_data([ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0) ],
                                    self.call_getdata, self.call_cleardata, None)
    def call_getdata(self, clipb, sel, info, data):
        sel.set_text(self.call_str)
    def call_cleardata(self, clipb, data):
        self.call_str = ""

    def sms(self, ev):
        if not self.sms_cb:
            self.sms_cb = gtk.Clipboard(selection='sms-new')
        self.sms_str = self.clist.list[self.current].num
        self.sms_cb.set_with_data([ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0)],
                                  self.sms_get_data, self.sms_cleardata, None)
    def sms_get_data(self, clipb, sel, info, data):
        sel.set_text(self.sms_str)
    def sms_cleardata(self, clipb, data):
        self.sms_str = ""


    def close(self, ev):
        self.lst.show()
        self.sel.grab_focus()
        self.ed.hide()
        if self.current != None and self.current >= len(self.clist):
            self.current = None
        self.selected(None, self.sel.selected)

class contact_list:
    def __init__(self):
        self.set_list([])
    def set_list(self, list):
        self.list = list
        self.match_start = []
        self.match_word = []
        self.match_any = []
        self.match_str = ''
        self.total = 0
        self.str = ''

    def set_str(self,str):
        self.str = str

    def recalc(self):
        if self.str == self.match_str:
            return
        if self.match_str != '' and self.str[:len(self.match_str)] == self.match_str:
            # str is a bit longer
            self.recalc_quick()
            return
        self.match_start = []
        self.match_word = []
        self.match_any = []
        self.match_str = self.str
        s = self.str.lower()
        l = len(self.str)
        for i in range(len(self.list)):
            name = self.list[i].name.lower()
            if name[0:l] == s:
                self.match_start.append(i)
            elif name.find(' '+s) >= 0:
                self.match_word.append(i)
            elif name.find(s) >= 0 or match_num(self.list[i].num, s):
                self.match_any.append(i)
        self.total = len(self.match_start) + len(self.match_word) + len(self.match_any)

    def recalc_quick(self):
        # The string has been extended so we only look through what we already have
        self.match_str = self.str
        s = self.str.lower()
        l = len(self.str)
        
        lst = self.match_start
        self.match_start = []
        for i in lst:
            name = self.list[i].name.lower()
            if name[0:l] == s:
                self.match_start.append(i)
            else:
                self.match_word.append(i)
        self.match_word.sort()
        lst = self.match_word
        self.match_word = []
        for i in lst:
            name = self.list[i].name.lower()
            if name.find(' '+s) >= 0:
                self.match_word.append(i)
            else:
                self.match_any.append(i)
        self.match_any.sort()
        lst = self.match_any
        self.match_any = []
        for i in lst:
            name = self.list[i].name.lower()
            if name.find(s) >= 0 or match_num(self.list[i].num, s):
                self.match_any.append(i)
        self.total = len(self.match_start) + len(self.match_word) + len(self.match_any)
                    
    def __len__(self):
        self.recalc()
        if self.str == '':
            return len(self.list)
        else:
            return self.total + 1
    def getitem(self, ind):
        if ind < 0:
            print '!!!!', ind
        if self.str == '':
            return ind

        if ind == 0:
            return -1
        ind -= 1
        if ind < len(self.match_start):
            return self.match_start[ind]
        ind -= len(self.match_start)
        if ind < len(self.match_word):
            return self.match_word[ind]
        ind -= len(self.match_word)
        if ind < len(self.match_any):
            return self.match_any[ind]
        return None

    def __getitem__(self, ind):

        i = self.getitem(ind)
        if i < 0:
            return (protect(self.str), 'virtual')
        if i == None:
            return None
        s = self.list[i].name
        n = self.list[i].num
        s = protect(s)
        n = protect(n)
        if s[:8] == '!deleted':
            p = s.find('-')
            if p <= 0:
                p = 7
            return (s[p+1:],'deleted')
        else:
            return (s + '<span color="blue" size="15000">\n     '+ n + '</span>', 'normal')

def protect(txt):
    txt = txt.replace('&', '&amp;')
    txt = txt.replace('<', '&lt;')
    txt = txt.replace('>', '&gt;')
    return txt

if __name__ == "__main__":

    c = Contacts()
    gtk.main()
    
