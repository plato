
#
# Contacts DB interface for various programs
# Currently only understand name/number and speed-dials
# Separately we also provide access to log of incoming and
# outgoing calls, mapped to names through the contacts DB.
#
# Need:
#   list of "speed-dials".  These are ordered
#   look up name/number from speed-dial
#   map 'name' to entry - first match, with '.'s
#   map 'number' to probable name
#   load/save address book
#   load /var/log/incoming and /var/log/outgoing
#

import os, time

class entry:
    def __init__(self, name, num = None):
        # if 'num' is None, then 'name' contains "name;num;..."
        if num == None:
            a = name.strip().split(';')
            if len(a) < 2:
                raise ValueError;
            name = a[0]
            num = a[1]
        self.name = name
        self.num = num
        self.speed = None
        
    def is_speed(self):
        return len(self.name) == 1

    def is_deleted(self):
        return self.name[:8] == '!deleted'

    def match_type(self, patn):
        # patn might match:
        # 1: start of name
        # 2: start of word in name
        # 3: somewhere in name
        # 4: somewhere in num
        p = patn.lower()
        n = name.lower()
        l = len(p)
        if n[0:l] == p:
            return 1
        if n.find(' '+p) >= 0:
            return 2
        if n.find(p) >= 0:
            return 3
        if self.num.find(p):
            return 4
        return -1

    def same_num(self, num):
        l = len(num)
        if l < 4:
            # too short to be at all useful
            return False
        # 8 is enough to identify
        if l > 8:
            l = 8
        return len(self.num) >= l and self.num[-l:] == num[-l:]

    def __cmp__(self, other):
        if self.speed and not other.speed:
            return -1
        if other.speed and not self.speed:
            return 1
        return cmp(self.name, other.name)

class contacts:
    def __init__(self):
        try:
            self.file = '/data/address-book'
            self.load()
        except:
            self.file = '/home/neilb/home/mobile-numbers-jan-08'
            self.load()

    def load(self):
        self.list = []
        self.deleted = []
        self.speed = {}
        speed = {}
        f = open(self.file)
        for l in f:
            e = entry(l)
            if e.is_speed():
                speed[e.num] = e.name
            elif e.is_deleted():
                self.deleted.append(e)
            else:
                self.list.append(e)
        if speed:
            for i in range(len(self.list)):
                if self.list[i].name in speed:
                    self.speed[speed[self.list[i].name]] = self.list[i]
                    self.list[i].speed = speed[self.list[i].name]
        self.resort()

    def resort(self):
        self.list.sort()
        self.deleted.sort()

    def save(self):
        f = open(self.file + '.new', 'w')
        for e in self.list:
            f.write(e.name+';'+e.num+';\n')
            if e.speed:
                f.write(e.speed+';'+e.name+';\n')
        for e in self.deleted:
            f.write(e.name+';'+e.num+';\n')
        f.close()
        os.rename(self.file+'.new', self.file)

    def add(self, name, num, speed = None):
        c = entry(name, num)
        self.list.append(c)
        if speed:
            c.speed = speed
            self.speed[speed] = c
        self.resort()

    def undelete(self, ind):
        e = self.deleted[ind]
        del self.deleted[ind]
        s = e.name
        if s[:8] == '!deleted':
            p = s.find('-')
            if p <= 0:
                p = 7
            e.name = s[p+1:]
        self.list.append(e)
        self.resort()
        if e.speed:
            self.speed[e.speed] = e

    def delete(self, ind):
        e = self.list[ind]
        del self.list[ind]
        n = time.strftime('!deleted.%Y.%m.%d-') + e.name
        e.name = n
        self.deleted.append(e)
        self.resort()

    def find_num(self, num):
        for e in self.list:
            if e.same_num(num):
                return e
        return None

if __name__ == "__main__":
    c = contacts()
    print c.speed.keys()
