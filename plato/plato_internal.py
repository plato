#
# internal commands for 'plato'
# some of these affect plato directly, some are just
# ad hoc simple things.

import time as _time
import gobject, gtk
import dnotify, os

class date:
    def __init__(self, name, owner):
        self.buttons = lambda:['Calendar']
        self.embedded = lambda:None
        self.format = 'cmd'
        self.timeout = None
        self.owner = owner

    def press(self, ind):
        if ind != 0:
            return
        self.gowner.emit('request-window','cal')


    def get_name(self):
        now = _time.time()
        if self.timeout == None:
            next_hour = int(now/60/60)+1
            self.timeout = gobject.timeout_add(
                int (((next_hour*3600) - now) * 1000),
                self.do_change)
        return _time.strftime('%d-%b-%Y',
                              _time.localtime(now))

    def do_change(self):
        self.timeout = None
        if self.owner:
            self.owner.queue_draw()


class time:
    def __init__(self, name, owner):
        self.buttons = lambda:None
        self.embedded = lambda:None
        self.format = 'cmd'
        self.timeout = None
        self.owner = owner

    def get_name(self):
        now = _time.time()
        if self.timeout == None:
            next_min = int(now/60)+1
            self.timeout = gobject.timeout_add(
                int (((next_min*60) - now) * 1000),
                self.do_change)
        return _time.strftime('%H:%M',
                              _time.localtime(_time.time()))

    def do_change(self):
        self.timeout = None
        if self.owner:
            self.owner.queue_draw()

zonelist = None
def get_zone(ind):
    global zonelist
    if zonelist == None:
        try:
            f = open("/data/timezone_list")
            l = f.readlines()
        except IOError:
            l = []
        zonelist = map(lambda x:x.strip(), l)
    if ind < 0 or ind >= len(zonelist):
        return "UTC"
    return zonelist[ind]

class tz:
    # Arg is either a timezone name or a number
    # to index into a file containing a list of
    # timezone names  '0' is first line
    def __init__(self, name, owner, zone):
        try:
            ind = int(zone)
            self.zone = get_zone(ind)
        except ValueError:
            self.zone = zone
        self.buttons = lambda:None
        self.embedded = lambda:None
        self.format = 'darkgreen'
        self.owner = owner

    def get_name(self):
        if 'TZ' in os.environ:
            TZ = os.environ['TZ']
        else:
            TZ = None
        os.environ['TZ'] = self.zone
        _time.tzset()
        now = _time.time()
        tm = _time.strftime("%d-%b-%Y %H:%M", _time.localtime(now))

        if TZ:
            os.environ['TZ'] = TZ
        else:
            del(os.environ['TZ'])
        _time.tzset()
        return '<span size="10000">'+tm+"\n"+self.zone+'</span>'


class file:
    def __init__(self, name, owner, path):
        self.buttons = lambda:None
        self.embedded = lambda:None
        self.path = path
        self.format = 'cmd'
        self.owner = owner
        self.watch = None
        self.fwatch = None

    def get_name(self):
        try:
            f = open(self.path)
            l = f.readline().strip()
            f.close()
        except IOError:
            l = "-"
        self.set_watch()

        return l

    def set_watch(self):
        if self.watch == None:
            try:
                self.watch = dnotify.dir(os.path.dirname(self.path))
            except OSError:
                self.watch = None
        if self.watch == None:
            return
        if self.fwatch == None:
            self.fwatch = self.watch.watch(os.path.basename(self.path),
                                           self.file_changed)

    def file_changed(self, thing):
        if self.fwatch:
            self.fwatch.cancel()
            self.fwatch = None
        if self.owner:
            self.owner.queue_draw()

class quit:
    def __init__(self, name, owner):
        self.buttons = lambda:['Exit']
        self.embedded = lambda:None
        self.name = name
        self.format = 'cmd'

    def press(self, ind):
        gtk.main_quit()

class zoom:
    def __init__(self, name, owner):
        self.buttons = lambda:['group+','group-','task+','task-']
        self.owner = owner
        self.name = name
        self.format = 'cmd'
        self.embedded = lambda:None
    def press(self, ind):
        w = self.owner.win
        if ind == 0:
            w.grouplist.set_zoom(w.grouplist.zoom+1)
        if ind == 1:
            w.grouplist.set_zoom(w.grouplist.zoom-1)
        if ind == 2:
            w.tasklist.set_zoom(w.tasklist.zoom+1)
        if ind == 3:
            w.tasklist.set_zoom(w.tasklist.zoom-1)
