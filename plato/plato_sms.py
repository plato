#
# plato plugin for text messages
#
#

import gtk
from plato_internal import file
from fingerscroll import FingerScroll
from storesms import SMSstore

import contactdb
contacts = None

def protect(txt):
    txt = txt.replace('&', '&amp;')
    txt = txt.replace('<', '&lt;')
    txt = txt.replace('>', '&gt;')
    return txt

class newmsg(file):
    # display either (SMS) or recipient of last message
    # button is "open" to load SendSMS
    # embedded is most recent text message
    def __init__(self, name, owner):
        file.__init__(self, name, owner, "/data/SMS/newmesg")
        self.owner = self
        self.gowner = owner
        self.get_name = self.mgetname
        self.buttons = self.mbuttons
        self.embedded = self.membedded
        self.messages = 0
        self.who_from = None
        self.buffer = FingerScroll(gtk.WRAP_WORD_CHAR)
        self.buffer.show()
        self.buff = self.buffer.get_buffer()
        self.store = SMSstore("/data/SMS")
        self.last_txt = 0
        global contacts
        if contacts == None:
            contacts = contactdb.contacts()
        self.queue_draw()

    def mgetname(self):
        if self.messages == 0:
            return '(SMS)'
        if self.messages == 1:
            return self.who_from
        return '%s (+%d)' % (self.who_from, self.messages-1)

    def mbuttons(self):
        return ['Open']

    def membedded(self):
        if self.messages == 0:
            return None
        return self.buffer
    def embed_full(self):
        return False

    def press(self, ind):
        self.gowner.emit('request-window','SendSMS')

    def set_display(self, mesg):
        self.buff.delete(self.buff.get_start_iter(),
                         self.buff.get_end_iter())
        self.buff.insert(self.buff.get_end_iter(), mesg)

    def queue_draw(self):
        # newmesg has changed
        # need to get new messages and set
        # embedded, message, and from
        #
        (next, l) = self.store.lookup(None, 'NEW')
        self.messages = len(l)
        if len(l) >= 1:
            self.set_display(l[0].text)
            self.who_from = l[0].correspondent
            global contacts
            if self.who_from:
                c = contacts.find_num(self.who_from)
                if c:
                    self.who_from = c.name

            if l[0].stamp > self.last_txt:
                self.last_txt = l[0].stamp
                self.gowner.update(self, True)
        self.gowner.update(self, False)
        self.gowner.queue_draw()
        file.set_watch(self)
