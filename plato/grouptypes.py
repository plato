#
# Generic code for group types
#
# A 'group' must provide:
#
# - 'parse' to take a line from the config file and interpret it.
# - 'name' and 'format' which are present to ListSelect.
#     'format' is normally 'group'.
# - 'tasks' which provides a task list to display in the other
#    ListSelect.
#
# A 'group' can generate the signal:
# - 'new-task-list' to report that the task list has changed
# ???
#

import cmd, re

class IgnoreType:
    def __init__(self, win, name):
        self.name = name
        self.format = 'group'
        self.tasks = []

    def parse(self, line):
        pass

class ListType:
    """A ListType group parses a list of tasks out of
    the config file and provides them as a static list.
    Tasks can be:
      !command   - run command and capture text output
      (window)command - run command and expect it to appear as 'window'
      command.command - run an internal command from the given module
    In each case, arguments can follow and are treated as you would expect.
    """

    def __init__(self, win, name):
        self.name = name
        self.format = 'group'
        self.tasks = []
        self.win = win

    def parse(self, line):
        line = line.strip()
        m = re.match('^([A-Za-z0-9_ ]+)=(.*)', line)
        if m:
            name = m.groups()[0].strip()
            line = m.groups()[1].strip()
        else:
            name = None
        if line[0] == '!':
            self.parse_txt(name, line)
        elif line[0] == '(':
            self.parse_win(name, line)
        else:
            self.parse_internal(name, line)

    def parse_txt(self, name, line):
        task = cmd.ShellTask(name, line, self)
        if task.name:
            self.tasks.append(task)

    def parse_win(self, name, line):
        f = line[1:].split(')', 1)
        if len(f) != 2:
            return
        if name == None:
            name = f[0]
        task = cmd.WinTask(name, f[0], f[1], self)
        if task:
            self.tasks.append(task)

    def parse_internal(self, name, line):
        # split in to words, honouring quotes
        words = map(lambda a: a[0].strip('"')+a[1].strip("'")+a[2],
                    re.findall('("[^"]*")?(\'[^\']*\')?([^ "\'][^ "\']*)? *',
                               line))[:-1]

        cmd = words[0].split('.', 1)
        if len(cmd) == 1:
            words[0] = "internal."+cmd[0]
            cmd = ['internal',cmd[0]]
        if len(cmd) != 2:
            return

        exec "import plato_" + cmd[0]
        fn = eval("plato_"+words[0])
        if name == None:
            name = cmd[0]
        task = fn(name, self, *words[1:])
        if task:
            self.tasks.append(task)

    def update(self, task, refresh):
        if refresh:
            self.win.select(self, task)
        self.win.update(task)
    def emit(self, *args):
        self.win.emit(*args)

    def connect(self, name, *cmd):
        return self.win.connect(name, *cmd)
    def queue_draw(self):
        return self.win.queue_draw()
