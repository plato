import os, stat, time
from subprocess import Popen

class alert:
    def __init__(self, name, owner):
        blist = []
        for i in os.listdir("/etc/alert"):
            if stat.S_ISDIR(os.lstat("/etc/alert/"+i)[0]):
                blist.append(i)
        self.blist = blist

        self.embedded = lambda:None
        self.format = 'cmd'
        self.owner = owner
        try:
            self.mode = os.readlink("/etc/alert/normal")
        except:
            self.mode = '??'

    def buttons(self):
        return self.blist

    def press(self, ind):
        if ind < 0 or ind >= len(self.blist):
            return
        o = self.blist[ind]
        os.unlink("/etc/alert/normal")
        os.symlink(o, "/etc/alert/normal")
        self.mode = o
        self.owner.queue_draw()

    def get_name(self):
        return 'mode: ' + self.mode

class discrete:
    """allow entering discrete mode for some period of
    time. This creates /data/rules.d/99-discrete.
    Buttons are: 'clear' '10m' '1hr'.
    Times add to the current discression
    """
    def __init__(self, name, owner):
        self.embedded = lambda:None
        self.format = 'cmd'
        self.owner = owner
        self.check_status()

    def get_name(self):
        now = time.time() - 30
        if self.end < now:
            return "Discrete: off"
        dur = int((self.end - now)/60)
        if dur < 90:
            return "Discrete: %d mins" % dur
        return "Discrete: %d:%02d" % (int(dur/60), dur%60)

    def buttons(self):
        return ['Clear','+10m','+1hr']

    def check_status(self):
        self.end = 0
        try:
            f = open("/data/rules.d/99-discrete")
            l = f.readline()
            start = os.fstat(f.fileno()).st_mtime
            f.close()
            if len(l) > 1 and l[0] == '<':
                a = l[1:].split(':')
                self.end = start + int(a[0].strip())*60 + 30
        except:
            pass

    def set_status(self, offset):
        self.check_status()
        if self.end < self.now:
            self.end = self.now
        if offset == 0:
            self.end = 0
        else:
            self.end += offset * 60
        try:
            if self.end == 0:
                os.unlink("/data/rules.d/99-discrete")
            else:
                f = open("/data/rules.d/99-discrete","w")
                f.write('<%d : volume=0\n' % ((self.end - self.now)/60))
                f.close()
        except:
            pass

    def press(self, ind):
        self.now = time.time()
        if ind == 0:
            self.set_status(0)
        if ind == 1:
            self.set_status(10)
        if ind == 2:
            self.set_status(60)
        self.owner.queue_draw()

class gsmmode:
    def __init__(self, name, owner):
        self.embedded = lambda:None
        self.format = 'cmd'
        self.owner = owner
        try:
            fd = open("/var/lib/misc/gsm/mode")
            self.mode = fd.read(1)
            fd.close()
        except IOError:
            self.mode = '3'

    def get_name(self):
        if self.mode == '0':
            return "GSM: 2G"
        if self.mode == '1':
            return "GSM: 3G"
        if self.mode == '2':
            return "GSM: 2/3G"
        if self.mode == '3':
            return "GSM: 3/2G"
        return "GSM: ??"
    def buttons(self):
        return ['2g','3g','2/3g','3/2g']
    def press(self,ind):
        try:
            fd = open("/var/lib/misc/gsm/mode.tmp","w")
            self.mode = "%d" % ind
            fd.write(self.mode)
            fd.close()
            os.rename("/var/lib/misc/gsm/mode.tmp",
                      "/var/lib/misc/gsm/mode")
        except IOError:
            self.mode = "??"
        self.owner.queue_draw()

class rotate:
    def __init__(self,name, owner):
        self.embedded = lambda:None
        self.format = 'cmd'
        self.owner = owner
        self.mode = '??'
        self.names =  ['Normal','Left','Invert','Right']

    def get_name(self):
        return "Rotate(%s)" % self.mode
    def buttons(self):
        return self.names
    def press(self, ind):
        Popen('xrandr -o %d; xset dpms force on' % ind,
              shell = True, close_fds = True).wait()
        self.mode = self.names[ind]
        self.owner.queue_draw()

class off_mode:
    def __init__(self, name, owner):
        self.embedded = lambda:None
        self.format = 'cmd'
        self.owner = owner
        try:
            fd = open("/sys/kernel/debug/pm_debug/enable_off_mode")
            self.mode = fd.read(1)
            fd.close()
        except IOError:
            self.mode = '0'

    def get_name(self):
        if self.mode == '0':
            return 'Off Mode (no)'
        return 'Off Mode (Yes)'

    def buttons(self):
        return ['Yes','no']

    def press(self, ind):
        try:
            fd = open("/sys/kernel/debug/pm_debug/enable_off_mode","w")
            if ind:
                fd.write("0\n")
                self.mode = '0'
            else:
                fd.write("1\n")
                self.mode = '1'
            fd.close()
        except iOError:
            self.mode = '??'
            self.owner.queue_draw()
