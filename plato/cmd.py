#
# Support for running commands from the Laucher
#
# Once a command is run, we watch for it to finish
# and update status when it does.
# We also maintain window list and for commands that appear
# in windows, we associate the window with the command.
# ShellTask() runs a command and captures output in a text buffer
#  that can be displayed in a FingerScroll
# WinTask() runs a command in a window

import os,fcntl, gobject
import pango
from subprocess import Popen, PIPE
from fingerscroll import FingerScroll

class ShellTask:
    # Normally there is one button : "Run"
    # When this is pressed we create a 'FingerScroll' text buffer
    # to hold the output.
    # The button then becomes 'ReRun'
    # When we get deselected, the buffer gets hidden and we get
    # new button 'Display'
    def __init__(self, name, line, owner):
        self.format = "cmd"
        self.append = False
        self.is_full = True
        # remove leading '!'
        line = line[1:]
        if line[0] == '-':
            self.is_full = False
            line = line[1:]
        if line[0] == '+':
            self.append = True
            line = line[1:]
        if name == None:
            self.name = line
        else:
            self.name = name
        self.cmd = line
        self.buffer = None
        self.displayed = False
        self.job = None
        self.owner = owner

    def buttons(self):
        if self.displayed:
            if self.job:
                return ['-','Kill']
            else:
                return ['ReRun', 'Close']
        if self.buffer != None:
            if self.job:
                return ['-', 'Display']
            else:
                return ['Run', 'Display']
        return ['Run']

    def embedded(self):
        if self.displayed:
            return self.buffer
        return None
    def embed_full(self):
        return self.is_full

    def press(self, num):
        if num == 1:
            if self.displayed and self.job:
                # must be a 'kill' request'
                os.kill(self.job.pid, 15)
                return
            self.displayed = not self.displayed
            self.owner.update(self, False)
            return

        if self.job:
            return

        if self.buffer == None:
            self.buffer = FingerScroll()
            self.buffer.show()
            self.buffer.connect('hide', self.unmap_buff)

            fd = pango.FontDescription('Monospace 10')
            fd.set_absolute_size(15*pango.SCALE)
            self.buffer.modify_font(fd)
        self.buff = self.buffer.get_buffer()
        if not self.append:
            self.buff.delete(self.buff.get_start_iter(), self.buff.get_end_iter())
        # run the program
        self.job = Popen(self.cmd, shell=True, close_fds = True,
                         stdout=PIPE, stderr=PIPE)

        def set_noblock(f):
            flg = fcntl.fcntl(f, fcntl.F_GETFL, 0)
            fcntl.fcntl(f, fcntl.F_SETFL, flg | os.O_NONBLOCK)
        set_noblock(self.job.stdout)
        set_noblock(self.job.stderr)
        self.wc = gobject.child_watch_add(self.job.pid, self.done)
        self.wout = gobject.io_add_watch(self.job.stdout, gobject.IO_IN | gobject.IO_ERR | gobject.IO_HUP, self.read)
        self.werr = gobject.io_add_watch(self.job.stderr, gobject.IO_IN | gobject.IO_ERR | gobject.IO_HUP, self.read)

        self.displayed = True

    def read(self, f, dir):
        l = f.read()
        self.buff.insert(self.buff.get_end_iter(), l)
        gobject.idle_add(self.adjust)
        if l == "":
            if self.job:
                gobject.source_remove(self.wout)
                gobject.source_remove(self.werr)
                try:
                    self.job.terminate()
                except:
                    pass
                self.job.wait()
                self.job.stdout.close()
                self.job.stderr.close()
                self.job = None
                self.owner.update(self, None)
            return False
        return True

    def adjust(self):
        adj = self.buffer.vadj
        adj.set_value(adj.upper - adj.page_size)

    def done(self, *a):
        if self.job:
            self.read(self.job.stdout, None)
            self.read(self.job.stderr, None)
        if self.job:
            gobject.source_remove(self.wout)
            gobject.source_remove(self.werr)
            self.job.stdout.close()
            self.job.stderr.close()
        self.job = None
        self.owner.update(self, None)

    def unmap_buff(self, widget):
        if self.job == None:
            self.displayed = False


class WinTask:
    # A WinTask runs a command and expects it to
    # create a window.  This window can then be
    # raised or closed, or the process killed.
    # we find out about windows appearing
    # by connecting to the 'new-window' signal in
    # the owner
    # When there is no process, button in "Run"
    # When there is a process but no window,  "Kill"
    # When there is process and window, "Raise", "Close", "Kill"
    def __init__(self, name, window, line, owner):
        self.name = name
        self.job = None
        self.win_id = None
        self.win = None
        self.window_name = window
        self.cmd = line
        self.embedded = lambda:None
        self.owner = owner
        owner.connect('new-window', self.new_win)
        owner.connect('lost-window', self.lost_win)
        owner.connect('request-window', self.request_win)

    def buttons(self):
        if not self.job and not self.win_id:
            return ['Run']
        if not self.win_id:
            return ['-','Kill']
        if not self.job:
            return ['Raise','Close']
        return ['Raise','Close','Kill']

    def get_format(self):
        if self.job or self.win:
            return "win"
        else:
            return "cmd"

    def press(self, num):
        if not self.job and not self.win:
            if num != 0:
                return
            self.job = Popen(self.cmd, shell=True, close_fds = True)
            self.wc = gobject.child_watch_add(self.job.pid, self.done)
            return

        if not self.win_id and self.job:
            if num != 1:
                return
            os.kill(self.job.pid, 15)
            return

        # We have a win_id and a job
        if num == 0:
            self.win.raise_win()
        elif num == 1:
            self.win.close_win()
        elif num == 2 and self.job:
            os.kill(self.job.pid, 15)

    def done(self, *a):
        self.job = None
        self.win_id = None
        self.owner.update(self, None)

    def new_win(self, source, win):
        if self.job and self.job.pid == win.pid:
            self.win_id = win.id
            self.win = win
        if self.window_name and self.window_name == win.name:
            self.win_id = win.id
            self.win = win
        if self.win_id:
            self.format = "win"
        self.owner.update(self, None)

    def lost_win(self, source, id):
        if self.win_id == id:
            self.win_id = None
            self.win = None
            self.format = "cmd"
            self.owner.update(self, None)

    def request_win(self, source, name):
        if self.window_name == name:
            self.press(0)
