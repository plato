#!/usr/bin/env python

#TODO
# aux button
# calculator
# sms
#   entry shows number of new messages
#   embed shows latest new message if there is one
#   buttons allow 'read' or 'open'
# phone calls
#   number of missed calls, or incoming number
#   embed shows call log, or tap board
#   buttons allow 'call' or 'answer' or 'open' ....
# embed calendar
#   selected by 'date'
#   tapping can select a date
# run windowed program
# monitor list of windows
# address list
#   this is a 'group' where 'tasks' are contacts


# Launcher, version 2.
# This module just defines the UI and plugin interface
# All tasks etc go in loaded modules.
#
# The elements of the UI are:
#
#   ----------------------------------
#   |  Text (Entry) box (one line)   |
#   +--------------------------------+
#   |                |               |
#   |    group       |      task     |
#   |   selection    |     selection |
#   |    list        |      list     |
#   |                |               |
#   |                |               |
#   +----------------+               |
#   |   optional     |               |
#   |   task-        |               |
#   |     specific   |               |
#   |   widget       +---------------|
#   |                | secondary     |
#   |                | button row    |
#   +----------------+---------------+
#   |         Main Button Row        |
#   +--------------------------------+
#
# If the optional widget is present, then the Main Button Row
# disappears and the secondary button row is used instead.
#
# The selection lists auto-scroll when an item near top or bottom
# is selected.  Selecting an entry only updates other parts of
# the UI and does not perform any significant action.
# Actions are performed by buttons which appear in a button
# row.
# The optional widget can be anything provided by the group
# or task, for example:
#  - tap-board for entering text
#  - calendar for selecting a date
#  - display area for small messages (e.g. SMS)
#  - preview during file selection
#  - map of current location
#
# Entered text alway appears in the Text Entry box and
# is made available to the current group and task
# That object may use the text and may choose to delete it.
# e.g.
#   A calculator task might display an evaluation of the text
#   A call task might offer to call the displayed number, and
#     delete it when the call completes
#   An address-book group might restrict displayed tasks
#     to those which match the text
#   A 'todo' task might set the text to match the task content,
#     then update the content as the text changes.
#
# A config file describes the groups.
# It is .platorc in $HOME
# A group is enclosed in [] and can have an optional type:
#  [group1]
#  [Windows/winlist]
# If no type is given the 'list' type is used.
# this parses following lines to create a static list of tasks.
# Other possible types are:
#   winlist: list of windows on display
#   contacts: list of contacts
#   notifications: list of recent notifications (alarm/battery/whatever)
#
# the 'list' type is described in more detail in 'grouptypes'.
#

import sys, gtk, os, pango
import gobject

if __name__ == '__main__':
    sys.path.insert(1, '/home/neilb/home/freerunner/lib')
    sys.path.insert(1, '/home/neilb/home/freerunner/sms')
    sys.path.insert(1, '/root/lib')

from tapboard import TapBoard
import grouptypes, listselect, scrawl
from grouptypes import *
from window_group import *
from evdev import EvDev

import plato_gsm

class MakeList:
    # This class is a wrapper on a 'tasklist' which presents an
    # entry list that can be used by ListSelect.
    # Each entry in the list is a name and a format
    # The group can either contain a list of task: tasks
    # or a function: get_task
    # in the later case the length is reported as -1
    #
    # a task must provide a .name or a get_name function
    # similarly a .format or a .get_format function
    def __init__(self, group):
        self.group = group

    def __getitem__(self, ind):
        i = self.get_item(ind)
        if i == None:
            return None
        try:
            name = i.name
        except AttributeError:
            name = i.get_name()
        try:
            format = i.format
        except AttributeError:
            format = i.get_format()
        return (name, format)

    def __len__(self):
        try:
            e = self.group.tasks
            l = len(e)
        except AttributeError:
            l = 50000
        return l

    def get_item(self, ind):
        try:
            e = self.group
            if ind >= 0 and ind < len(e):
                i = e[ind]
            else:
                i = None
        except AttributeError:
            try:
                e = self.group.tasks
                if ind >= 0 and ind < len(e):
                    i = e[ind]
                else:
                    i = None
            except AttributeError:
                i = self.group.get_task(ind)
        return i


class LaunchWin(gtk.Window):
    __gsignals__ = {
        'new-window' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                        (gobject.TYPE_PYOBJECT,)),
        'lost-window' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         (gobject.TYPE_INT,)),
        'request-window' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         (gobject.TYPE_STRING,)),
        }
    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)
        self.set_default_size(480, 640)
        self.connect('destroy', lambda w: gtk.main_quit())

        self.embeded_widget = None
        self.embeded_full = False
        self.hidden = True
        self.next_group = 0
        self.widgets = {}
        self.buttons = []
        self.create_ui()
        self.load_config()
        self.grouplist.select(0)

    def create_ui(self):
        # Create the UI framework, first the components

        # The Entry
        e1 = gtk.Entry()
        e1.set_alignment(0.5) ; # Center text
        e1.connect('changed', self.entry_changed)
        e1.connect('backspace', self.entry_changed)
        e1.show()
        self.entry = e1

        # Label for the entry
        lbl = gtk.Label("Label")
        lbl.hide()
        self.label = lbl

        # The text row: Entry fills, label doesn't
        tr = gtk.HBox()
        tr.pack_start(lbl, expand = False)
        tr.pack_end(e1, expand = True)
        tr.show()

        # The group list
        l1 = listselect.ListSelect(center = False, linescale = 1.3, markup = True)
        l1.connect('selected', self.group_select)
        # set appearance here
        l1.set_colour('group','blue')
        self.grouplist = l1
        l1.set_zoom(35)
        l1.show()

        # The task list
        l2 = listselect.ListSelect(center = True, linescale = 1.3, markup = True)
        l2.connect('selected', self.task_select)
        l2.set_colour('cmd', 'black')
        l2.set_colour('win', 'blue')
        l2.set_zoom(35)
        # set appearance
        self.tasklist = l2
        l2.show()

        # The embedded widget: provide a VBox as a place holder
        v1 = gtk.VBox()
        self.embed_box = v1

        # The Main button box - buttons are added later
        h1 = gtk.HBox(True)
        self.main_buttons = h1
        # HACK
        h1.set_size_request(-1, 80)
        h1.show()

        # The Secondary button box
        h2 = gtk.HBox(True)
        h2.set_size_request(-1, 60)
        self.secondary_buttons = h2

        # Now make the two columns

        v2 = gtk.VBox(True)
        v2.pack_start(self.grouplist, expand = True)
        v2.pack_end(self.embed_box, expand = False)
        v2.show()

        v3 = gtk.VBox()
        v3.pack_start(self.tasklist, expand = True)
        v3.pack_end(self.secondary_buttons, expand = False)
        v3.show()

        # and bind them together
        h3 = gtk.HBox(True)
        h3.pack_start(v2, expand=True)
        h3.pack_end(v3, expand=True)
        h3.show()

        # A vbox to hold tr and main buttons
        v3a = gtk.VBox()
        v3a.show()
        v3a.pack_start(tr, expand=False)
        v3a.pack_end(h3, expand=True)
        self.non_buttons = v3a

        # a box for a 'full screen' embedded widget
        v3b = gtk.VBox()
        v3b.hide()
        self.embed_full_box = v3b


        # And now one big vbox to hold it all
        v4 = gtk.VBox()
        v4.pack_start(self.non_buttons, expand=True)
        v4.pack_start(self.embed_full_box, expand=True)
        v4.pack_end(self.main_buttons, expand=False)
        v4.show()
        self.add(v4)
        self.show()

        ## We want writing recognistion to work
        ## over the whole middle section.  Only that
        ## turns out to be too hard for my lowly gtk
        ## skills.  So we do recognition separately
        ## on each selection box only.
        s1 = scrawl.Scrawl(l1, self.getsym, lambda p: l1.tap(p[0],p[1]))
        s2 = scrawl.Scrawl(l2, self.getsym, lambda p: l2.tap(p[0],p[1]))
        s1.set_colour('red')
        s2.set_colour('red')


        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(30 * pango.SCALE)
        self.button_font = fd;
        self.entry.modify_font(fd)
        self.label.modify_font(fd)

    def load_config(self):
        fname = os.path.join(os.environ['HOME'], ".platorc")
        types = {}
        types['ignore'] = IgnoreType
        types['list' ] = ListType
        types['winlist'] = WindowType
        types['call_list']=plato_gsm.call_list
        groups = []
        f = open(fname)
        gobj = None
        for line in f:
            l = line.strip()
            if not l:
                continue
            if l[0] == '[':
                l = l.strip('[]')
                f = l.split('/', 1)
                group = f[0]
                if len(f) > 1:
                    group_type = f[1]
                else:
                    group_type = "list"
                if group_type in types:
                    gobj = types[group_type](self, group)
                else:
                    gobj = types['ignore'](self, group)
                groups.append(gobj)
            elif gobj != None:
                gobj.parse(l)
        self.grouplist.list = MakeList(groups)
        self.grouplist.list_changed()


    def entry_changed(self, entry):
        print "fixme", entry.get_text()

    def group_select(self, list, item):
        g = list.list.get_item(item)
        self.tasklist.list = MakeList(g)
        self.tasklist.list_changed()
        self.tasklist.select(None)
        if self.tasklist.list != None:
            self.task_select(self.tasklist,
                             self.tasklist.selected)

    def set_group(self, group):
        self.tasklist.list = MakeList(group)
        self.tasklist.list_changed()
        self.tasklist.select(None)
        if self.tasklist.list != None:
            self.task_select(self.tasklist,
                             self.tasklist.selected)

    def task_select(self, list, item):
        if item == None:
            self.set_buttons(None)
            self.set_embed(None, None)
        else:
            task = self.tasklist.list.get_item(item)
            if task:
                self.set_buttons(task.buttons())
                bed = task.embedded()
                full = False
                if bed != None and type(bed) != str:
                    full = task.embed_full()
                self.set_embed(bed, full)

    def update(self, task):
        if self.tasklist.selected != None and \
           self.tasklist.list.get_item(self.tasklist.selected) == task:
            self.set_buttons(task.buttons())
            bed = task.embedded()
            full = False
            if bed != None and type(bed) != str:
                full = task.embed_full()
            self.set_embed(bed, full)

    def select(self, group, task):
        i = 0
        gl = self.grouplist.list
        while i < len(gl) and gl.get_item(i) != None:
            if gl.get_item(i) == group:
                self.grouplist.select(i)
                break
            i += 1
        tl = self.tasklist.list
        i = 0
        while i < len(tl) and tl.get_item(i) != None:
            if tl.get_item(i) == task:
                self.tasklist.select(i)
                self.present()
                break
            i += 1

    def set_buttons(self, list):
        if not list:
            # hide the button boxes
            self.secondary_buttons.hide()
            self.main_buttons.hide()
            self.buttons = []
            return
        if self.same_buttons(list):
            return

        self.buttons = list
        self.update_buttons(self.main_buttons)
        self.update_buttons(self.secondary_buttons)
        if self.embeded_widget and not self.embeded_full:
            self.main_buttons.hide()
            self.secondary_buttons.show()
        else:
            self.secondary_buttons.hide()
            self.main_buttons.show()

    def same_buttons(self, list):
        if len(list) != len(self.buttons):
            return False
        for i in range(len(list)):
            if list[i] != self.buttons[i]:
                return False
        return True

    def update_buttons(self, box):
        # make sure there are enough buttons
        have = len(box.get_children())
        need = len(self.buttons) - have
        if need > 0:
            for i in range(need):
                b = gtk.Button("?")
                b.child.modify_font(self.button_font)
                b.set_property('can-focus', False)
                box.add(b)
                b.connect('clicked', self.button_pressed, have + i)
            have += need
        b = box.get_children()
        # hide extra buttons
        if need < 0:
            for i in range(-need):
                b[have-i-1].hide()
        # update each button
        for i in range(len(self.buttons)):
            b[i].child.set_text(self.buttons[i])
            b[i].show()

    def button_pressed(self, widget, num):
        self.hidden = True
        self.tasklist.list.get_item(self.tasklist.selected).press(num)
        self.task_select(self.tasklist,
                         self.tasklist.selected)

    def set_embed(self, widget, full):
        if type(widget) == str:
            widget, full = self.make_widget(widget)

        if widget == self.embeded_widget and full == self.embeded_full:
            return
        if self.embeded_widget:
            self.embeded_widget.hide()
            if self.embeded_full:
                self.embed_full_box.remove(self.embeded_widget)
            else:
                self.embed_box.remove(self.embeded_widget)
            self.embeded_widget = None
        self.embeded_widget = widget
        self.embeded_full = full
        if widget and not full:
            self.embed_box.add(widget)
            widget.show()
            self.main_buttons.hide()
            self.embed_box.show()
            self.non_buttons.show()
            self.embed_full_box.hide()
            if self.buttons:
                self.secondary_buttons.show()
        elif widget:
            self.embed_full_box.add(widget)
            widget.show()
            self.embed_full_box.show()
            self.non_buttons.hide()
            if self.buttons:
                self.main_buttons.show()
        else:
            self.embed_box.hide()
            self.embed_full_box.hide()
            self.non_buttons.show()
            self.secondary_buttons.hide()
            if self.buttons:
                self.main_buttons.show()

    def make_widget(self, name):
        if name in self.widgets:
            return self.widgets[name]
        if name == "tapboard":
            w = TapBoard()
            def key(w, k):
                if k == '\b':
                    self.entry.emit('backspace')
                elif k == 'Return':
                    self.entry.emit('activate')
                elif len(k) == 1:
                    self.entry.emit('insert-at-cursor', k)
            w.connect('key', key)
            self.widgets[name] = (w, False)
            return (w, False)
        return None

    def getsym(self, sym):
        print "gotsym", sym
        if sym == '<BS>':
            self.entry.emit('backspace')
        elif sym == '<newline>':
            self.entry.emit('activate')
        elif len(sym) == 1:
            self.entry.emit('insert-at-cursor', sym)

    def activate(self, *a):
        # someone pressed a button or icon to get our
        # attention.
        # We raise the window and if nothing has happened recently,
        # select first group
        if self.hidden:
            self.hidden = False
            self.next_group = 0
        else:
            if (self.next_group > len(self.grouplist.list) or
                self.grouplist.list[self.next_group] == None):
                self.next_group = 0
            self.grouplist.select(self.next_group)
            self.next_group += 1

        self.present()

    def ev_activate(self, down, moment, typ, code, value):
        if typ != 1:
            # not a key press
            return
        if code != 169 and code != 116:
            # not AUX or Power
            return
        if value == 0:
            # down press, wait for up
            self.down_at = moment
            return
        self.activate()

class LaunchIcon(gtk.StatusIcon):
    def __init__(self, callback):
        gtk.StatusIcon.__init__(self)
        self.set_from_stock(gtk.STOCK_EXECUTE)
        self.connect('activate', callback)


if __name__ == '__main__':
    sys.path.insert(1, '/home/neilb/home/freerunner/lib')
    sys.path.insert(1, '/root/lib')
    l = LaunchWin()
    i = LaunchIcon(l.activate)
    try:
        aux = EvDev("/dev/input/aux", l.ev_activate)
    except OSError:
        aux = None

    gtk.settings_get_default().set_long_property("gtk-cursor-blink", 0, "main")
    gtk.main()
