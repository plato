#
# A plato 'group' of windows.
# This needs to be present for any window management
# to work. i.e. a WinTask won't see the window unless this
# was been activated.

from wmctrl import *
import gobject

class Wwrap:
    # Wrap a window from wmctrl as a Task
    def __init__(self, w):
        self.w = w
        self.name = w.name
        self.format = "blue"
        self.embedded = lambda:None

    def buttons(self):
        if self.w.pid > 0:
            return ['Raise', 'Close', 'Kill']
        return ['Raise','Close']

    def press(self, ind):
        if ind == 0:
            self.w.raise_win()
            return
        if ind == 1:
            self.w.close_win()
            return
        if ind == 2:
            if self.w.pid > 1:
                os.kill(self.w.pid, 15)
            return


class win_refresh:
    def __init__(self, winlist):
        self.format='cmd'
        self.embedded = lambda:None
        self.winlist = winlist

    def get_name(self):
        return "Windows"
    def buttons(self):
        return ["Refresh"]
    def press(self, ind):
        self.winlist.get_list()

class WindowType:
    def __init__(self, win, name):
        self.owner = win
        self.format = 'group'
        self.name = name
        self.buttons = lambda:None
        self.embedded = lambda:None
        self.ignore = []
        self.list = winlist(add_handle = self.add)
        self.current = {}
        self.my_refresh = win_refresh(self.list)
        gobject.io_add_watch(self.list.fd, gobject.IO_IN, self.list.events)
        self.list.on_change(self.change, self.add, self.delete)

    def parse(self, line):
        # any window names listed in config file are ignored
        self.ignore.append(line)

    def get_task(self, ind):
        if ind == 0:
            return self.my_refresh
        ind -= 1
        w = self.list.winfo
        if ind >= len(w):
            return None
        return Wwrap(w[self.list.windows[ind]])

    def change(self):
        self.owner.queue_draw()

    def add(self, window):
        print "emit new window", window.name
        self.owner.emit('new-window',window)

    def delete(self, wid):
        self.owner.emit('lost-window',wid)
