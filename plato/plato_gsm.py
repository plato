
#
# plato plugins for mobile phone functionality.
# carrier:
#     displays current carrier
#     button will search for possible carriers
#     while search, button show how long search has progressed
#     on succcess, get one button per carrier valid for 2 minutes
#     When one is selected, write that to /run/gsm/request_carrier
#
# calls:
#     display incoming phone number and when there is one, allows
#     'answer' or 'reject', then '-' or 'hangup'
#     When no call, buttons for 'dial' or 'contacts'
#
# newmsg:
#     displays new message and allows more to be shown in embedded
#     also allows sms reader to be run

from plato_internal import file
from subprocess import Popen, PIPE
import os.path, fcntl, time, gobject, re, gtk

def record(key, value):
    f = open('/run/gsm-state/.new.' + key, 'w')
    f.write(value)
    f.close()
    os.rename('/run/gsm-state/.new.' + key,
              '/run/gsm-state/' + key)


def recall(key):
    try:
        fd = open("/run/gsm-state/" + key)
        l = fd.read(1000)
        fd.close()
    except IOError:
        l = ""
    return l.strip()

class carrier(file):
    def __init__(self, name, owner):
        file.__init__(self, name, owner, "/run/gsm-state/carrier")
        self.buttons = self.cbuttons

        self.carriers = []
        self.carrier_buttons = None
        self.carrier_valid = 0
        self.reader = None
        self.job = None
        self.job_start = 0
        self.timeout = None

    def cbuttons(self):
        if self.job:
            # waiting for carriers - can cancel
            if self.timeout == None:
                self.timeout = gobject.timeout_add(3000, self.change_button)
            waited = time.time() - self.job_start
            return [ 'Cancel after %d seconds' % waited]

        if self.carrier_buttons and time.time() - self.carrier_valid < 120:
            return self.carrier_buttons
        self.carrier_buttons = None
        if (os.path.exists('/run/gsm-state/request_carrier') and
            os.path.getsize('/run/gsm-state/request_carrier') > 1):
                return ['Search Carriers','Cancel Request']
        return ['Search Carriers']

    def change_button(self):
        self.timeout = None
        self.owner.update(self, None)

    def press(self, ind):
        if self.job:
            if ind == 0:
                self.job.terminate()
            return

        if self.carriers and self.carrier_buttons:
            if ind < len(self.carriers):
                num = self.carriers[ind][0]
                record('request_carrier',num)
                self.carrier_buttons = None
            return

        if ind == 1:
            record('request_carrier', '')
            return
        if ind != 0:
            return

        # initiate search
        # run gsm-carriers expecting output like:
        # 50503 0 "voda AU" "vodafone AU"
        # record output in carriers[] and short names in carrer_buttons
        self.job = Popen("gsm-carriers", shell=True, close_fds=True,
                         stdout = PIPE)
        self.job_start = time.time()
        gobject.child_watch_add(self.job.pid, self.done)
        flg = fcntl.fcntl(self.job.stdout, fcntl.F_GETFL, 0)
        fcntl.fcntl(self.job.stdout, fcntl.F_SETFL, flg | os.O_NONBLOCK)
        self.buf = ''
        self.reader = gobject.io_add_watch(self.job.stdout, gobject.IO_IN,
                                           self.read)

    def read(self, f, dir):
        try:
            l = f.read()
        except IOError:
            l = None
        if l == None:
            if self.job.poll() == None:
                return True
        elif l != '':
            self.buf += l
            return True
        self.job.wait()
        lines = self.buf.split('\n')
        c=[]
        b=[]
        for line in lines:
            words = re.findall('([^"][^ ]*|"[^"]*["]) *', line)
            if len(words) == 4:
                c.append(words)
                b.append(words[2].strip('"'))
        self.carriers = c
        self.carrier_buttons = b
        self.carrier_valid = time.time()
        self.job = None
        self.owner.update(self, None)
        return False

    def done(self, *a):
        if not self.job:
            return
        self.job.wait()
        #flg = fcntl.fcntl(self.job.stdout, fcntl.F_GETFL, 0)
        #fcntl.fcntl(self.job.stdout, fcntl.F_SETFL, flg & ~os.O_NONBLOCK)
        self.read(self.job.stdout, None)


class calls(file):
    # Monitor 'status' which can be
    #   INCOMING BUSY on-call or empty
    # INCOMING:
    #   get caller from 'incoming' and display - via contacts if possibe
    #        Buttons are 'answer','reject'
    # BUSY:
    #   display BUSY
    #        Buttons are '-', 'cancel'
    # on-call:
    #   Buttons are 'hold', 'cancel'
    # empty:
    #   display (call logs)
    #     Buttons are "Received" "Dialled" "Dialer" "Contacts"
    def __init__(self, name, owner):
        file.__init__(self, name, owner, '/run/gsm-state/status')
        self.buttons = self.cbuttons
        self.get_name = self.cgetname
        self.status = ''
        self.choose_logs = False
        self.caller = None
        self.dialer_win = None
        self.contacts_win = None
        self.dj = None
        self.cj = None
        # catch queue_draw from 'file'
        self.owner = self
        self.gowner = owner
        owner.connect('new-window', self.new_win)
        owner.connect('lost-window', self.lost_win)

    def cbuttons(self):
        if self.status == '' or self.status == '-':
            if self.choose_logs:
                return ['Call', "Recv'd\nCalls","Dialed\nCalls"]
            else:
                return ['Logs', "Dialer", "Contacts"]
        if self.status == 'INCOMING':
            return ['Answer','Reject']
        if self.status == 'BUSY':
            return ['-','Cancel']
        if self.status == 'on-call':
            return ['Hold','Cancel']
        return []

    def cgetname(self):
        status = file.get_name(self)
        if self.status != status:
            self.status = status
            print "update for", status
            # was "status == 'INCOMING'" to raise on incoming calls
            # but want Dailer to do that
            self.gowner.update(self, False)
        if self.status == 'INCOMING':
            if self.caller == None:
                self.caller = recall('incoming')
                if self.caller == '':
                    self.caller = '(private)'
                else:
                    contacts = contactdb.contacts()
                    n = contacts.find_num(self.caller)
                    if n:
                        self.caller = n.name
        else:
            self.caller = None
        if self.status == '' or self.status == '-':
            if self.choose_logs:
                return '(call logs)'
            else:
                return '(no call)'
        if self.status == 'INCOMING':
            return self.caller + ' calling'
        if self.status == 'BUSY':
            return 'BUSY'
        if self.status == 'on-call':
            return "On Call"
        return '??'+self.status

    def press(self, ind):
        if self.status == '' or self.status == '-':
            if self.choose_logs:
                if ind == 0:
                    self.choose_logs = False
                elif ind == 1:
                    self.gowner.win.set_group(call_list(self.gowner.win, '', True))
                elif ind == 2:
                    self.gowner.win.set_group(call_list(self.gowner.win, '', False))
            else:
                if ind == 0:
                    self.choose_logs = True
                elif ind == 1:
                    if self.dialer_win:
                        self.dialer_win.raise_win()
                    elif not self.dj:
                        self.dj = Popen("dialer", shell=True, close_fds = True)
                        gobject.child_watch_add(self.dj.pid, self.djdone)
                elif ind == 2:
                    if self.contacts_win:
                        self.contacts_win.raise_win()
                    elif not self.cj:
                        self.cj = Popen("contacts", shell=True, close_fds = True)
                        gobject.child_watch_add(self.cj.pid, self.cjdone)
            self.gowner.update(self, None)
            return

        if self.status == 'INCOMING':
            if ind == 0:
                record('call','answer')
            elif ind == 1:
                record('call','')
            return

        if ind == 0:
            print "on hold??"
            return
        if ind == 1:
            record('call','')

    def queue_draw(self):
        # file changed
        self.cgetname()
        self.gowner.queue_draw()

    def djdone(self, *a):
        self.dj.wait()
        self.dj = None
    def cjdone(self, *a):
        self.cj.wait()
        self.cj = None

    def new_win(self, source, win):
        if win.name == "Dialer":
            self.dialer_win = win
        if win.name == "Contacts":
            self.contacts_win = win

    def lost_win(self, source, id):
        if self.dialer_win and self.dialer_win.id == id:
            self.dialer_win = None
        if self.contacts_win and self.contacts_win.id == id:
            self.contacts_win = None

import contactdb

contacts = None

def friendly_time(tm, secs):
    now = time.time()
    if secs > now or secs < now - 7*24*3600:
        return time.strftime("%Y-%m-%d %H:%M:%S", tm)
    age = now - secs
    if age < 12*3600:
        return time.strftime("%H:%M:%S", tm)
    return time.strftime("%a %H:%M:%S", tm)

class corresp:
    # This task represents a correspondant in the
    # lists of dialled numbers and  received calls
    # It displays the contact name/number and time/date
    # in a friendly form
    # If there is a number we provide a button to call-back
    # and one to find contact
    def __init__(self, name, owner, when):
        global contacts
        self.format="brown"
        self.owner = owner
        self.number = name
        self.embedded = lambda:None
        dt, tm = when
        self.when = time.strptime(dt+'='+tm, '%Y-%m-%d=%H:%M:%S')
        self.secs = time.mktime(self.when)

        if contacts == None:
            contacts = contactdb.contacts()
        if name == None:
            self.contact = "-private-number-"
        else:
            n = contacts.find_num(name)
            if n:
                self.contact = n.name
            else:
                self.contact = name

    def get_name(self):
        w = friendly_time(self.when, self.secs)
        return '<span size="xx-small">%s</span>\n<span size="xx-small">%s</span>' % (
            self.contact, w)

    def buttons(self):
        if self.number:
            return ['Call','Txt','Contacts']
        return []

    def press(self, ind):
        if ind == 0:
            record('call',self.number)
            send_number('voice-dial', self.number)
            self.owner.emit('request-window','Dialer')
        if ind == 1:
            send_number('sms-new', self.number)
            self.owner.emit('request-window','SendSMS')
        if ind == 2:
            send_number('contact-find', self.number)
            self.owner.emit('request-window','Contacts')

sel_number = None
clips = {}
def clip_get_data(clip, sel, info, data):
    global sel_number
    sel.set_text(sel_number)
def clip_clear_data(clip, data):
    global sel_number
    sel_number = None

def send_number(sel, num):
    global sel_number, clips
    sel_number = num
    if sel not in clips:
        clips[sel] = gtk.Clipboard(selection = sel)
    c = clips[sel]
    c.set_with_data([(gtk.gdk.SELECTION_TYPE_STRING, 0, 0)],
                    clip_get_data, clip_clear_data, None)


class call_list:
    # A list of 'corresp' tasks taken from /var/log/incoming or
    # /var/log/outgoing
    def __init__(self, win, name, incoming = None):
        self.win = win
        if incoming == None:
            incoming = name == 'incoming'

        if incoming:
            self.list = incoming_list(self.update)
            self.name = "Incoming Calls"
        else:
            self.list = outgoing_list(self.update)
            self.name = "Outgoing Calls"
        self.format = 'group'

    def get_task(self, ind):
        if ind >= len(self.list):
            return None
        start, end, num = self.list[ind]
        return corresp(num, self, start)

    def update(self):
        self.win.queue_draw()
    def emit(self, *a):
        self.win.emit(*a)


import dnotify
class incoming_list:
    # present as a list of received calls
    # and notify whenever it changes
    # Each entry is a triple (start, end, number)
    # start and end are (date, time)
    def __init__(self, notify, file='incoming'):
        self.list = []
        self.notify = notify
        self.watch = dnotify.dir("/var/log")
        self.fwatch = self.watch.watch(file, self.changed)
        self.changed()
    def __len__(self):
        return len(self.list)
    def __getitem__(self, ind):
        return self.list[-1-ind]
    def flush_one(self, start, end, number):
        if start:
            self.lst.append((start, end, number))
    def changed(self):
        self.lst = []

        try:
            f = open("/var/log/incoming")
        except IOError:
            f = []
        start = None; end=None; number=None
        for l in f:
            w = l.split()
            if len(w) != 3:
                continue
            if w[2] == '-call-':
                self.flush_one(start, end, number)
                start = (w[0], w[1])
                number = None; end = None
            elif w[2] == '-end-':
                end = (w[0], w[1])
                self.flush_one(start, end, number)
                start = None; end = None; number = None
            else:
                number = w[2]
                if not start:
                    start = (w[0], w[1])
        if number:
            self.flush_one(start, end, number)
        try:
            f.close()
        except AttributeError:
            pass
        self.list = self.lst
        self.notify()

class outgoing_list(incoming_list):
    # present similar list for outgoing calls
    def __init__(self, notify):
        incoming_list.__init__(self, notify, file='outgoing')

    def changed(self):
        self.lst = []
        try:
            f = open("/var/log/outgoing")
        except IOError:
            f = []
        start = None; end=None; number=None
        for l in f:
            w = l.split()
            if len(w) != 3:
                continue
            if w[2] == '-end-':
                end = (w[0], w[1])
                self.flush_one(start, end, number)
                start = None; end = None; number = None
            else:
                self.flush_one(start, end, number)
                start = (w[0], w[1])
                number = w[2]
        if number:
            self.flush_one(start, end, number)
        try:
            f.close()
        except AttributeError:
            pass
        self.list = self.lst
        self.notify()


