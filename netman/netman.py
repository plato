#!/usr/bin/env python

#TODO
# handle iptables masquerade directly so it can be tuned to the IP address
# discovered.
#  	up iptables -t nat -A POSTROUTING -s 192.168.202.16/29 -j MASQUERADE
#	down iptables -t nat -D POSTROUTING -s 192.168.202.16/29 -j MASQUERADE
#  This should be done on any interface that is the 'hotspot'
#DONE - rfkill unblock
#DONE - kill children on exit
#- add 3G support
#  - connect
#  - disconnect
#  - poll for status
#  - configure access point
#- USB detect presence of connector?
#- wifi:
#  - report crypto status and strength
#DONE  - extract 'id=' from CONNECTED to find current
#DONE  - notice when wifi goes away
#      CTRL-EVENT-DISCONNECTED
#DONE  - if no dhcp response for a while, reassociate
#  - config page to:
#     list known and visible networks
#     disble/enable, forget, set-password
#     make sure to save config
#     kill supplicant when done?
#DONE  - ensure label is refreshed on different connect and disconnect stages.
#
# Adding a network involves:
#  add_network - returns $N
#  set_network $N ssid "Netid"
#  set_network $N key_mgmt WPA-PSK
#  set_network $N psk "Password"
#  enable_network $N

# Manage networks for the openmoko phoenux
# There are 4 devices (unless I add VPN support)
# USB, WIFI, WWAN, Bluetooth
#
# USB is normally on, though it can be turned off to
# allow a different USB widget.
# WIFI, WWAN, Bluetooth need to be explicitly enabled
# for now at least
# There is only one default route, and one DNS server
# If WWAN is active, it provides route and DNS else
# if WIFI is active and DHCP responds, it provides route and DNS
# else if BT is active and DHCP responds, it provides route and DNS
# else USB should provide route and DNS
#
# When we have a route, we provide DHCP to other interfaces
# using dnsmasq, and provide masquarading to Internet.
#
# Main page shows each interface with status including
#  IP address
# One can be selected, and bottom buttons can be used to
#   enable / disable, hotspot,  and configure
# 'configure' goes to a new page, different for each interface.
#
# listen configures the interface to allow incoming...
#
# WWAN: configure allows APN to be set.  This is stored per SIM card.
# WIFI: lists known and visible ESSIDs and show password which can be
#    editted.  Also bottom buttons for 'activate' and 'forget' or 'allow'.
# BT: lists visible peers which support networking and allows 'bind'
#    or 'connect'
# USB: ???
#

# WWAN in managed using atchan talking to the ttyHS_Control port
#   We cannot hold it open, so poll status occasionally.
#   ifconfig and route is managed directly.
# WIFI is managed by talking to wpa_supplicant over a socket.
#   ifconfig up/down might need to be done directly for power management
#   udhcp is managed directly
# BT ?? don't know yet
# USB if managed with ifup/ifdown
#
# dnsmasq is stopped and restarted as needed with required options.
#
# Addresses:
# This program doesn't know about specific IP addresses, that all
# lives in config files.  However the plan is as follows:
# Everything lives in a single class-C: 192.168.202.0/24
# usb-server gets 6 addresses:  0/29   1 is me, 2-7 is you
# usb-client gets same 6 addresses:  0/29   2 is you, 1 is me
# wifi-hotspot gets 6 addresses: 8/29  9 is me, 10-14 are for clients
# bt-server gets 6 addresses:  16/29   17 is me, 18-23 are for clients
#
# dnsmasq only responds to DHCP on 1, 9, 17
#
# only one of 3g, wifi, bt, usb-client can be active at at time
# They set the server in resolv.conf, and set a default.
# usb-hotspot, wifi-hotspot, bt-hotspot set up IP masqurading.
# 
#

# so...
# Each of 'usb', 'wifi', 'bt', can be in 'hotspot' mode at any time.
# Of 'wifi', 'gsm', 'bt', 'usb', the first that is active and accessable
# and not configured for 'hotspot' is configured as default route and others
# that are not 'hotspot' are disabled - except 'usb' which becomes
# 'p2p' for local traffic only.
#
# An interface can be in one of several states:
#  - hotspot (not gsm) - active and providing access
#  - disabled - explicitly disabled, not to be used
#  - active - has found a connection, provides the default route
#  - over-ridden - a higher-precedence interface is active
#  - pending - no higher interface is active, but haven't found connection yet.
#
# When 'active' is released, we switch to 'disabled' and deconfig interface
# When 'active' is pressed, we switch to 'pending' and rescan which might
#    switch to 'over-ridden', or might start configuring, leading to 'active'
# When 'hotspot' is pressed, we deconfig interface if up, then enable hotspot
# When 'hotspot' is released, we deconfig and switch to 'pending'


import gtk, pango
from subprocess import Popen, PIPE
import suspend

class iface:
    def __init__(self, parent):
        self.state = 'disabled'
        self.parent = parent
        self.make_button()
        self.can_hotspot = True
        self.hotspot_net = None
        self.hotspot = False
        self.addr = 'No Address'
        self.default = None
        self.set_label()

    def make_button(self):
        self.button = gtk.ToggleButton(self.name)
        self.button.set_alignment(0,0.5)
        self.button.show()
        self.button.connect('pressed', self.press)

    def set_label(self):
        self.addr = self.get_addr()
        if self.state == 'disabled':
            config = ''
        else:
            self.get_config()
            config = ' (%s)' % self.config
        if self.hotspot:
            hs = 'hotspot '
        else:
            hs = ''
        self.label = ('<span color="blue" size="30000">%s</span>\n<span size="12000">  %s%s\n%s</span>\n'
                      % (self.name, hs, self.state, config))
        if self.state == 'active' and not self.hotspot:
            self.default = self.get_default()
            self.label = self.label + '<span size="12000">Gateway: %s</span>' % self.default
        self.button.child.set_markup(self.label)

        if (self.hotspot and
            self.addr != self.hotspot_net and
            self.addr != 'No Address') :
            if self.hotspot_net:
                Popen(['iptables','-t','nat','-D','POSTROUTING','-s',
                       self.hotspot_net,'-j','MASQUERADE']).wait()
            self.hotspot_net = self.addr
            # Delete then add to avoid creating multiple entries
            Popen(['iptables','-t','nat','-D','POSTROUTING','-s',
                   self.hotspot_net,'-j','MASQUERADE']).wait()
            Popen(['iptables','-t','nat','-A','POSTROUTING','-s',
                   self.hotspot_net,'-j','MASQUERADE']).wait()
        if not self.hotspot and self.hotspot_net != None:
            Popen(['iptables','-t','nat','-D','POSTROUTING','-s',
                   self.hotspot_net,'-j','MASQUERADE']).wait()
            self.hotspot_net = None

    def selected(self):
        pass

    def get_config(self):
        self.config = '-'

    def press(self, ev):
        self.parent.select(self)

    def get_addr(self):
        p = Popen(['ip', 'add', 'show', 'dev', self.iface],
                  stdout = PIPE, close_fds = True)
        ip = 'No Address'
        for l in p.stdout:
            l = l.strip().split()
            if l[0] == 'inet':
                ip = l[1]
        p.wait()
        return ip

    def get_default(self):
        p = Popen(['ip', 'route', 'list', 'match', '128.0.0.1'],
                  stdout = PIPE, close_fds = True)
        route = 'none'
        for l in p.stdout:
            l = l.strip().split()
            if l[0] == 'default' and l[1] == 'via':
                route = l[2]
            elif l[0] == 'default' and l[1] == 'dev' and l[2] == self.iface:
                route = 'Provided'
        return route

    def rfkill(self, state):
        Popen(['rfkill', state, self.rfkill_name]).wait()

    def shutdown(self):
        pass

    def config_widget(self):
        return None

from socket import *
import os, gobject, time
from listselect import ListSelect
class WLAN_iface(iface):
    # We run wpa_supplicant and udhcpc as needed, killing them when done
    # For 'hotspot' we also run 'ifup' or 'ifdown'
    # When wpa_supplicant is running, we connect to the socket to
    # communicate and add new networks
    # Some wpa_cli commands:
    # ATTACH   -> results in 'OK'
    # PING -> results in 'PONG'  (wpa_cli does this every 5 seconds)
    # LIST_NETWORKS
    # 0	LinuxRules	any	[CURRENT]
    # 1	n-squared	any	
    # 2	JesusIsHere	any	
    # 3	TorchNet	any	
    # 4	freedom	any	
    
    #
    # Some async responses:
    # <3>CTRL-EVENT-BSS-ADDED 0 00:60:64:24:0a:22
    # <3>CTRL-EVENT-BSS-ADDED 1 00:04:ed:1e:98:48
    # <3>CTRL-EVENT-SCAN-RESULTS 
    # <3>Trying to associate with 00:60:64:24:0a:22 (SSID='LinuxRules' freq=2427 MHz)
    # <3>Associated with 00:60:64:24:0a:22
    # <3>WPA: Key negotiation completed with 00:60:64:24:0a:22 [PTK=CCMP GTK=TKIP]
    # <3>CTRL-EVENT-CONNECTED - Connection to 00:60:64:24:0a:22 completed (auth) [id=0 id_str=]
    # <3>CTRL-EVENT-BSS-REMOVED 1 00:04:ed:1e:98:48
    # <3>CTRL-EVENT-DISCONNECTED bssid=..... reason=0

    def __init__(self, parent):
        self.name = 'WLAN'
        self.rfkill_name = 'wifi'
        self.iface = 'wlan0'
        self.ssid = 'unset'
        self.supplicant = None
        self.udhcpc = None
        self.netlist = {}
        self.scanlist = {}
        self.configing = False
        self.pending = None
        self.to_send = []
        iface.__init__(self, parent)
        self.set_label()
        self.cfg = None

    def get_config(self):
        self.config = self.addr + ' ESSID=%s' % self.ssid

    def activate(self):
        self.rfkill('unblock')
        self.supplicant_up()

    def shutdown(self):
        self.udhcpc_close()
        self.supplicant_close()
        Popen('ifconfig wlan0 down', shell=True).wait()
        self.rfkill('block')

    def supplicant_up(self):
        if self.supplicant:
            return
        try:
            os.unlink('/run/wpa_supplicant/wlan0')
        except:
            pass
        self.supplicant = Popen(['wpa_supplicant','-i','wlan0',
                                 '-c','/etc/wpa_supplicant.conf','-W'],
                                close_fds = True)
        try:
            os.unlink('/run/wpa_ctrl_netman')
        except:
            pass
        s = socket(AF_UNIX, SOCK_DGRAM)
        s.bind('/run/wpa_ctrl_netman')
        ok = False
        while not ok:
            try:
                s.connect('/run/wpa_supplicant/wlan0')
                ok = True
            except:
                time.sleep(0.1)
        self.sock = s
        self.watch = gobject.io_add_watch(s, gobject.IO_IN, self.recv)
        self.request('ATTACH')

    def restart_dhcp(self):
        # run udhcpc -i wlan0 -f (in foreground)
        # recognise message like
        # Lease of 192.168.1.184 obtained, lease time 600

        self.udhcpc_close()
        env = os.environ.copy()
        if self.hotspot:
            env['NO_DEFAULT'] = 'yes'
        self.udhcpc = Popen(['udhcpc','--interface=wlan0','--foreground',
                             '--script=/etc/wifi-udhcpc.script'],
                            close_fds = True,
                            env = env,
                            stdout = PIPE)
        self.udhcpc_watch = gobject.io_add_watch(
            self.udhcpc.stdout, gobject.IO_IN,
            self.dhcp_read)

    def dhcp_read(self, dir, foo):
        if not self.udhcpc:
            return False
        m = self.udhcpc.stdout.readline()
        if not m:
            return False
        l = m.strip().split()
        print 'dhcp got', l
        if len(l) >= 4 and l[0] == 'Lease' and l[3] == 'obtained,' and self.state != 'active':
            self.state = 'active'
            self.parent.update_active()
            self.set_label()
            if self.checker:
                gobject.source_remove(self.checker)
                self.checker = None
        if l == ['udhcp','script','bound']:
            self.set_label()
        return True

    def request(self, msg):
        if self.pending:
            self.to_send.append(msg)
            return
        self.pending = msg
        self.sock.sendall(msg)
        print 'sent request', msg

    def recv(self, dir, foo):
        try:
            r = self.sock.recv(4096)
        except error:
            return False
        if not r:
            self.supplicant_close()
            return False

        if r[0] == '<':
            self.async(r[r.find('>')+1:])
            return True
        elif self.pending == 'ATTACH':
            # assume it is OK
            self.request('LIST_NETWORKS')
        elif self.pending == 'LIST_NETWORKS':
            self.take_list(r)
        elif self.pending == 'SCAN_RESULTS':
            self.take_scan(r)
        elif self.pending == 'ADD_NETWORK':
            self.got_network(r)

        self.pending = None
        if len(self.to_send) > 0:
            self.request(self.to_send.pop(0))
        return True

    def udhcpc_close(self):
        if self.udhcpc:
            gobject.source_remove(self.udhcpc_watch)
            self.udhcpc.terminate()
            self.udhcpc.wait()
            self.udhcpc = None
            self.udhcpc_watch = None

    def supplicant_close(self):
        if self.supplicant:
            self.supplicant.terminate()
            gobject.source_remove(self.watch)
            self.watch = None
            self.supplicant.wait()
            self.supplicant = None

    def async(self, mesg):
        print 'GOT ', mesg
        print 'got (%s)' % mesg[:20]
        if mesg[:20] == 'CTRL-EVENT-CONNECTED':
            p = mesg.find('id=')
            if p > 0:
                id = mesg[p+3:].split()[0]
                for ssid in self.netlist:
                    if id == self.netlist[ssid][0]:
                        self.ssid = ssid
            self.request('LIST_NETWORKS')
            self.set_label()
            self.checker = gobject.timeout_add(30000, self.reassoc)
            return self.restart_dhcp()
        if mesg[:23] == 'CTRL-EVENT-DISCONNECTED':
            if self.state == 'active':
                self.state = 'pending'
            self.udhcpc_close()
            self.set_label()
            self.parent.update_active()
            self.request('LIST_NETWORKS')
            return
        if mesg[:23] == 'CTRL-EVENT-SCAN-RESULTS':
            self.request('SCAN_RESULTS')
            return
        if mesg[:19] == 'Trying to associate':
            l = mesg.split("'")
            print 'len', len(l)
            if len(l) > 1:
                print 'ssid = ', l[1]
                self.ssid = l[1]
            return

    def reassoc(self):
        if self.state == 'active':
            return False
        self.request('REASSOCIATE')
        self.checker = gobject.timeout_add(30000, self.reassoc)
        return False

    def take_list(self, mesg):
        self.netlist = {}
        for line in mesg.split('\n'):
            if line != '' and line[0:7] != 'network':
                l = line.split('\t')
                # number  name  'any'  '[CURRENT]'-or-'[DISABLED]'
                self.netlist[l[1]] = (int(l[0]),l[3])
        self.fill_list()

    def take_scan(self, mesg):
        self.scanlist = {}
        for line in mesg.split('\n'):
            if line != '' and line[:5] != 'bssid':
                l = line.split('\t')
                self.scanlist[l[4]] = (l[2],l[3])
        print 'scan', self.scanlist
        self.fill_list()

    def config_widget(self):
        if self.cfg == None:
            self.make_cfg()
        self.fill_list()
        self.configing = True
        self.supplicant_up()
        self.rescan()
        return self.cfg

    def rescan(self):
        if not self.configing:
            return
        self.request('SCAN')
        gobject.timeout_add(5000, self.rescan)

    def make_cfg(self):
        # config widget is an entry, a ListSel, and a button row
        cfg = gtk.VBox()
        entry = gtk.Entry()
        ls = ListSelect()
        bb = gtk.HBox()

        cfg.show()
        cfg.pack_start(entry, expand = False)
        entry.show()

        cfg.pack_start(ls, expand = True)
        ls.show()
        ls.set_zoom(40)
        cfg.pack_start(bb, expand = False)
        bb.show()
        bb.set_homogeneous(True)
        bb.set_size_request(-1, 80)

        ls.connect('selected', self.select)

        entry.modify_font(self.parent.fd)
        self.enable_btn = self.add_button(bb, 'Enable', self.enable)
        self.disable_btn = self.add_button(bb, 'Disable', self.disable)
        self.set_key_btn = self.add_button(bb, 'Set Key', self.set_key)
        self.forget_btn = self.add_button(bb, 'Forget', self.forget)
        self.enable_btn.hide()
        self.disable_btn.hide()
        self.set_key_btn.hide()
        self.forget_btn.hide()
        self.add_button(bb, 'Done', self.done)

        self.cfg = cfg
        self.key_entry = entry
        self.net_ls = ls

    def add_button(self, bb, name, cmd):
        btn = gtk.Button()
        btn.set_label(name)
        btn.child.modify_font(self.parent.fd)
        btn.show()
        bb.pack_start(btn, expand = True)
        btn.connect('clicked', cmd)
        return btn

    def selected_net(self):
        net = self.net_ls.selected_str
        if net == None:
            return (None, None, None)
        net = net[1:]
        l=None
        s=None
        if net in self.netlist:
            l = self.netlist[net]
        if net in self.scanlist:
            s = self.scanlist[net]
        return (net, l,s)

    def select(self, s, n):
        net = self.selected_net()
        if net[1]:
            (num,st) = net[1]
            if st == '[DISABLED]':
                self.enable_btn.show()
                self.disable_btn.hide()
                self.set_key_btn.hide()
                self.forget_btn.show()
            else:
                self.enable_btn.hide()
                self.disable_btn.show()
                self.set_key_btn.show()
                self.forget_btn.hide()
        else:
            # don't know this one
            self.enable_btn.show()
            self.disable_btn.hide()
            self.set_key_btn.show()
            self.forget_btn.hide()

    def create_network(self, ssid, sec, key):
        if sec == '[ESS]':
            self.settings = [('ssid "%s"' % ssid),
                             'key_mgmt NONE',
                             ]
        else:
            self.settings = [('ssid "%s"' % ssid),
                             'key_mgmt WPA-PSK',
                             ('psk "%s"' % key),
                             ]
        self.request('ADD_NETWORK')
    def got_network(self, r):
        n = int(r)
        for l in self.settings:
            self.request('SET_NETWORK %d %s' % (n, l))
        self.request('ENABLE_NETWORK %d' % n)
        self.request('SAVE_CONFIG')
        self.request('LIST_NETWORKS')
        self.request('REASSOCIATE')

    def enable(self, x):
        net = self.selected_net()
        if net[1]:
            num = net[1][0]
            self.request('ENABLE_NETWORK %d'% num)
            self.request('SAVE_CONFIG')
            self.request('LIST_NETWORKS')
            return
        if net[2]:
            self.create_network(net[0],net[2][1],"xxxxxxxx")

    def disable(self, x):
        net = self.selected_net()
        if net[1]:
            num = net[1][0]
            self.request('DISABLE_NETWORK %d'% num)
            self.request('SAVE_CONFIG')
            self.request('LIST_NETWORKS')

    def set_key(self, x):
        net = self.selected_net()
        if net[1]:
            self.request('SET_NETWORK %d psk "%s"' %
                         (net[1][0], self.key_entry.get_text()))
            self.key_entry.set_text('')
            self.request('SAVE_CONFIG')
            self.request('REASSOCIATE')
        elif net[2]:
            self.create_network(net[0],net[2][1],self.key_entry.get_text())
            self.key_entry.set_text('')

    def forget(self, x):
        net = self.selected_net()
        if net[1]:
            num = net[1][0]
            self.request('REMOVE_NETWORK %d'% num)
            self.request('SAVE_CONFIG')
            self.request('LIST_NETWORKS')

    def done(self, x):
        self.configing = False
        self.parent.deconfig()

    def fill_list(self):
        if not self.configing:
            return
        l = []
        for i in self.scanlist:
            strength =self.scanlist[i][0]
            sec = self.scanlist[i][1]
            if sec == "[ESS]":
                t=' '
            elif sec == "[IBSS]":
                t='!'
            elif ('[WPA-PSK' in sec or '[WPA2-PSK' in sec
                  or '[WEP]' in sec):
                t='*'
            else:
                t='?'
            if i in self.netlist:
                if self.netlist[i][1] == '[DISABLED]':
                    t='-'
                if self.netlist[i][1] == '[CURRENT]':
                    l.append((t+i, 'magenta'))
                else:
                    l.append((t+i, 'blue'))
            else:
                l.append((t+i, 'black'))
        for i in self.netlist:
            if i not in self.scanlist:
                l.append(('-'+i, 'red'))
        if not l:
            l = [('none','green')]
        self.net_ls.list = l
        self.net_ls.list_changed()
        self.select(self.net_ls, self.net_ls.selected)

    def config_hotspot(self):
        self.rfkill('unblock')
        self.state = 'pending'
        self.supplicant_up()
            

####
# WWAN support code, for talking to GSMD
#

import dnotify

def record(key, value):
    f = open('/run/gsm-state/.new.' + key, 'w')
    f.write(value)
    f.close()
    os.rename('/run/gsm-state/.new.' + key,
              '/run/gsm-state/' + key)

def recall(key, nofile = ""):
    try:
        fd = open("/run/gsm-state/" + key)
        l = fd.read(1000)
        l = l.strip()
        fd.close()
    except IOError:
        l = nofile
    return l

####
# load current usage from log file.
# This is currently completely unoptimised.
def read_bytes(dev = 'hso0'):
    f = file('/proc/net/dev')
    rv = None
    for l in f:
        w = l.strip().split()
        if w[0] == dev + ':':
            rv = ( int(w[1]), int(w[9]) )
            break
    f.close()
    return rv

def get_usage():
    SIM = recall('sim')
    f = open("/var/log/gsm-data")
    rcv = 0
    snd = 0
    for l in f:
        w = l.strip().split()
        if w[2] == SIM:
            rcv += int(w[3])
            snd += int(w[4])

    base = recall('data-last-usage').strip().split()
    current = read_bytes()
    rcv += current[0] - int(base[0])
    snd += current[1] - int(base[1])
    return rcv + snd

class WWAN_iface(iface):
    def __init__(self, parent):
        self.name = 'GSM/3G'
        self.rfkill_name = 'wwan'
        self.conf = None
        self.iface = 'hso0'
        self.sim = recall('sim')
        self.APN = 'INTERNET'
        if self.sim == "505023304781083":
            # exetel mobile internet sim
            self.APN = 'exetel1'
        if self.sim == "505023104508131":
            # My exetel sim card
            self.APN = 'INTERNET'
        iface.__init__(self, parent)
        self.can_hotspot = False
        d = dnotify.dir('/run/gsm-state')
        self.data_watcher = d.watch('data', self.check_data)

    def check_data(self, fl):
        l = recall('data')
        print 'data got %s' % l
        if l:
            self.state = 'active'
        elif self.state == 'active':
            self.state = 'pending'
        gobject.idle_add(self.recalc)

    def recalc(self):
        print 'call recalc'
        self.parent.update_active()
        if self.state == 'active':
            d = recall('dns').split()
            f = open("/etc/resolv.conf","w")
            for s in d:
                f.write('nameserver %s\n' % s)
            f.close()
            os.system('route add default dev hso0')
        self.set_label()

    def get_config(self):
        self.config = self.addr + ' APN=%s' % self.APN

    def activate(self):
        self.rfkill('unblock')
        self.state = 'pending'
        record('data-APN', self.APN)

    def shutdown(self):
        self.state = 'disabled'
        record('data-APN','')
        self.rfkill('block')

    def selected(self):
        if self.state == 'disabled':
            config = ''
        else:
            self.get_config()
            config = ' (%s)' % self.config
        if self.hotspot:
            hs = 'hotspot '
        else:
            hs = ''
        usage = get_usage()/1000000.0
        self.label = ('<span color="blue" size="30000">%s</span>   %fMB\n<span size="12000">  %s%s\n%s</span>\n'
                      % (self.name, usage, hs, self.state, config))
        if self.state == 'active' and not self.hotspot:
            self.default = self.get_default()
            self.label = self.label + '<span size="12000">Gateway: %s</span>' % self.default
        self.button.child.set_markup(self.label)


class BT_iface(iface):
    def __init__(self, parent):
        self.name = 'BT'
        self.iface = 'pan0'
        self.rfkill_name = 'bluetooth'
        iface.__init__(self, parent)

class USB_iface(iface):
    def __init__(self, parent):
        self.name = 'USB'
        self.conf = 'p2p'
        self.iface = 'usb0'
        self.state = 'pending'
        iface.__init__(self, parent)
        self.set_label()

    def get_config(self):
        self.config = self.addr

    def setconf(self, mode):
        if self.conf == mode:
            return
        if mode:
            cmd = 'ifdown --force usb0; ifup usb0=usb0-%s' % mode
        else:
            cmd = 'ifdown --force usb0'
        print 'run command', cmd
        Popen(cmd, shell=True).wait()
        self.conf = mode

    def shutdown(self):
        # interpret this as 'switch to p2p mode', unless 'disabled'
        if self.state == 'disabled':
            self.setconf(None)
        else:
            self.setconf('p2p')

    def activate(self):
        # must set state to 'active' once it is - then maybe update_active
        print 'activate usb'
        self.setconf('client')
        self.state = 'active'

    def config_hotspot(self):
        self.setconf('hotspot')
        self.state = 'active'

class Netman(gtk.Window):
    # Main window has one radio button for each interface.
    # Along bottom are 3 buttons: toggle 'enable', toggle 'hotspot', 'config'
    # 
    def __init__(self):
        gtk.Window.__init__(self)
        self.connect('destroy', self.close_application)
        self.set_title('NetMan')

        self.iface = [ WLAN_iface(self), WWAN_iface(self),
                       BT_iface(self), USB_iface(self) ]

        # 'v' fills the window, 'i' holds the interface, 'b' holds the buttons
        v = gtk.VBox()
        v.show()
        self.main_window = v
        self.add(v)
        i = gtk.VBox()
        i.show()
        v.pack_start(i, expand = True)
        i.set_homogeneous(True)

        for iface in self.iface:
            i.pack_start(iface.button, expand = True)

        b = gtk.HBox()
        b.show()
        b.set_size_request(-1, 80)
        b.set_homogeneous(True)
        v.pack_end(b, expand = False)

        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(35*pango.SCALE)
        self.fd = fd

        self.blocker = suspend.blocker()

        self.enable_btn = self.add_button(b, 'enabled', fd, True, self.enable)
        self.hotspot_btn = self.add_button(b, 'hotspot', fd, True, self.hotspot)
        self.add_button(b, 'config', fd, False, self.config)
        self.selected = self.iface[0]
        self.iface[0].button.set_active(True)

        self.update_active()

    def close_application(self, ev):
        for i in self.iface:
            i.shutdown()
        gtk.main_quit()

    def select(self, child):
        for i in self.iface:
            if i != child:
                i.button.set_active(False)
        self.selected = None
        self.enable_btn.set_active(child.state != 'disabled')
        self.hotspot_btn.set_inconsistent(not child.can_hotspot)
        self.hotspot_btn.set_active(child.hotspot)
        self.selected = child
        child.selected()

    def add_button(self, bb, name, fd, toggle, func):
        if toggle:
            btn = gtk.ToggleButton()
        else:
            btn = gtk.Button()
        btn.set_label(name)
        btn.child.modify_font(fd)
        btn.show()
        bb.pack_start(btn, expand = True)
        btn.connect('clicked', func)
        return btn

    def enable(self, ev):
        if not self.selected:
            return
        print 'active?', self.enable_btn.get_active()
        if not self.enable_btn.get_active():
            return self.disable()

        if self.selected.state != 'disabled':
            return
        print 'enable %s, was %s' % (self.selected.name, self.selected.state)
        self.selected.state = 'pending'
        self.update_active()
        self.selected.set_label()
    def disable(self):
        if self.selected.state == 'disabled':
            return
        self.selected.state = 'disabled'
        self.selected.shutdown()
        self.update_active()
        self.selected.set_label()

    def hotspot(self, ev):
        if not self.selected:
            return
        if not self.selected.can_hotspot:
            return
        if self.hotspot_btn.get_active():
            # enable hotspot
            s = self.selected
            self.selected = None
            self.enable_btn.set_active(True)
            self.selected = s
            s.hotspot = True
            try:
                f = open('/proc/sys/net/ipv4/ip_forward','w')
                f.write('1')
                f.close()
            except:
                pass
            s.config_hotspot()
            s.set_label()
            self.blocker.block()
        else:
            # disable hotspot
            self.selected.hotspot = False
            self.selected.shutdown()
            self.update_active()
            self.selected.set_label()

    def config(self, ev):
        w = self.selected.config_widget()
        if not w:
            return
        self.remove(self.main_window)
        self.add(w)
        w.show()

    def deconfig(self):
        self.remove(self.get_child())
        self.add(self.main_window)

    def update_active(self):
        # try to activate the first available interface,
        # and stop others
        active_found = False
        print 'update_active start'
        hotspot = False
        for i in self.iface:
            print i.name, i.state
            if i.hotspot:
                hotspot = True
            if i.state in ['disabled'] or i.hotspot:
                continue
            if active_found:
                if i.state != 'over-ridden':
                    i.shutdown()
                    i.state = 'over-ridden'
                    i.set_label()
            elif i.state == 'active':
                active_found = True
            else:
                if i.state == 'over-ridden':
                    i.state = 'pending'
                i.activate()
                if i.state == 'active':
                    active_found = True
                i.set_label()
        if not hotspot:
            self.blocker.unblock()
        print 'update_active end'

if __name__ == '__main__':

    n = Netman()
    n.set_default_size(480, 640)
    n.show()
    gtk.main()

